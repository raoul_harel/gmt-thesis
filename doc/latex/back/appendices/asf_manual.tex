\chapter{Agency system framework manual}
\label{ch:asf_manual}
\lstset{style=csharp}

This document is an instruction manual for the software library associated with the actor agency system framework outlined in Chapter \ref{ch:agency_system_framework}. It contains a listing of prerequisites (Section \ref{sec:asf_manual-prerequisites}), installation instructions (Section \ref{sec:asf_manual-installation}), usage guidelines for both operation module implementers as well as system administrators (sections \ref{sec:asf_manual-operation_modules} and \ref{sec:asf_manual-system_administration}, respectively), and pointers to further documentation (Section \ref{sec:asf_manual-further_documentation}). 

\section{Prerequisites}
\label{sec:asf_manual-prerequisites}

\begin{itemize}
	\item \textbf{C Sharp} is the programming language used across the project.
	\item \textbf{Visual Studio} is the integrated development environment used to manage source files and configure builds. Version 2015 and above should be compatible.
\end{itemize}

\section{Installation}
\label{sec:asf_manual-installation}

Currently, we do not host any pre-compiled DLLs for download, so you would want to build the library and its dependencies from source. The relevant directories in the repository are \code{core/dialog-agency/} and \code{core/commons/}. \code{dialog-agency} is the project directory of the framework itself, while \code{commons} contains all sorts of utilities which we rely on as a dependency. 

\subsection{Building from source}
\label{sec:asf_manual-installation-from_source}

In order to build all relevant projects from source, we recommend doing the following (all mentioned paths are relative to \code{core/dialog-agency/}):

\begin{enumerate}[noitemsep]
	\item 
		Open the framework's test project solution \code{test-dialog-agency.sln} located under \code{test/} in Visual Studio.
	\item 
		Build the solution.
	\item 
		Run all tests present through Visual Studio's test explorer to make sure they pass successfully (optional).
	\item
		Both \code{dialog-agency.dll} as well as \code{commons.dll} should now be available under \code{test/bin/\{Debug|Release\}/}, depending on your desired build configuration. You may now reference both in your own projects as you wish.
\end{enumerate}

\section{Operation modules}
\label{sec:asf_manual-operation_modules}

In this section we provide general instructions for the usage of the framework from a prospective module implementer's point of view. Starting with an overview of the module base types (Section \ref{sec:asf_manual-operation_modules-bases}) and proceeding with details regarding module initialization (Section \ref{sec:asf_manual-operation_modules-initialization}), module-state interaction (Section \ref{sec:asf_manual-operation_modules-state_interaction}), and specific implementation instructions for each of the six module types (\cref{%
	sec:asf_manual-operation_modules-implementing_rap,% 
	sec:asf_manual-operation_modules-implementing_cap,%
	sec:asf_manual-operation_modules-implementing_state_update,%
	sec:asf_manual-operation_modules-implementing_action_selection,%
	sec:asf_manual-operation_modules-implementing_action_timing,%
	sec:asf_manual-operation_modules-implementing_action_realization%
}).

\subsection{Module bases}
\label{sec:asf_manual-operation_modules-bases}

The first step in implementation of a new module, is to have it be derived from one of the six abstract module base types provided by the framework:

\begin{multicols}{2}
\begin{itemize}[noitemsep]
	\item \code{RecentActivityPerceptionModule}
	\item \code{CurrentActivityPerceptionModule}
	\item \code{StateUpdateModule}
	\item \code{ActionSelectionModule}
	\item \code{ActionTimingModule}
	\item \code{ActionRealizationModule}
\end{itemize}
\end{multicols}

\noindent
Once we have a new module derived from one of the above, its functionality can be specified further by overriding several key methods provided by its base. Two of these are common across all module types, and pertain to a module's initialization logic and its interaction with the information state. These are covered in sections \ref{sec:asf_manual-operation_modules-initialization} and \ref{sec:asf_manual-operation_modules-state_interaction}, respectively. The remaining methods pertain to each module's individual responsibility, and therefore are each tied to a specific base. These are covered in \cref{%
	sec:asf_manual-operation_modules-implementing_rap,% 
	sec:asf_manual-operation_modules-implementing_cap,%
	sec:asf_manual-operation_modules-implementing_state_update,%
	sec:asf_manual-operation_modules-implementing_action_selection,%
	sec:asf_manual-operation_modules-implementing_action_timing,%
	sec:asf_manual-operation_modules-implementing_action_realization%
}.

\subsection{Module initialization}
\label{sec:asf_manual-operation_modules-initialization}

Every module is initialized exactly once during its lifetime, immediately following construction of the system instance to which it belongs (Section \ref{sec:asf_manual-system_administration-state}). At that time, it is given the opportunity to execute any initialization-related logic it requires.

This is enabled by the \code{Setup()} method, which is available across all module base types through their shared ancestor, \code{class OperationModule<T>}. As is evident from listing \ref{code:asf-module_setup_signature}, usage of \code{Setup()} is completely optional, as its default implementation is a no-op.

\begin{lstlisting}[
float=htbp, 
caption=Module setup method signature., 
label=code:asf-module_setup_signature
]
public abstract class OperationModule<T>
{
	...
	public virtual void Setup() { }
}
\end{lstlisting}

\subsection{Module-state interaction}
\label{sec:asf_manual-operation_modules-state_interaction}

A module's interaction with the information state is relevant at two stages: (1) during initialization of a new system instance, when it is checked to see if the system's state object contains the necessary data required by the module, and (2) during the module's execution, when the module may read/write data from/to the state.

\subsubsection{State data requirements}

Different modules (and different implementations of modules) require different types of data be available to them through the state. While ensuring that the state indeed contains the appropriate data is the responsibility of a system's administrator (Section \ref{sec:asf_manual-system_administration-state}), the framework does provide a mechanism by which a module can declare and enforce this type of requirements.

This is enabled by the \code{GetRequired\allowbreak StateComponents()} method, which --- again like in the case of \code{Setup()} --- is available across all module base types. \code{GetRequired\allowbreak StateComponents()} takes on a single parameter \code{types}: an initially empty collection, to be populated with all data types the module demands be available through the state.

For example, if we were to implement an action selection module whose logic requires data of type \code{Foo} to be available, we would do so in the manner seen in listing \ref{code:asf-module_state_data_requirement_example}.

\begin{lstlisting}[
float=htbp, 
caption=Module state data requirement declaration signature., 
label=code:asf-module_state_data_requirement_signature
]
public abstract class OperationModule<T>
{
	...
	public virtual void 
		GetRequiredStateDataTypes(ICollection<Type> types) { }
}
\end{lstlisting}
\begin{lstlisting}[
float=htbp, 
caption=Declaring state data requirements., 
label=code:asf-module_state_data_requirement_example
]
class Foo { ... }

public sealed class MyActionSelectionModule : ActionSelectionModule
{
	...
	public override void 
		GetRequiredStateDataTypes(ICollection<Type> types) 
	{
		types.Add(typeof(Foo));
	}
}
\end{lstlisting}

If during construction of a new system instance there is an attempt made to pair a module with an information state that is not conforming to the module's requirements, the framework will terminate with an error. This mechanism helps enforce these requirements and enables system administrators to catch errors they made regarding state structure or state-module pairings at an early a stage as possible.

\subsubsection{Mutable and immutable states}

All modules have access to the system's information state through an inherited \code{State} property, and depending on the module's type, \code{State} takes on one of two forms: either mutable or immutable.

As is more thoroughly explained in Section \ref{sec:asf_manual-system_administration-state}, the state is comprised of data components, with each component dedicated to holding a single type of data. Accessing these containers is allowed through both the mutable and immutable versions of the state. This is done through the \code{State.Get<T>()} method, with \code{T} being the type of data to retrieve. In contrast, only through the mutable version is it possible to overwrite components with new data. This is done through the \code{State.Set<T>(value)} method, with \code{value} being the new value to assign.

As is to be expected, the only module with access to the mutable state interface is the state update module. All other module types are confined to the read-only version.

\subsection{Implementing recent activity perception}
\label{sec:asf_manual-operation_modules-implementing_rap}

The recent activity perception module reports on recently occurring dialog moves and their sources. To implement this functionality, a module deriving from \code{RecentActivityPerception\allowbreak Module} must override the abstract method \code{Compose\allowbreak ActivityReport()} (listing \ref{code:asf-rap_main_signature}).

\code{Compose\allowbreak ActivityReport()} takes on a single parameter, \code{report}, through which we may submit whatever activity we perceive. 

\begin{lstlisting}[
float=htbp, 
caption=Recent activity perception module's main method signature., 
label=code:asf-rap_main_signature
]
public abstract class RecentActivityPerceptionModule 
	: OperationModule<ImmutableState>
{
	...
	protected abstract void 
		ComposeActivityReport(RecentActivityReport report);
}
\end{lstlisting}

Submission of an activity event is done by pairing a dialog move with the actor who performed it and adding the pair to the report via \code{report.Add(source, move)}, as can be seen done in listing \ref{code:asf-rap_report_event_addition}. It is important to note that the report will not accept duplicates of events it already contains nor events whose dialog move is the idle move.

\begin{lstlisting}[
float=htbp, 
caption=Reporting recent activity., 
label=code:asf-rap_report_event_addition
]
// <within ComposeActivityReport()>

Actor some_actor = ...;
DialogMove some_move = ...;

report.Add(source: some_actor, move: some_move);
\end{lstlisting}

\subsection{Implementing current activity perception}
\label{sec:asf_manual-operation_modules-implementing_cap}

\begin{sloppypar}
The current activity perception module reports the current activity status of actors, i.e. whether they are currently in the process of realizing a dialog move (active) or not (passive). Analogous to the implementation of a \code{RecentActivityPerception\allowbreak Module}, a module deriving from \code{CurrentActivityPerception\allowbreak Module} must override the abstract method \code{ComposeActivity\allowbreak Report()} (listing \ref{code:asf-cap_main_signature}), taking on a single parameter, \code{report}. We use the \code{report} object to submit our classification of actors.
\end{sloppypar}

\begin{lstlisting}[
float=htbp, 
caption=Current activity perception module's main method signature., 
label=code:asf-cap_main_signature
]
public abstract class CurrentActivityPerceptionModule 
: OperationModule<ImmutableState>
{
	...
	protected abstract void 
		ComposeActivityReport(CurrentActivityReport report);
}
\end{lstlisting}

Classification of actors is done by marking them as either active or passive through the respective methods \code{report.AddAsActive(actor)} and \code{report.AddAsPassive(actor)} (listing \ref{code:asf-cap_report_actor_addition}). It is important to note that the report will not accept duplicates of actors it already contains nor will it permit for an actor to be classified as both passive and active simultaneously.

\begin{lstlisting}[
float=htbp, 
caption=Reporting current activity., 
label=code:asf-cap_report_actor_addition
]
// <within ComposeActivityReport()>

Actor some_passive_actor = ...;
Actor some_active_actor = ...;

report.AddAsPassive(some_passive_actor);
report.AddAsActive(some_active_actor);
\end{lstlisting}

\pagebreak

\subsection{Implementing state update}
\label{sec:asf_manual-operation_modules-implementing_state_update}

The state update module takes the combined input of the two perception modules together with a snapshot of the current state of the system, and bases a manipulation of the information state on those. A module deriving from \code{StateUpdate\allowbreak Module} must override the abstract method \code{PerformUpdate()} (listing \ref{code:asf-state_update_main_signature}).

\begin{lstlisting}[
float=htbp, 
caption=State update module's main method signature., 
label=code:asf-state_update_main_signature
]
public abstract class StateUpdateModule 
	: OperationModule<MutableState>
{
	...
	public abstract void PerformUpdate
	(
		RecentActivityReport recent_activity,
		CurrentActivityReport current_activity,
		SystemActivitySnapshot system_activity
	);
}
\end{lstlisting}

\code{PerformUpdate()} takes on three parameters: \code{recent\_activity}, \code{current\_activity}, and \code{system\_activity}. The origin of \code{recent\_activity} and \code{current\_activity} is in the respective output of the two perception modules (sections \ref{sec:asf_manual-operation_modules-implementing_rap} and \ref{sec:asf_manual-operation_modules-implementing_cap}, respectively), while \code{system\_activity} is provided by the system itself.

\pagebreak

The \code{recent\_activity} report allows for inspection of its contents in the form of a collection of realized dialog move events stored in its \code{Events} property. Each \textit{event} represents a pairing of a dialog move with the actor who made it (listing \ref{code:asf-querying_recent_activity}). 

\begin{lstlisting}[
float=htbp, 
caption=Querying recent activity., 
label=code:asf-querying_recent_activity
]
// <within PerformUpdate()>

foreach (var @event in recent_activity.Events)
{
	Actor source = @event.Source;
	DialogMove move = @event.Move;
}
\end{lstlisting}

The \code{current\_activity} report classifies actors as either active or passive, and makes this information accessible in two ways: (1) the \code{PassiveActors} property, containing a collection of all actors perceived as passive, (2) the \code{ActiveActors} property, which is the same for actors perceived as active, and (3) the \code{GetStatus(actor)} method, which yields an actor's current status flag: either \code{ActorActivityStatus.Passive} or \code{ActorActivityStatus.Active}. Example usage of the \code{current\_activity} object is in listing \ref{code:asf-querying_current_activity}.

\begin{lstlisting}[
float=htbp, 
caption=Querying current activity., 
label=code:asf-querying_current_activity
]
// <within PerformUpdate()>

foreach (var actor in current_activity.PassiveActors) { ... }
foreach (var actor in current_activity.ActiveActors) { ... }

Actor some_actor = ...;
var status = current_activity.GetStatus(some_actor);
if (status == ActorActivityStatus.Passive) { ... }
else if (status == ActorActivityStatus.Active) { ... }
\end{lstlisting}

The \code{system\_activity} snapshot is a small structure containing information about the system's own activity, expressed through three properties: \code{ActualMove} is the dialog move currently being realized, \code{TargetMove} is the move the system wishes to realize, and \code{RecentMove} is the most recently realized move (which in effect equals last update iteration's \code{ActualMove}).

\begin{lstlisting}[
float=htbp, 
caption=System activity snapshot structure., 
label=code:asf-system_activity_snapshot
]
public sealed class SystemActivitySnapshot
{
	...
	public DialogMove RecentMove { get; }
	public DialogMove TargetMove { get; }
	public DialogMove ActualMove { get; }
}
\end{lstlisting}

Unlike all other module types, the \code{StateUpdate\allowbreak Module} is in fact the only one which has access to the state's mutator method \code{State.Set<T>(value)}. It is expected that an implementation of the module would take into account its input, process it in some manner, and update the values of relevant state data components accordingly. For more information regarding the information state object, see Section \ref{sec:asf_manual-system_administration-state}, and for more about interaction with the state from within a module, see Section \ref{sec:asf_manual-operation_modules-state_interaction}.

\subsection{Implementing action selection}
\label{sec:asf_manual-operation_modules-implementing_action_selection}

The action selection module's job is to select a dialog move the system should aspire to realize. A module deriving from \code{ActionSelection\allowbreak Module} implements this functionality by overriding the abstract method \code{SelectMove()} (listing \ref{code:asf-action_selection_main_signature}). \code{SelectMove()} takes no parameters, and is only responsible for returning the chosen dialog move. 

\begin{lstlisting}[
float=htbp, 
caption=Action selection module's main method signature., 
label=code:asf-action_selection_main_signature
]
public abstract class ActionSelectionModule
	: OperationModule<ImmutableState>
{
	...
	public abstract DialogMove SelectMove();
}
\end{lstlisting}

\subsection{Implementing action timing}
\label{sec:asf_manual-operation_modules-implementing_action_timing}

The action timing module helps coordinate the execution of a system's target dialog move. It does so by indicating whether a realization of that move at this time is acceptable. A module deriving from \code{ActionTiming\allowbreak Module} implements this functionality by overriding the abstract method \code{IsValidMoveNow()} (listing \ref{code:asf-action_timing_main_signature}). \code{IsValidMoveNow()} takes on a single parameter, \code{move}, representing the aforementioned target move, and is expected to indicate its appropriateness at this time with a corresponding boolean return value.

\begin{lstlisting}[
float=htbp, 
caption=Action timing module's main method signature., 
label=code:asf-action_timing_main_signature
]
public abstract class ActionTimingModule
	: OperationModule<ImmutableState>
{
	...
	public abstract bool IsValidMoveNow(DialogMove move);
}
\end{lstlisting}

\subsection{Implementing action realization}
\label{sec:asf_manual-operation_modules-implementing_action_realization}

The action realization module is where dialog moves are converted into concrete behavior. A module deriving from \code{ActionRealization\allowbreak Module} should override the abstract method \code{RealizeMove()} (listing \ref{code:asf-action_realization_main_signature}). \code{RealizeMove()} takes on a single parameter , \code{move}, representing a dialog move the system wishes be realized. The status of that realization is in turn indicated by yielding one of two flags: either \code{Action\allowbreak RealizatioStatus.InProgress} or \code{Action\allowbreak RealizationStatus.Complete}.

\begin{lstlisting}[
float=htbp, 
caption=Action realization module's main method signature., 
label=code:asf-action_realization_main_signature
]
public abstract class ActionRealizationModule
	: OperationModule<ImmutableState>
{
	...
	public abstract 
		ActionRealizationStatus RealizeMove(DialogMove move);
}
\end{lstlisting}

\section{System administration}
\label{sec:asf_manual-system_administration}

In this section we provide general instructions for the usage of a the framework from a prospective system administrator's point of view, wherein we cover four aspects of administration: (1) provision of actors and dialog moves (sections \ref{sec:asf_manual-system_administration-actors} and \ref{sec:asf_manual-system_administration-dialog_moves}, respectively), (2) setting up of the information state (Section \ref{sec:asf_manual-system_administration-state}), (3) instantiation of the system (Section \ref{sec:asf_manual-usage-system_administration-instantiation}), and (4) simulation advancement (Section \ref{sec:asf_manual-usage-system_administration-stepping}).

\subsection{Actors}
\label{sec:asf_manual-system_administration-actors}

Actors are represented by the \code{Actor} interface. It is a blank one, and serves primarily as a guarantor of type safety during the framework's internal operations. Regardless, objects which are to be regarded as actors must implement it, as it might take on additional properties and methods in a future iteration of the library.

\subsection{Dialog moves}
\label{sec:asf_manual-system_administration-dialog_moves}

Dialog moves are represented by the abstract \code{DialogMove<TActor>} class, with \code{TActor} being the type of actor the move targets. For convenience, any concrete move derived from it may indicate its arity (i.e. the amount of targets it supports) through invocation of one of three possible base constructors, visible in listing \ref{code:asf-dialog_move_derivation}.

\begin{lstlisting}[
float=htbp, 
caption=Derivation of a new dialog move., 
label=code:asf-dialog_move_derivation
]
class MyActor : Actor { ... }

class MyMove : DialogMove<MyActor>
{
	public MyMove(DialogMoveKind kind) 
		: base(kind) { ... }  // If untargeted.
	public MyMove(DialogMoveKind kind, MyActor target) 
		: base(kind, target) { ... }  // If single-target.
	public MyMove(DialogMoveKind kind, IEnumerable<MyActor> targets) 
		: base(kind, targets) { ... }  // If multi-target.
}
\end{lstlisting}

Administrators may formulate whatever moves their simulated scenario demands --- with one exception: the special idle dialog move is already made available out of the box in the form of a singleton's property, \code{IdleMove.Instance}.

\code{DialogMoveKind} is an extendable enumeration provided so that it is more convenient to define the set of moves available in a scenario. Listing \ref{code:asf-dialog_move_kind_extension} shows a sample dialog move kind definition.

\begin{lstlisting}[
float=htbp, 
caption=Derivation of new dialog move kinds., 
label=code:asf-dialog_move_kind_extension
]
class MyMoveKind : DialogMoveKind
{
	public static readonly DialogMoveKind 
		Greeting = new MyMoveKind("greeting");
	public static readonly DialogMoveKind 
		Goodbye = new MyMoveKind("goodbye");
}

// <usage>
Actor some_actor = ...;
var greeting = new DialogMove(MyMoveKind.Greeting, target: some_actor);
\end{lstlisting}

\subsection{Setting up the information state}
\label{sec:asf_manual-system_administration-state}

The information state is made up of what we dub as \textit{data components}. Each component is associated with one type of data, and the state may support up to one component per type of data. As is explained in Section \ref{sec:asf_manual-operation_modules-state_interaction}, since the types of data a state is required to support are solely motivated by the operation modules comprising the system utilizing it, we opt to specify those during object construction.

For example, if our system contains modules that require data containers of type \code{Foo} and \code{Bar} be available, we are able to set up the state object to support these and at the same time initialize their respective components with a default value. This is illustrated in listing \ref{code:asf-state_construction}.

\begin{lstlisting}[
float=htbp, 
caption=Information state construction., 
label=code:asf-state_construction
]
class Foo { ... }
class Bar { ... }

var some_foo = new Foo(...);
var some_bar = new Bar(...);

var state = new InformationState.Builder()
	.Support<Foo>(initial_value: some_foo)
	.Support<Bar>(initial_value: some_bar)
	.Build();
\end{lstlisting}

\subsection{Instantiating the system}
\label{sec:asf_manual-usage-system_administration-instantiation}

A complete agency system is represented by the \code{AgencySystem} class. To instantiate it, two things are needed: (1) concrete instances of the six operation modules, and (2) a state object that conforms to their requirements. For more information regarding implementation of operation modules, see Section \ref{sec:asf_manual-operation_modules}; and regarding the information state object, see Section \ref{sec:asf_manual-system_administration-state}.

Once we have both modules and a state, we can set up a new system instance in two quick steps: In the first, we wrap up the six operation modules in a \code{ModuleBundle} (listing \ref{code:asf-module_bundle}), and in the second we feed both the bundle and the state to the system's constructor (listing \ref{code:asf-system_instantiation}).

\begin{lstlisting}[
float=htbp, 
caption=Bundling up modules., 
label=code:asf-module_bundle
]
var modules = new ModuleBundle.Builder()
	.WithRecentActivityPerceptionBy(my_RAP_module)
	.WithCurrentActivityPerceptionBy(my_CAP_module)
	.WithStateUpdateBy(my_SU_module)
	.WithActionSelectionBy(my_AS_module)
	.WithActionTimingBy(my_AT_module)
	.WithActionRealizationBy(my_AR_module)
	.Build();
\end{lstlisting}
\begin{lstlisting}[
float=htbp, 
caption=Instantiating the system., 
label=code:asf-system_instantiation
]
ModuleBundle modules = ...;
InformationState state = ...;

var system = new AgencySystem(state, modules);
\end{lstlisting}

\subsection{Advancing the simulation}
\label{sec:asf_manual-usage-system_administration-stepping}

Advancing the simulation is a straightforward affair. Simply invoke \code{system.Step()} to perform a complete iteration of the system's agency process.

\begin{lstlisting}[
float=htbp, 
caption=Executing one iteration of the agency process., 
label=code:asf-system_stepping
]
system.Step();
\end{lstlisting}

It is important to note that since both perception modules' input and the action realization module's output are left to be customized by the administrator, a recommended design approach for these would include some mechanism by which input/output can be transferred to/from them, and then to utilize that mechanism in the appropriate time with relation to calls of \code{Step()}. An illustration of this idea is visible in listing \ref{code:asf-custom_io_coordination}.

\begin{lstlisting}[
float=htbp, 
caption=Coordinating custom module intput/output with system stepping., 
label=code:asf-custom_io_coordination
]
var RAP = new MyRecentActivityPerceptionModule(...);
var CAP = new MyCurrentActivityPerceptionModule(...);
var AR = new MyActionRealizationModule(...);
var system = new AgencySystem(...);

RAP.SetInput(...);  // Environmental input goes in.
CAP.SetInput(...);
system.Step();      // Input is processed.
AR.GetOutput(...);  // Actor behavior comes out.
\end{lstlisting}

\pagebreak

\section{Further documentation}
\label{sec:asf_manual-further_documentation}

For more detailed documentation, we direct you to the complete reference site accessible through  \code{core/dialog-agency/doc/html/index.html}. Alternatively, you may directly browse documentation comments in the source files themselves under \code{core/dialog-agency/library/}.
