\chapter{Social-practice-based agency manual}
\label{ch:spbas_manual}

This document is an instruction manual for the software library associated with the social practice based agency system outlined in Chapter \ref{ch:spb_agency}. It contains a listing of prerequisites (Section \ref{sec:spbas_manual-prerequisites}), installation instructions (Section \ref{sec:spbas_manual-installation}), usage guidelines (Section \ref{sec:spbas_manual-usage}), and pointers to further documentation (Section \ref{sec:spbas_manual-further_documentation}). 

\section{Prerequisites}
\label{sec:spbas_manual-prerequisites}

\begin{itemize}
	\item \textbf{C Sharp} is the programming language used across the project.
	\item \textbf{Visual Studio} is the integrated development environment used to manage source files and configure builds. Version 2015 and above should be compatible.
\end{itemize}

\section{Installation}
\label{sec:spbas_manual-installation}

Currently, we do not host any pre-compiled DLLs for download, so you would want to build the library and its dependencies from source. The relevant directories in the repository are \code{demo/dialog-spb/}, \code{core/dialog-agency/} and \code{core/commons/}. \code{dialog-spb} is the project directory of the system itself, \code{dialog-agency} is the directory of the framework (outlined in Chapter \ref{ch:agency_system_framework}) on top of which the system is built, while \code{commons} contains all sorts of utilities which we rely on as a dependency. 

\subsection{Building from source}
\label{sec:spbas_manual-installation-from_source}

In order to build all relevant projects from source, we recommend doing the following (all mentioned paths are relative to \code{demo/dialog-spb/}):

\begin{enumerate}[noitemsep]
	\item 
		Open the system's test project solution \code{test-dialog-spb.sln} located under \code{test/} in Visual Studio.
	\item 
		Build the solution.
	\item 
		Run all tests present through Visual Studio's test explorer to make sure they pass successfully.
	\item
		All three DLLs: \code{dialog-spb.dll}, \code{dialog-agency.dll}, and \code{commons.dll} should now be available under \code{test/bin/\{Debug|Release\}/}, depending on your desired build configuration. You may now reference them in your own projects as you wish.
\end{enumerate}

\section{Usage}
\label{sec:spbas_manual-usage}

In this section we go over the available APIs for practice description (Section \ref{sec:spbas_manual-usage-practice_description}), and cover the prepackaged action selection/timing module implementations (Section \ref{sec:spbas_manual-usage-action_modules}).

\subsection{Composing practice descriptions}
\label{sec:spbas_manual-usage-practice_description}

Recall from Section \ref{sec:spba-describing_social_practices} the specification of several expectation types used to describe practices. These are available in the form of distinct classes, one for each type. In addition, there is also a more user-friendly method-based interface that more closely resembles a natural language description of practices. In Table \ref{table:spbas_manual-expectation_types} we list each expectation and its corresponding class- and convenience method names.

Detailed documentation for each class and method is available both in the source code as well as on the documentation site (see Section \ref{sec:spbas_manual-further_documentation} for details), so we do not cover that here. However, we do provide several usage examples of expectation arrangements using the natural-language-based convenience methods. 

\begin{table}[hbtp]
	\begin{tabularx}{\textwidth}{ X|X|X }
		Expectation Type & Class Name              & Convenience Method    \\
		\hline
		Event            & \code{EventExpectation} & \code{ExpectEvent}    \\ 
		Sequential       & \code{Sequence}         & \code{ExpectSequence} \\
		Conjunctive      & \code{Conjunction}      & \code{ExpectAll}      \\
		Disjunctive      & \code{Disjunction}      & \code{ExpectAny}      \\
		Repeating        & \code{Repeat}           & \code{ExpectRepeat}   \\
		Conditional      & \code{Conditional}      & \code{ExpectIf}       \\
		Divergent        & \code{Divergence}       & \code{ExpectOne}      \\
	\end{tabularx}	
\caption{Expectation types and their corresponding class names and convenience methods.}
\label{table:spbas_manual-expectation_types}
\end{table}

To start, we recommend importing all methods statically as demonstrated in listing \ref{code:spbas-importing_NLI}. Now, we can begin to arrange expectations together. Declaring an event expectation involves specification of the expected dialog move, and its source actor. This can be done through multiple variants, all visible in listing \ref{code:spbas-event_usage}. All of \code{Sequence}, \code{Conjunction}, \code{Disjunction}, and \code{Divergence} may have two or more children and therefore share the same usage pattern, listing \ref{code:spbas-conjunction_usage} illustrates it for conjunctive expectations, but the same form applies to the rest. \code{Repeat} is straightforward enough (listing \ref{code:spbas-repeat_usage}), and conditionals are similar to repeat except for an additional condition predicate parameter (listing \ref{code:spbas-conditional_usage}).

\begin{lstlisting}[
float=htbp, 
caption=Importing practice-building convenience methods into scope., 
label=code:spbas-importing_NLI
]
using static Dialog.SPB.Program.NLI.ProgramBuildUtilities;
// All Expect<...> methods are now in scope and ready to be used.
\end{lstlisting}

\begin{lstlisting}[
float=tbp, 
caption=Declaration variants of an event expectation., 
label=code:spbas-event_usage
]
// For zero-target moves:
ExpectEvent(kind: <move kind>, source: <actor>);
// For single-target moves:
ExpectEvent(kind: <move kind>, source: <actor>, target: <actor>);
// For multi-target moves:
ExpectEvent(kind: <move kind>, source: <actor>, targets: <actors>);
// For custom moves:
ExpectEvent(move: <move>, source: <actor>);
\end{lstlisting}

\begin{lstlisting}[
float=htbp, 
caption=Declaration of a conjunctive expectation., 
label=code:spbas-conjunction_usage
]
ExpectAll
(
	"my conjunction",  // Expectations can be assigned titles.
	Expect..., Expect..., Expect...  // List child expectations.
);
\end{lstlisting}

\begin{lstlisting}[
float=htbp, 
caption=Declaration of a repeating expectation., 
label=code:spbas-repeat_usage
]
// Expecting the repeated satisfaction of a single child:
ExpectRepeat(Expect...);
\end{lstlisting}

\begin{lstlisting}[
float=htbp, 
caption=Declaration of a conditional expectation., 
label=code:spbas-conditional_usage
]
var some_flag = false;
ExpectIf(() => some_flag, Expect...);
\end{lstlisting}

One last point to touch on is the ability to associate callbacks with different resolutions of an expectation. This feature can be accessed through any convenience method's optional \code{satisfied}, \code{failed}, and \code{resolved} parameters. Respectively, they can be used to associate pieces of logic with an expectation's satisfaction, failure, or either. An example usage is available for viewing in listing \ref{code:spbas-resolution_callbacks}.

\begin{lstlisting}[
float=htbp, 
caption=Associating a callback with an expectation's resolution status., 
label=code:spbas-resolution_callbacks
]
var some_flag = false;
Expect...(..., satisfied: _ => some_flag = true);
\end{lstlisting}

\pagebreak
\subsection{Action selection and timing}
\label{sec:spbas_manual-usage-action_modules}

Once we have described a social practice in terms of expectations, we pair that description with a specified actor to create a \code{SocialContext}. The actor in question is Self, i.e. the one representing the system itself. The next step is to store this social context as a component of the system's information state (Section \ref{sec:asf-architecture-state}). We do this so it will be accessible to two agency modules: one for action selection and another for timing (\cref{sec:asf-architecture-as,sec:asf-architecture-at}).

The action selection module is represented by the \code{SPBASModule} class (see Section \ref{sec:spba-action_selection} for details of operation), and requires no further setup other than specification of the winner candidate move selection method, whose signature is visible in listing \ref{code:spbas-winner_selection_signature}. Likewise, the action timing module is represented by the \code{SPBATModule} class. Its only input is a mapping between expectations of the social practice and the two sets of interruption rules mentioned in Section \ref{sec:spba-action_timing}.

\begin{lstlisting}[
float=htbp, 
caption=Winner candidate selection method signature., 
label=code:spbas-winner_selection_signature
]
// Produces the index of the winning candidate.
public delegate int SelectionMethod
(
	List<DialogMove> candidates, 
	ImmutableState state
);
\end{lstlisting}

\section{Further documentation}
\label{sec:spbas_manual-further_documentation}

For more detailed documentation, we direct you to the complete reference site accessible through  \code{demo/dialog-spb/doc/html/index.html}. Alternatively, you may directly browse documentation comments in the source files themselves under \code{demo/dialog-spb/library/}.
