\chapter{Communication management system manual}
\label{ch:cms_manual}
\lstset{style=csharp}

This document is an instruction manual for the software library associated with the actor communication management system outlined in Chapter \ref{ch:communication_management_system}. It contains a listing of prerequisites (Section \ref{sec:cms_manual-prerequisites}), installation instructions (Section \ref{sec:cms_manual-installation}), a brief walk-through of principal functionalities (Section \ref{sec:cms_manual-usage}), and pointers to further documentation (Section \ref{sec:cms_manual-further_documentation}). 

\section{Prerequisites}
\label{sec:cms_manual-prerequisites}

\begin{itemize}
	\item \textbf{C Sharp} is the programming language used across the project.
	\item \textbf{Visual Studio} is the integrated development environment used to manage source files and configure builds. Version 2015 and above should be compatible.
\end{itemize}

\section{Installation}
\label{sec:cms_manual-installation}

Currently, we do not host any pre-compiled DLLs for download, so you would want to build the library and its dependencies from source. The relevant directories in the repository are \code{core/dialog-communication/} and \code{core/commons/}. \code{dialog-communication} is the project directory of the system itself, while \code{commons} contains all sorts of utilities which we rely on as a dependency. 

\subsection{Building from source}
\label{sec:cms_manual-installation-from_source}

In order to build all relevant projects from source, we recommend doing the following (all mentioned paths are relative to \code{core/dialog-communication/}):

\begin{enumerate}[noitemsep]
	\item 
		Open the system's test project solution \code{test-dialog-communication.sln} located under \code{test/} in Visual Studio.
	\item 
		Build the solution.
	\item 
		Run all tests present through Visual Studio's test explorer to make sure they pass successfully (optional).
	\item
		Both \code{dialog-communication.dll} as well as \code{commons.dll} should now be available under \code{test/bin/\{Debug|Release\}/}, depending on your desired build configuration. You may now reference both in your own projects as you wish.
\end{enumerate}

\section{Usage}
\label{sec:cms_manual-usage}

General usage of the library follows a three step plan: (1) initialization, (2) actor engagement, and (3) invocation of the global update routine. We cover each of these in order throughout the remainder of this section.

\subsection{Initialization}
\label{sec:cms_manual-usage-initialization}

Recall from Section \ref{sec:cms-architecture} that at the architecture's center is a single object: the communication manager. Also recall from Section \ref{sec:cmd-usage-specification} that the data types a manager is intended to support must be specified during its initialization. Suppose the actors in our scene communicate through two modalities called \code{Foo} and \code{Bar}. In this case, the manager's initialization code would resemble listing \ref{code:cms-manager_initialization}.

\begin{lstlisting}[
	float=hbp, 
	caption=Initializing the manager., 
	label=code:cms-manager_initialization
]
class Foo { ... }
class Bar { ... }

var manager = new CommunicationManager.Builder()
	.Support<Foo>()
	.Support<Bar>()
	.Build();
\end{lstlisting}

\subsection{Actor engagement and data subscription}
\label{sec:cms_manual-usage-actor_engagement}

Once we possess an initialized instance of a manager, we may begin engagement with actors (listing \ref{code:cms-actor_engagement}). Firstly, note that in order for an object to engage with the manager, it must conform to the \code{ManagedActor} interface provided under the \code{Dialog.Communication.Actors} name space. Secondly, notice that an engagement results in a new data subscription being created. This fresh \code{subscription} object is the gateway through which the engaged actor's data preferences may be set.

\begin{lstlisting}[
float=htbp, 
caption=Engaging an actor., 
label=code:cms-actor_engagement
]
class VirtualHuman : ManagedActor { ... }

var alice = new VirtualHuman(...);
DataSubscription subscription = manager.Engage(alice);
\end{lstlisting}

For example, if Alice were interested in communications of type \code{Foo}, she may indicate that in her subscription preferences alongside a callback which is to be invoked once such communications become available for viewing (listing \ref{code:cms-subscription_inclusion}). And, if at some point in the future Alice is no longer interested in data of this type, she is free to alter her subscription (listing \ref{code:cms-subscription_exclusion}) or disengage from the conversation entirely (listing \ref{code:cms-actor_disengagement}).

\begin{lstlisting}[
	float=htbp, 
	caption=Including a data type in a subscription., 
	label=code:cms-subscription_inclusion
]
DataSubscription.Handler<Foo> foo_handler =
	(manager, packets) => { ... };

subscription.Include(foo_handler);
\end{lstlisting}

\begin{lstlisting}[
	float=htbp, 
	caption=Excluding a data type from a subscription., 
	label=code:cms-subscription_exclusion
]
subscription.Exclude<Foo>();
\end{lstlisting}

\begin{lstlisting}[
	float=htbp, 
	caption=Disengaging an actor., 
	label=code:cms-actor_disengagement
]
manager.Disengage(alice);
\end{lstlisting}

\subsection{The update routine}
\label{sec:cms_manual-usage-update_routine}

Given a manager with one or more engaged actors, we may now invoke its global update routine (Algorithm \ref{alg:cms-manager_detailed_update_routine} in Section \ref{sec:cms-architecture-packets_and_channels}) with a single method call, as per listing \ref{code:cms-update_routine_invocation}. At this point, let us assume that an actor, Alice, is engaged with the manager and has the data type \code{Foo} included in her subscription preferences. Once invoked, the update routine will walk Alice through two steps: perception and action. 

\begin{lstlisting}[
	float=htbp, 
	caption=Invoking the update routine., 
	label=code:cms-update_routine_invocation
]
manager.Update();
\end{lstlisting}

Recall from the previous section that beside the type specification itself, Alice also registered a callback method --- called the \textit{subscription handler} --- to be associated with it, and which will now be used during the perception step. A subscription handler's signature can be seen in listing \ref{code:cms-subscription_handler_signature}, and contains three parameters: \code{T}, \code{manager}, and \code{packets} --- with \code{T} being the targeted data type to handle (in our case, \code{Foo}), \code{manager} a reference to the communication manager invoking the handler, and \code{packets} an enumeration of packets carrying data of type \code{T}.

\begin{lstlisting}[
	float=htbp, 
	caption=Subscription handler method signature., 
	label=code:cms-subscription_handler_signature
]
public delegate void Handler<T>
(
	CommunicationManager manager,
	IEnumerable<DataPacket<T>> packets
);
\end{lstlisting}

It is within the subscription handler(s) where the perception step takes place, and hence where actors get the chance to process and update their internal state based on the packets they are presented. Once all engaged actors have had their registered handlers invoked, the manager will commence the second update step: action. \pagebreak

\begin{lstlisting}[
float=htbp, 
caption=Action method signature., 
label=code:cms-act_method_signature
]
public interface ManagedActor
{
	...
	void Act(DataSubmission submission);
}
\end{lstlisting}

The action step is where actors are given the chance to produce their own data they wish be broadcast to others. It takes place within a special method --- \code{Act()} --- that all actors inherit from the \code{ManagedActor} interface (which they all share as per Section \ref{sec:cms_manual-usage-actor_engagement}). The signature of \code{Act()} is visible in listing \ref{code:cms-act_method_signature}, and contains a single parameter: \code{submission}. 

Just as a subscription object is used to regulate data transfer from the manager to actors, so is a submission object used to regulate data transfer from actors to the manager. One by one, the manager invokes \code{Act()} on all engaged actors, and supplies them with a submission object through which they may submit any data they wish. Following up on our example case: if Alice has some data she wishes be broadcast, she may do so by submitting it to the manager during an invocation of her own action method, as seen in listing \ref{code:cms-data_submission}.

\begin{lstlisting}[
	float=htbp, 
	caption=Submitting data., 
	label=code:cms-data_submission
]
// <within Act()>

var some_foo = new Foo();
var some_bar = new Bar();

submission.Add(some_foo);
submission.Add(some_bar);
\end{lstlisting}

\section{Further documentation}
\label{sec:cms_manual-further_documentation}

For more detailed documentation, we direct you to the complete reference site accessible through  \code{core/dialog-communication/doc/html/index.html}. Alternatively, you may directly browse documentation comments in the source files themselves under \code{core/dialog-communication/library/}.
