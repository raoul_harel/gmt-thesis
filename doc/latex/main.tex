\documentclass[11pt, a4paper, oneside, notitlepage]{book}

%%%%%%%%%%%%
% Packages %
%%%%%%%%%%%%

% Language
\usepackage[english]{babel}

% Fonts
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\DeclareUnicodeCharacter{301}{ }

% Math
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
	
% Floats - General 
\usepackage[margin=1cm]{caption}
\usepackage{subcaption}

% Floats - Tables
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage{multirow}

% Floats - Graphics
\usepackage{graphicx}
\usepackage[svgnames]{xcolor}

% Floats - Algorithms
\usepackage{algpseudocode}

\usepackage{etoolbox}
\makeatletter
\patchcmd{\@chapter}% <cmd>
{\chaptermark{#1}}% <search>
{\chaptermark{#1}%
	\addtocontents{loa}{\protect\addvspace{10\p@}}}% replace
{}{}% <success><failure>
\makeatother
\usepackage[chapter]{algorithm}

\algnewcommand\algorithmicforeach{\textbf{for each}}
\algdef{S}[FOR]{ForEach}[1]{\algorithmicforeach\ #1\ \algorithmicdo}

\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

% Quotes

\usepackage{tikz}
\usepackage{framed}

\usepackage{libertine} % or any other font package
\newcommand*\quotefont{\fontfamily{LinuxLibertineT-LF}} % selects Libertine as the quote font

\newcommand*\quotesize{60} % if quote size changes, need a way to make shifts relative
% Make commands for the quotes
\newcommand*{\openquote}
{\tikz[remember picture,overlay,xshift=-4ex,yshift=-2.5ex]
	\node (OQ) {\quotefont\fontsize{\quotesize}{\quotesize}\selectfont``};\kern0pt}

\newcommand*{\closequote}[1]
{\tikz[remember picture,overlay,xshift=4ex,yshift={#1}]
	\node (CQ) {\quotefont\fontsize{\quotesize}{\quotesize}\selectfont''};}

% select a colour for the shading
\colorlet{shadecolor}{White}

\newcommand*\shadedauthorformat{\emph} % define format for the author argument

% Now a command to allow left, right and centre alignment of the author
\newcommand*\authoralign[1]{%
	\if#1l
	\def\authorfill{}\def\quotefill{\hfill}
	\else
	\if#1r
	\def\authorfill{\hfill}\def\quotefill{}
	\else
	\if#1c
	\gdef\authorfill{\hfill}\def\quotefill{\hfill}
	\else\typeout{Invalid option}
	\fi
	\fi
	\fi}
% wrap everything in its own environment which takes one argument (author) and one optional argument
% specifying the alignment [l, r or c]
%
\newenvironment{shadequote}[2][l]%
{\authoralign{#1}
	\ifblank{#2}
	{\def\shadequoteauthor{}\def\yshift{-2ex}\def\quotefill{\hfill}}
	{\def\shadequoteauthor{\par\authorfill\shadedauthorformat{#2}}\def\yshift{2ex}}
	\begin{snugshade}\begin{quote}\openquote}
		{\shadequoteauthor\quotefill\closequote{\yshift}\end{quote}\end{snugshade}}
	
% Header
\usepackage{fancyhdr}

% Bibliography & References
\usepackage{cleveref}
\crefrangelabelformat{section}{#3#1#4--#5\crefstripprefix{#1}{#2}#6}
\crefrangelabelformat{subsection}{#3#1#4--#5\crefstripprefix{#1}{#2}#6}

\usepackage[backend=biber,sortcites=true]{biblatex}
\usepackage[nottoc,numbib]{tocbibind}
\usepackage{nameref}

% Appendices
\usepackage[titletoc]{appendix}

% Misc.
\usepackage{afterpage}
\usepackage{multicol}
\usepackage{enumitem}
\usepackage{csquotes}
\usepackage{listings}
\usepackage{color}

%%%%%%%%%%%%%%%%%%%%%
% Table of Contents %
%%%%%%%%%%%%%%%%%%%%%

\setcounter{tocdepth}{3}

%%%%%%%%
% Code %
%%%%%%%%

\definecolor{bluekeywords}{rgb}{0,0,1}
\definecolor{greencomments}{rgb}{0,0.5,0}
\definecolor{redstrings}{rgb}{0.64,0.08,0.08}
\definecolor{xmlcomments}{rgb}{0.5,0.5,0.5}
\definecolor{types}{rgb}{0.17,0.57,0.68}

\lstdefinestyle{csharp}{
	language=[Sharp]C,
	aboveskip=6mm,
	belowskip=0mm,
	frame=single,
	tabsize=4,
	captionpos=b,
	showspaces=false,
	showtabs=false,
	breaklines=true,
	showstringspaces=false,
	breakatwhitespace=true,
	escapeinside={(*@}{@*)},
	commentstyle=\color{greencomments},
	morekeywords={partial, var, value, get, set},
	keywordstyle=\color{bluekeywords}\ttfamily,
	stringstyle=\color{redstrings}\ttfamily,
	basicstyle=\ttfamily\small
}
\lstdefinestyle{cfg}{
	basicstyle=\ttfamily,
	captionpos=b,
	literate={->}{$\rightarrow$}{2},
	frame=single
}

%%%%%%%%%%
% Layout %
%%%%%%%%%%

\setlength{\hoffset}{-1in}
\setlength{\voffset}{-1in}
\setlength{\topmargin}{2cm}
\setlength{\headheight}{0.5cm}
\setlength{\headsep}{1cm}
\setlength{\oddsidemargin}{3.5cm}
\setlength{\evensidemargin}{3.5cm}
\setlength{\textwidth}{16cm}
\setlength{\textheight}{23.3cm}
\setlength{\footskip}{1.5cm}
\addtolength{\footnotesep}{2mm}

\pagestyle{fancy}
\fancyhf{}

\renewcommand\sectionmark[1]{}
\renewcommand{\chaptermark}[1]{\markboth{}{#1}}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\renewcommand{\baselinestretch}{1.5}

\fancypagestyle{plain}{}
\rfoot{\thepage}

%%%%%%%%%%%%%%%%
% Bibliography %
%%%%%%%%%%%%%%%%

\addbibresource{back/references/tech_demos.bib}
\addbibresource{back/references/video_game_footage.bib}
\addbibresource{back/references/personal_assistants.bib}
\addbibresource{back/references/multi_modal_conversation.bib}
\addbibresource{back/references/social_practice_theory.bib}
\addbibresource{back/references/existing_software.bib}
\addbibresource{back/references/turn_taking.bib}
\addbibresource{back/references/misc.bib}
\addbibresource{back/references/corpora.bib}
\addbibresource{back/references/dialog_management.bib}

%%%%%%%%%%%%%%%%%%%%%%
% Custom Definitions %
%%%%%%%%%%%%%%%%%%%%%%

\def\inSingleQuotes#1{\lq{#1}\rq}

\renewcommand{\texttt}[1]{%
	\begingroup
	\ttfamily
	\begingroup\lccode`~=`/\lowercase{\endgroup\def~}{/\discretionary{}{}{}}%
	\begingroup\lccode`~=`,\lowercase{\endgroup\def~}{,\discretionary{}{}{}}%
	\begingroup\lccode`~=`.\lowercase{\endgroup\def~}{.\discretionary{}{}{}}%
	\catcode`/=\active\catcode`[=\active\catcode`.=\active
	\scantokens{#1\noexpand}%
	\endgroup
}
\newcommand{\code}[1]{\texttt{#1}}

%%%%%%%%%%%%
% Document %
%%%%%%%%%%%%

\begin{document}

	\include{front/title}
	
	\pagestyle{empty}
	\include{front/abstract}
	\include{front/foreword}
	
	\pagestyle{fancy}
	\frontmatter
 	\lhead{}
 	\tableofcontents\textsl{}
	
	\mainmatter
 	\lhead{\rightmark}
 	\include{main/skeleton}
	\lhead{}
	
 	\printbibliography[title=References,heading=bibintoc]
 	%\nocite{*}
	
	\listoffigures
	\listoftables
	\listofalgorithms
	\addcontentsline{toc}{chapter}{List of algorithms}
	
	\begin{appendices}
	\include{back/appendices/cms_manual}
	\include{back/appendices/asf_manual}
	\include{back/appendices/spbas_manual}
	\include{back/appendices/demo_manual}
	\end{appendices}
	
	\backmatter

\end{document}
