\section{Turn-taking}
\label{sec:background-turn_taking}

In this section we briefly survey the principal strategies employed to model the turn-taking aspect of dialog. These methods are divided among three categories: passive-wait, decision-theoretic, and data-driven (\cref{sec:background-turn_taking-passive_methods,sec:background-turn_taking-decision_theoretic_methods,sec:background-turn_taking-data_driven}, respectively). Following the survey, we identify and discuss a common shortcoming of the methods reviewed (Section \ref{sec:background-turn_taking-discussion}) --- one which we address in later chapters.

\subsection{Passive-wait strategies}
\label{sec:background-turn_taking-passive_methods}

A recurring theme in turn-taking models is the desire to avoid overlapping speech. Pragmatically, this makes sense: As it is harder to process, tandem speech undermines one of the principal goals of conversation --- the dissemination of information. Empirical observation confirms this tendency: although varying to some extents depending on culture, overlapping speech is universally the exception rather than the rule \cite{stivers2009universals}. Therefore, a straightforward strategy for overlap avoidance in conversation is to never interrupt and wait until everyone else has concluded their turn before taking ours. The challenge here is recognizing when that is the case, i.e. when does a turn end?

Pauses in speech alone are not a reliable indicator for turn-endings, as they also naturally occur between consecutive utterances or as part of intonation. To that end, passive-wait methods rely on various end-of-turn detection techniques that take into account signals from multiple modalities such as prosody, lexical indicators, eye-gaze, gestures, and others \cite{gravano2009turn, kawahara2012prediction, de2009multimodal, de2006projecting, wilson2005oscillator, mondada2007multimodal}.

Passive-wait methods excel in scenarios where the conversation is largely one-sided: from human to machine. However, in other settings the reluctance to interrupt gives out a cold, unnatural vibe to the artificial party. This because although overlapping speech in human conversation is limited, it is not without its purpose and sometimes it is even desired. For example: when interrupting the current speaker to address a misunderstanding, or when counting down together towards the start of a new year. Therefore, passive-wait methods are too limited to be generally applicable to a dynamic multi-party setting.

\subsection{Decision-theoretic strategies}
\label{sec:background-turn_taking-decision_theoretic_methods}

If we view actors as beings in possession of goals, and their engagement in conversation as a means to meet these goals, then we may transform the problem of turn-taking into a decision-theoretic one. In a decision-theoretic formulation, agents have --- during specific windows in time --- the option to either bid for the dialog floor themselves or surrender it to others. Prevailing methods to make that decision involve either following a static rule-based recipe or consulting a utility cost-function. 

\subsubsection*{Decisions through static rules}

The influential model developed by Sacks, Schegloff, and Jefferson (SSJ) is an example of a rule-based solution to the bid-or-not decision problem. Although it is one of the oldest models and despite being derived from dyadic conversation, it is still widely used as a basis for new turn-taking methods \cite{si2006thespian,jan2007computational}. It can be summarized in the following triplet of guidelines \cite{sacks1974simplest}:

\begin{enumerate}[noitemsep]
	\item The last actor to speak may explicitly allocate the next turn to another party.
	\item If the current speaker does not select a successor, anyone may bid and acquire the floor.
	\item If none of the other participants bids, the last actor to speak may take an additional turn.
\end{enumerate}

This simple answer to the turn-taking problem has its flaws \cite{power1986some}. Firstly, it assumes turns are discrete, and that interruptions or overlapping speech are non-existent: As seen in \cite{jan2007computational}, an attempt is made to combine the SSJ model with cultural parameters and it is found that the observed degree of overlapping speech in Spanish does not agree with the discrete-turns assumption. Secondly, the SSJ model (and derivatives) does not extend to multi-party settings well as it does nothing to address what should be done in a state of simultaneous bids by multiple actors. Thirdly, it assumes perfect content transmission which is not guaranteed in practice: e.g. when the last speaker allocates the next turn to another actor, a misunderstanding might cause a different actor to wrongfully assume it has the floor.

\subsubsection*{Utility-based decision costs} 

Another approach to the turn-taking decision problem is the assignment of a numerical value to each choice representing its utility cost. In this context, \inSingleQuotes{utility} often refers to a quantified contribution of a choice --- either bid or surrender --- to the actor's goals. The simplest cost-functions are nothing but simple heuristics, often tailored for specific conversational scenarios \cite{bohus2011decisions, bohus2011multiparty, selfridge2010importance}. But, more complex functions may incorporate the mental state of an actor, its beliefs about other actors, and even a capability to predict future effects of a given choice on the conversation's state \cite{pynadath2005psychsim, larsson2000information, raux2009finite}.

Utility-based approaches are attractive due to their flexible nature. However, their effectiveness depends entirely on a suitable formulation of the cost-function, which is far from a trivial requirement to satisfy. Naturally, it is in our interest to choose a function that resembles the internal decision making process humans use as closely as possible, however --- as is often the case in such matters --- a boost to realism comes with the price of additional complexity.


\subsection{Data-driven behavior synthesis}
\label{sec:background-turn_taking-data_driven}

Data-driven methods are a popular choice for the simulation of commonly occurring patterns in conversation. They make use of available corpora to infer relevant distribution parameters of important conversational features. In \cite{laskowski2011single} the ICSI meeting corpus is used to train a probabilistic model capable of generating speech/non-speech patterns for multi-party conversation \cite{janin2003icsi}, but is not put to the test in a real-time environment.

Such methods are useful when it is desired to synthesize mock-conversations between virtual characters alone, but are unsuitable for use in interactions involving humans for two reasons: First, the resulting model is dependent on the training-set; and second, it is not flexible enough to handle edge cases. Dependence on the training-set is crippling, since most datasets we have at our disposable are somewhat lacking: They tend to focus on only a narrow set of scenario types, and are often insufficiently annotated \cite{aaac_databases}. Lack of flexibility is another drawback that only manifests itself when humans are involved. That is to say: although the model will behave adequately on average, whenever an exceptional event occurs it will become glaringly obvious to any human observer that the model does not produce realistic behavior.

\subsection{Discussion}
\label{sec:background-turn_taking-discussion}

A common shortcoming of the methods reviewed is that they all operate under the implicit assumption that a turn of dialog has instantaneous duration. To be fair, this assumption is warranted in those applications where the nature of the conversation is either one-sided (human orders/queries machine) or in those that obey an exceedingly strict protocol. As was already mentioned in the introductory chapter, so far most dialog systems were made to operate in exactly these kinds of conditions, and so assuming turns are discrete and without duration did not cause much trouble. 

However, if we wish virtual dialog to play a greater role in more complex, dynamic applications such as those often found in serious-games or real-time discourse of a less-than-formal nature, then this issue must be addressed. If it is not, then it follows that once an actor takes a turn, it proceeds to complete it with no regard to any new events that transpire during it. In other words: Assuming instantaneous turns necessarily implies a refusal to handle interruption --- a big deal in multi-party scenarios, considering that the more actors participate, the more likely it is interruption will materialize. In Chapter \ref{ch:agency_system_framework} we present an agency framework for conversational actors that rectifies this and treats turns as having duration, and in Chapter \ref{ch:spb_agency} we use it to propose one approach towards the handling of interruption.
