\section{Role of social practices}
\label{sec:results-social_practices}

\algnewcommand{\ExpectEvent}[3]{\State \textbf{expect } #1 $:$ #2 $\rightarrow \{$#3$\}$}

\algblockdefx[Sequence]{ExpectSequence}{EndSequence}%
	{\textbf{expect sequence}}{\textbf{end sequence}}
\algblockdefx[Conjunction]{ExpectConjunction}{EndConjunction}%
	{\textbf{expect all}}{\textbf{end conjunction}}
\algblockdefx[Disjunction]{ExpectDisjunction}{EndDisjunction}%
	{\textbf{expect any}}{\textbf{end disjunction}}
\algblockdefx[Divergence]{ExpectDivergence}{EndDivergence}%
	{\textbf{expect one of}}{\textbf{end divergence}}
\algblockdefx[Repeat]{ExpectRepeat}{EndRepeat}%
	{\textbf{expect repeat}}{\textbf{end repeat}}
\algblockdefx[Conditional]{ExpectConditional}{EndConditional}%
	[1]{\textbf{expect if} #1}{\textbf{end conditional}}


Recall from Chapter \ref{ch:spb_agency} that a social practice description $\rho$ is defined as a three-tuple $(A, M, X)$, where $A$ is the set of participating actors, $M$ the set of possible dialog moves, and $X$ an expectation arrangement made up of events, sequences, conjunctions/disjunctions, and so on... To demonstrate how these components manifest themselves in a live application, we use this section as a walkthrough of the practice description for our couples-therapy scenario.

First things first: let $A = \{\text{PatientA}, \text{ PatientB}, \text{ Therapist}\}$ (in the application itself, the patient couple are named Alice and Bob while Charlie is the player-controlled therapist). Next, let $M$ be the collection of dialog moves listed in Table \ref{table:results-dialog_moves} (with the implicit addition of the universal idle move):

\input{main/results/tables/dialog_moves}

Next, we elaborate on the structure of $X$ --- the practice's expectation arrangement. In the interest of brevity, let us denote an event (a realization of a move, see Section \ref{sec:spba-describing_social_practices-expectations}) with source $s$, move $m$, and target set $T = \{t_1, t_2, \ldots\}$ as $s: m \rightarrow T$. Now, we can begin our description of the couples-therapy scenario in terms of social expectations.

\subsection{Expectation arrangement}
\label{sec:results-social_practices-expectations}

Begin with the arrangement's root node --- call it Session --- which is a sequential expectation segmenting the entire interaction into three parts: Greetings, Counseling, and Goodbyes (Algorithm \ref{alg:results-node_session}). Greetings and Goodbyes are both conjunctive expectations, each detailing an exchange of respectively a Greeting/Goodbye between each patient and the therapist (Algorithm \ref{alg:results-node_greetings}). We use a conjunction here to signal that it is not important in what order the moves are exchanged, as long they all do.

\input{main/results/algorithms/node_session}
\input{main/results/algorithms/node_greetings}

The Counseling expectation is a bit more complex. Within it, we wish to represent the following interaction: First, the therapist invites one of the patients to share an issue he/she is having with their partner. Then, the patient either accepts the invitation and elaborates on an issue, or declines. This process repeats until either all patients decline any further discussion of issues or the therapist decides it is time to end the counseling session. Algorithm \ref{alg:results-node_counseling} shows how a disjunction between the SessionClosing event and the IssueDiscussion cycle signals that the therapist may end the session at any time, and how a divergence is used to ensure exactly one issue discussion is ongoing at a time.

\input{main/results/algorithms/node_counseling}

In addition to the therapist closing the session, we still need to model the case where neither patient wishes to discuss issues any further. To do this, we wrap the contents of any IssueDiscussion with a conditional expectation referencing a special bit-flag: $\text{open}(p)$, indicating the a patient $p$ is open to discussion of his/her issues. Initially, $\text{open}(p) = true$ for all patients, and is only set to $false$ when a patient $p$ declines an invitation to discuss an issue. This interaction pattern is visible in Algorithm \ref{alg:results-node_issue_discussion}, while the details of acceptance/declination of an IssueSharingInvitation are listed in algorithms \ref{alg:results-node_issue_sharing_acceptance} and \ref{alg:results-node_issue_sharing_declination}.

\input{main/results/algorithms/node_issue_discussion}
\input{main/results/algorithms/node_issue_sharing_acceptance}
\input{main/results/algorithms/node_issue_sharing_declination}

\subsection{Interruption rules}
\label{sec:results-social_practices-interruption}

Accompanying the expectation arrangement from the previous section are sets of interruption rules, each associated with one or more expectations. To keep things simple, we limit such associations to the higher-level nodes of the practice description: Greetings, Goodbyes, and Counseling. These rules affect the artificially-driven actors in the scene exclusively, while the player retains his/her freedom to perform dialog moves without delay and at any point in time.

The rules governing Greetings and Goodbyes represent indifference to conflict (Table \ref{table:results-interruption_rules-conflict_indifference}), that is to say actors perform their target moves immediately and to completion. On the other hand, rules governing the Counseling phase represent conflict-avoiding behavior, where actors avoid interrupting others and surrender the floor when they are interrupted themselves (Table \ref{table:results-interruption_rules-conflict_avoidance}). In effect, this rule assignment allows for overlapping speech during the socially-lax parts of the interaction (greetings and goodbyes), but implies a more submissive stance from patients towards the therapist during actual counseling.

\input{main/results/tables/interruption_rules}
