\section{Overview}
\label{sec:results-overview}

In this section we provide a short description of our chosen simulation scenario (Section \ref{sec:results-overview-scenario_description}) and familiarize ourselves with the application's general structure (Section \ref{sec:results-overview-architecture}).

\subsection{Scenario description}
\label{sec:results-overview-scenario_description}

The scenario we chose to simulate is that of a short couples-therapy session with three participating actors: one therapist and a patient couple. We note the following three attributes of this particular setting as the principal motivators for our choice: (1) It contains the minimal number of actors required for an interaction to be considered multi-party; (2) the set of participating actors is clearly heterogeneous, that is to say not all actors fulfill the same role; and (3) it involves an interaction whose flow is --- relative to most day-to-day conversations --- quite strict and therefore more easily expressed using social practices. 

In our particular implementation, the patient couple behavior is driven artificially, while the therapist is controlled by a human player. This choice was made bearing in mind that one of our main goals for multi-party simulation is the training of medical professionals in dealing with tough, emotionally-charged conversations.

\subsection{Application architecture}
\label{sec:results-overview-architecture}

To realize the chosen simulation scenario, we make use of the tools and methods developed in \cref{%
	ch:communication_management_system,%
	ch:agency_system_framework,%
	ch:spb_agency}. Figure \ref{fig:results-architecture_overview} illustrates how they all relate and interact with each other, and also where the human player fits in.

\input{main/results/figures/architecture_overview}

At the heart of the application is an instance of the communication management system (CMS) from Chapter \ref{ch:communication_management_system}. Whenever an actor outputs some data representing its behavior, that data is submitted to the CMS, which synchronizes its broadcast to all other engaged actors. In our case, these are the two artificially-driven patient couple, the player-controlled therapist, and a silent \textit{eavesdropper} created by the application's user-interface (UI) component to listen in on the conversation so that it may be visualized on the screen.

All actors besides the UI's eavesdropper have their behavior influenced by a single social practice description. It details the expected content and timing of actions for each actor in a couples-therapy session, and is expressed using the concepts and methods from Chapter \ref{ch:spb_agency}. 

Actors themselves are internally structured according to the agency process from Chapter \ref{ch:agency_system_framework}. Recall that it consists of one information state and six modules dividing the process into three stages: (1) perception, (2) deliberation, and (3) action. In our simulation, perception-stage modules are fed behavior-encoding data from the CMS, and extract from it information about the state of other actors and their actions. Next, modules of the deliberation stage use the processed data to update the actor's internal copy of the practice description and to re-evaluate the resolution status of the expectations it contains. Once done, an appropriate action is selected, and when the time is right also converted to behavior by action-stage modules. Finally, the produced output is submitted to the CMS for dissemination.

We use the next three \cref{sec:results-cms,sec:results-agency,sec:results-social_practices} to expand on the role of each of the three major components mentioned, respectively: the communication management system, the agency process of different actors, and the scenario's social practice.
