\chapter{Conclusion}
\label{ch:conclusion}

In this brief closing chapter we look back at our original objectives for this research (Section \ref{sec:conclusion-research_objectives}) to evaluate where we succeeded and where we failed, and entertain ideas for future extensions of our work (Section \ref{sec:conclusion-future-work}).

\section{Reflecting on research objectives}
\label{sec:conclusion-research_objectives}

Recall our main research objective from Chapter \ref{ch:introduction}, reproduced below:

\begin{shadequote}{}
	\textit{Development of a \textbf{robust}, \textbf{generic} procedure for the construction of \textbf{multi-modal}, \textbf{multi-party} virtual conversation simulations.}
\end{shadequote}

\noindent
The multi-modal part of the objective refers to the interface through which data representing the different modalities in a scene is collected from one actor and redistributed to the rest. We believe the communication management system from Chapter \ref{ch:communication_management_system} is an adequate fulfillment of this aspect. It supports an arbitrary amount of participating actors and modalities. Plus, it is flexible enough to accommodate both turn-based as well as real-time applications.

In addition to the communication management system, the multi-party component of the objective is also covered by the agency system framework from Chapter \ref{ch:agency_system_framework}. Therein, we extract and package the domain-independent parts of virtual conversation in the form of several manageable modules, each with clearly defined input/output and responsibilities. To thoroughly and more accurately evaluate its usefulness additional experimentation is needed, but from our limited practical experience so far (in \cref{ch:spb_agency,ch:results}), the core goals have been met.

To solidify robustness and generality, in Chapter \ref{ch:spb_agency} we develop a new method for action selection/timing. By using aspects of social practice as our modeling tools, we presume this new approach is both more scalable and intuitive to work with than prevalent information-state-based ones, such as Trindikit (see Section \ref{sec:background-dialog_management}) \cite{larsson2000information}. Currently, we do not believe we have accumulated sufficient practical experience to determine whether this presumption is vindicated, but from what little we do have (Section \ref{sec:results-social_practices}) there is definite potential.

\section{Directions for future work}
\label{sec:conclusion-future-work}

The most pressing issue to be investigated is how well the developed tools and methods fair in practical applications. In Chapter \ref{ch:results} we build a small demo to showcase a proof-of-concept, but it is undoubtable that there is room for amendments to both the theoretical models used and their implementation. To better assess what specificities need adjustment, more comprehensive simulations for the envisioned target domain --- virtual conversation in entertainment, education, and training of professionals --- need to be developed and assessed.

Speaking of assessment, the evaluation of dialog systems is a notoriously difficult task. Ideally, the feedback received for a given iteration of a system in development should be as accurate as possible. Additionally, the stretch of time between a request for feedback and its reception should be as short as possible. However, these two requirements are hard to satisfy at once: If obtained through user studies, evaluation results are more accurate but take longer time and effort to compile. On the other hand, using automated tests and heuristics the delay in feedback becomes negligible but at the same time output is much harder to analyze than a human-written account of the system's performance. Any progress in this area is sorely needed.

The above concludes what we perceive to be the technically-oriented directions for future work. As for ones which are more exploratory in nature, we point to the social-practice-based approach to agency from Chapter \ref{ch:spb_agency}. We believe it has substantial growth potential, particularly in the refinement of existing social expectation types and addition of new ones (see Section \ref{sec:spba-discussion} for details). Once again, the only sensible way forward here is relentless experimentation and accumulation of practical experience.
