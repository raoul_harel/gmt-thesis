\section{Selecting action}
\label{sec:spba-action_selection}

In the previous Section \ref{sec:spba-describing_social_practices} we define social practices using a three-tuple $\rho = (A, M, X)$, where $A$ is the set of participating actors, $M$ the set of possible dialog moves, and $X$ a program expressed in a domain-specific language made up of social expectations. In this section we show how to derive action from such descriptions of practices.

When deriving action from a practice $\rho$, it is always with respect to one actor $a \in A$. As an ongoing interaction progresses, expectations in $X$ undergo changes of state including both their relevance and resolution (Section \ref{sec:spba-describing_social_practices-expectation_state}). By going through $X$'s expectation tree and looking for active nodes, we can collect a set of events $E = \{e_1, e_2, \ldots, e_n\}$ that $\rho$ implies are currently expected to be performed by members of $A$. We call $E$ the \textit{expected event set}. To extract from $E$ those events which are relevant for the specific actor in our query, we apply a filter yielding: $E_a = \{e \mid e \in E,\, e\text{'s source is } a\}$. From here, we define the \textit{candidate dialog move set} for $a$: 
\begin{equation*}
M_a = 
\begin{cases}
	\{m \mid m \text{ is referenced by some } e \in E_a\}, & \text{if}\ |E_a| \ne 0 \\
	\{m_0\}, & \text{otherwise ($m_0$ being the idle move)}
\end{cases}
\end{equation*}

\noindent
Be aware: this assumes that nothing is barring an actor from realizing any of the candidate moves at this point in time. This assumption generally works when the dialog moves in question do not require interaction with the environment, however in practice that may not always be the case (e.g. if a move realization requires a prop that is not currently in the actor's possession).

The next step is to pick one target move to perform out of the candidate set. When $|M_a| = 1$ this is trivial, but what about the case where $|M_a| \ge 2$ ? It seems we are back to square one, because once again we are faced with the same problem we set out to solve: How should one go about selecting a target move out of a given set? However, even though we did not achieve a general answer to the question, we did in fact manage to use social practices to significantly prune the search space; Recall that the set being referred to in the original problem was in fact $M$ --- the entire collection of available moves --- whereas from our experimental experience so far (in Chapter \ref{ch:results}), we gather that in practical applications $M_a$ represents only a very small subset of that. 
