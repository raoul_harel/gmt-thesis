\section{Describing social practices}
\label{sec:spba-describing_social_practices}

In this section we introduce an expectation-based model as our approach to the formal description of (conversational) social practices. First, we define exactly what we mean by the words \inSingleQuotes{practice} and \inSingleQuotes{expectation}, make a distinction between several kinds of expectations, and demonstrate how they may be combined to describe whole practices (Section \ref{sec:spba-describing_social_practices-expectations}). Then, we explain how the dynamic progression of a practice can be tracked throughout an ongoing interaction (Section \ref{sec:spba-describing_social_practices-expectation_state}), and discuss in detail how that idea applies to each kind of expectation differently (%
\cref{sec:spba-describing_social_practices-event_expectation,%
	sec:spba-describing_social_practices-sequential_expectation,%
  	  sec:spba-describing_social_practices-conjunctive_expectation,%
	  sec:spba-describing_social_practices-disjunctive_expectation,%
	  sec:spba-describing_social_practices-divergent_expectation,%
	  sec:spba-describing_social_practices-repeating_expectation,%
	  sec:spba-describing_social_practices-conditional_expectation}).

\subsection{Practices and expectations}
\label{sec:spba-describing_social_practices-expectations}

In the context of conversation, we use the term \textit{practice} to denote a socially-mandated pattern of interaction. For example: actors mutually greeting each other at the onset of dialog is one of the most basic instances of such a pattern. And even though practices may vary in complexity, context, and frequency of occurrence, their common denominator is their assignment of various \textit{expectations} at different times to either individual actors or groups.

Each and every expectation falls under one of two kinds: either \textit{atomic} or \textit{composite}. Atomic expectations predict the realization of concrete dialog moves by specific actors. We refer to the actor performing a move as its \textit{source}, and to the pairing of a dialog move with its source as an \textit{event}. For this reason, we use the terms event/atomic expectation interchangeably in the remainder of this chapter. Event expectations are specified further in Section \ref{sec:spba-describing_social_practices-event_expectation}.

In contrast with atomics, composites serve as containers for other expectations. We use the term \textit{parent} to refer to the composite itself and \textit{children} to refer to the collection of expectations it contains. Composites are used to indicate some temporal relationship amongst their children, and/or to impose logical constraint(s) upon them. In this chapter, we define six types of composite expectations: \textit{sequential}, \textit{conjunctive}, \textit{disjunctive}, \textit{divergent}, \textit{repeating}, and \textit{conditional}. These are specified further in 
\cref{sec:spba-describing_social_practices-sequential_expectation,%
      sec:spba-describing_social_practices-conjunctive_expectation,%
 	  sec:spba-describing_social_practices-disjunctive_expectation,%
      sec:spba-describing_social_practices-divergent_expectation,%
      sec:spba-describing_social_practices-repeating_expectation,%
      sec:spba-describing_social_practices-conditional_expectation},
and an overview of the context-free grammar governing their arrangement (and including atomics) is available for viewing in Figure \ref{fig:spba-expectation_cfg}.

Using expectations and events, an instance of a practice can be formally represented by a three-tuple $\rho = (A, M, X)$, where $A$ is the set of participating actors, $M$ the set of possible dialog moves, and $X$ an expectation-arrangement \textit{program} recognized by the language defined in Figure \ref{fig:spba-expectation_cfg}. $X$ itself is a single (composite) expectation that serves as the root ancestor of all others, and whose descendant atomics all exclusively refer to events in $A \times M$.

\input{main/social_practice_based_agency/figures/expectation_dsl_cfg}

\subsection{Expectation relevance and resolution}
\label{sec:spba-describing_social_practices-expectation_state}

Before we move on to discuss specific types of expectation in detail, we want to first be able to relate a practice's expectation arrangement $X$ to the dynamic progression of an ongoing interaction. To do this, we augment expectations of $X$ with two kinds of state-annotation: \textit{relevance} and \textit{resolution}.

Relevance refers to the applicability of a given expectation to a given time. Remember: composite expectations such as $X$ expresses a temporal relation between their children; This means that throughout the course of an interaction different expectations may be applicable at different times. We label those expectations that are currently applicable as \textit{relevant/active} and those that are not as \textit{irrelevant/inactive}. Note that the children of a composite expectation do not necessarily inherit its relevance status, but we defer that discussion until later sections, where we detail the behavior of individual expectation types themselves.

Accompanying the annotation for relevance is another for resolution. Recall that expectations are merely a human-friendly way for representing patterns of interaction. In this context, we view interaction as a sequence of events $I = (e_1, e_2, \ldots)$ that take place while the expectation is relevant; The expectation itself can be regarded as an implicit definition of a set $E = \{I_1, I_2, \ldots\}$ whose members include all possible interactions that adhere to the constraints imposed by that particular expectation. At the onset of interaction $I = \varnothing$, and proceeds to grow as new events take place. The expectation's resolution status then answers the following question: Will it ever be the case that $I \in E$? If it is already the case that $I \in E$, then the answer is positive and we resolve the expectation with \textit{satisfaction}. On the other hand, if no member of $E$ is prefixed by $I$ then it is certain that $I \notin E$ and we resolve the expectation with \textit{failure}. We label expectations that are either satisfied or failed as \textit{resolved}, and those that are neither as \textit{unresolved/pending}. 

In the upcoming \cref{sec:spba-describing_social_practices-event_expectation,%
	sec:spba-describing_social_practices-sequential_expectation,%
	sec:spba-describing_social_practices-conjunctive_expectation,%
	sec:spba-describing_social_practices-disjunctive_expectation,%
	sec:spba-describing_social_practices-divergent_expectation,%
	sec:spba-describing_social_practices-repeating_expectation,%
	sec:spba-describing_social_practices-conditional_expectation}
we go over individual expectation types in detail, and for composites we also specify how the relevance and resolution status of the parent expectation relates to that of its children.

\subsection{Event expectations}
\label{sec:spba-describing_social_practices-event_expectation}

Event expectations (also called atomics) represent a pattern of interaction containing a concrete event $e$, and serve as the basic building block for more complex composite expectations. Uniquely, this type of expectation cannot resolve as a failure, but rather remains pending until eventually becoming satisfied once $e$ takes place.

\subsection{Sequential expectations}
\label{sec:spba-describing_social_practices-sequential_expectation}

A sequential expectation $p$ represents a pattern of interaction wherein a sequence of $n \ge 2$ child expectations $(x_1, x_2, \ldots, x_n)$ is satisfied one child at a time in the specified order. Consequently, at most a single child is active at a time. Let us identify the active child expectation by its index $i$ in the sequence. At the onset of interaction $i = 1$, and it remains that way for as long as $x_i$ is pending resolution. Once $x_i$ is resolved, one of two scenarios unfolds: If $x_i$ failed, then $p$ fails as well. Otherwise, $i$ is incremented by one unless $i = n$, in which case we have satisfied the entire sequence and therefore also $p$ itself. This procedure is defined in Algorithm \ref{alg:spba-sequential_expectation} and illustrated by Figure \ref{fig:spba-sequential_expectation}.

\input{main/social_practice_based_agency/algorithms/sequential_expectation}
\input{main/social_practice_based_agency/figures/sequential_expectation}

\subsection{Conjunctive expectations}
\label{sec:spba-describing_social_practices-conjunctive_expectation}

A conjunctive expectation $p$ represents a pattern of interaction wherein $n \ge 2$ child expectations forming a set $\{x_1, x_2, \ldots, x_n\}$ are satisfied in any order. That is to say: all children are activated at the onset of interaction, and remain so until resolved. If any child fails, then $p$ fails, and only once all children have been satisfied will $p$ be satisfied. This procedure is defined in Algorithm \ref{alg:spba-conjunctive_expectation} and illustrated by Figure \ref{fig:spba-conjunctive_expectation}.

\input{main/social_practice_based_agency/algorithms/conjunctive_expectation}
\input{main/social_practice_based_agency/figures/conjunctive_expectation}

\subsection{Disjunctive expectations}
\label{sec:spba-describing_social_practices-disjunctive_expectation}

A disjunctive expectation $p$ represents a pattern of interaction wherein at least one of a set of $n \ge 2$ child expectations $\{x_1, x_2, \ldots, x_n\}$ is satisfied. That is to say: all children are activated at the onset of interaction, and as soon as any one of them is satisfied so will be $p$. The only case where $p$ fails is when all children fail. This procedure is defined in Algorithm \ref{alg:spba-disjunctive_expectation} and illustrated by Figure \ref{fig:spba-disjunctive_expectation}.

\input{main/social_practice_based_agency/algorithms/disjunctive_expectation}
\input{main/social_practice_based_agency/figures/disjunctive_expectation}

\subsection{Repeating expectations}
\label{sec:spba-describing_social_practices-repeating_expectation}

A repeating expectation $p$ represents a pattern of interaction wherein a single child expectation $x$ is satisfied zero or more times. Consequently, this type of expectation cannot be satisfied, but rather remains perpetually pending unless there comes a time where $x$ fails. This procedure is defined in Algorithm \ref{alg:spba-repeating_expectation}.

Repeating expectations can be combined with sequential ones to set the minimum number $m$ of repetitions expected. This can be achieved by defining a sequential expectation whose first $m$ children are $x$ and whose last child is $p$. Likewise, to set an upper limit $n$ on the number of repetitions, one could define a disjunction with two children where one of them is $p$ and the other a sequence containing $n$ times $x$. Lastly, in order to set both lower and upper limits, one may use a sequence whose first $m$ children are $x$, and whose last child is a disjunction between $p$ and another sequence containing $n - m$ times $x$.

\input{main/social_practice_based_agency/algorithms/repeating_expectation}

\subsection{Conditional expectations}
\label{sec:spba-describing_social_practices-conditional_expectation}

A conditional expectation $p$ represents a pattern of interaction wherein a single child expectation $x$ is satisfied, but is only activated while a specified predicate $C(\ldots)$ evaluates to \textit{true}. In other words, $p$ remains pending while $C(\ldots) = false$, and is assigned the same resolution status as $x$ otherwise. This procedure is defined in Algorithm \ref{alg:spba-conditional_expectation}.

\input{main/social_practice_based_agency/algorithms/conditional_expectation}

\subsection{Divergent expectations}
\label{sec:spba-describing_social_practices-divergent_expectation}

A divergent expectation $p$ represents a pattern of interaction wherein exactly one out of a set of $n \ge 2$ child expectations $\{x_1, x_2, \ldots, x_n\}$ is selected to be satisfied. However, before we could explain the selection procedure we must first introduce a new attribute of composite expectations: \textit{scope carriers}.

When a composite expectation has exactly one child currently active, this child is said to be its parent's scope carrier. It is so termed because the resolution status of both child and parent is dependent on the same scope: the child's. Among composites defined in this chapter so far, the following posses scope-carrier children: Sequences, repetitions, and possibly conditionals (when their predicate is \textit{true}). When a given expectation $x$ has scope carrier $s(x)$, that child may also in turn have its own scope carrier $s(s(x))$ and so on. We call the sequence $(x, s(x), s(s(x)), \ldots)$ the \textit{scope carrier chain} of $x$, with $x$ itself at the head and $x$'s most distant scope-carrying descendant --- call it $\hat{s}(x)$ --- at the end. We can now define the aforementioned divergent child selection procedure using $\hat{s}$.

At the onset of interaction, $p$ behaves in an identical manner to that of a disjunction, and continues to do so until $\hat{s}(x_i)$ is satisfied for some $i \in [1, n]$. At that point, all children are deactivated except for $x_i$ (which --- by the way --- causes $s(p) = x_i$). From then on, $p$'s resolution status is assigned the same value as that of $x_i$. This procedure is defined in Algorithm \ref{alg:spba-divergent_expectation} and illustrated by Figure \ref{fig:spba-divergent_expectation}.

Ultimately, the divergent child selection procedure comes down to detecting the first child which has undergone some sort of partial satisfaction. Initially, we entertained the idea of defining partial satisfaction of an expectation $x$ to mean the satisfaction of either $x$ or any of its descendants. However, we were afraid that --- due to the potential of some expectations' descendant set to be quite large --- it might become difficult to reason about and/or hold a complete mental picture of a divergence. For this reason, we came up with the concept of scope carriers to restrain the number of expectations to be considered. We figure that further experimentation and practical experience is needed before a more definitive verdict could be issued on this matter. 

\input{main/social_practice_based_agency/algorithms/divergent_expectation}
\input{main/social_practice_based_agency/figures/divergent_expectation}
