\section{Domain overview}
\label{sec:asf-overview}

In this section we gradually map out an abstraction of what we call \inSingleQuotes{the agency process} --- the process that governs an actor's entire capacity to act. That is, everything beginning at the perception of input and up to and including the production of output. We begin with an overview of an actor as regarded from the outside (Section \ref{sec:asf-overview-actors}), then segue into a brief discussion about actor behavior as carrier of meanings (Section \ref{sec:asf-overview-dialog_moves}), and finally describe in detail the three principal stages that make up the agency process itself (\cref{sec:asf-overview-perception,sec:asf-overview-deliberation,sec:asf-overview-action}).

\subsection{Actors and agency}
\label{sec:asf-overview-actors}

We view actors as stateful entities hosting a capacity for cognition within them. Their cognitive process takes on input from the external environment they are embedded in, and pairs it with their current state in order to produce a new state that reflects absorption of that new information. Then, as a final step the process produces action based on that new state as output to the environment.

If we were to condense this description into its bare essentials, we would say it consists of three stages: (1) perception, (2) deliberation, and (3) action \cite{rao1995bdi,franklin1996agent}. When discussing conversational actors specifically, a description of these components may be understood to mean, respectively: (1) the perception of other actors' behavior, (2) an update of the actor's own internal state, leading to selection of an action as response, and (3) the timing and realization of said response --- again, through behavior. We call this routine \inSingleQuotes{the agency process}, because it is both a sufficient and necessary account for an intelligent actor's capacity to act (Figure \ref{fig:asf-actors_and_agency}). For discussion about our proposed mechanism for bridging the gap between one actor's action and its perception by another, see Chapter \ref{ch:communication_management_system}. This chapter however is dedicated to the modeling of the agency process itself. 

\input{main/agency_system_framework/figures/actors_and_agency}

\subsection{Meanings and dialog moves}
\label{sec:asf-overview-dialog_moves}

There is a distinction to be made between a communicated intention and the behavior expressing it. Even though behavior is the concrete element flowing in and out of the agency process on both ends, it is not the element we wish to reason about internally within it. Instead, we much rather reason about the semantic content behavior carries: \textit{meanings} \cite{van2012agent}.

The reason for our added interest in meanings is that any discussion about behavior itself will always be more context-dependent, more restricted in scope, and more limited in application. Put another way: meaning is a constant, and behavior is its variable manifestation whose exact form depends on circumstance. 

Therefore, in order to minimize such a dependence and as we strive to have any further discussion be applicable to as wide a set of circumstances as possible, we shall shift our focus almost exclusively to the role of meanings rather than behavior from now on. 

In order to facilitate further discussion about meanings and their role, we label the act of communicating a meaning through behavior as the realization of a so-called \textit{dialog move}. We view actors as producers of a constant stream of dialog moves --- from the moment they enter a conversation and up to the moment of exit. Furthermore, since even ``idle'' behavior has a communicative quality to it and therefore must be regarded as a move in and of itself, we regard such a stream of moves to be without gaps. This means that it is impossible for an actor to \textit{not} be realizing any moves for as long as it is engaged in dialog.

Another observation we wish to make refers to the existence of an important universal move: the \textit{idle} move. It is readily apparent that this is the move we naturally default to. Therefore, even though the set of moves available for an actor may be heavily dependent upon scenario specificities, it is guaranteed that the idle move would always have its unique role to play in any setting.

Armed with dialog moves as an umbrella term to denote all actions perceived/produced by actors, we are now in a position to incorporate them in our description of the agency process, which now would read as: (1) the interpretation of perceived behavior as the realization of dialog moves, (2) an update of the state and selection of a target move to make in response, and (3) timing and realization of that target move through behavior (Figure \ref{fig:asf-agency_process}).

\input{main/agency_system_framework/figures/agency_process}

We elaborate on each of the main three stages that make up the agency process --- perception, deliberation, and action --- in that order and within the next three \cref{%
	sec:asf-overview-perception,%
	sec:asf-overview-deliberation,%
	sec:asf-overview-action%
}.

\subsection{Perception stage of agency}
\label{sec:asf-overview-perception}

Perceiving the behavior of other actors is a task we consider to be comprised of two temporally-related components: (1) perception of past recent activity up to but not including the present, and (2) perception of current activity.

In terms of dialog moves, recent activity perception translates to perceiving those moves that have taken place in the span of time beginning at the conclusion of the latest previous iteration of the agency process and ending at the onset of its current one.

As for current activity: even though we would have liked to be able to know what moves are in the process of being realized at present, we cannot. This is so because the act of realizing a dialog move is not an instantaneous event, but rather an eventual one. This entails an inconvenient consequence: we are not allowed to assume we know what dialog move is taking place until its realization is complete. However, there is at least one important exception to this rule: the idle move.

Unlike most moves, we consider the idle move (Section \ref{sec:asf-overview-dialog_moves}) to be realizable in an instant. This enables us to immediately recognize the difference between an actor realizing it and one that does not --- a vital piece of information that becomes particularly useful when deciding on the timing of an actor's own action. Also note that this simplification is not without justification; we are hard-pressed to come up with any practical scenario where an actor does not possess such an ability.

And so, the perception stage of the agency process is one that accomplishes a conversion of perceived behavior into two forms of output: dialog moves representing recent activity, and a classification of actors into passive (idle) and active (non-idle) categories representing current activity (Figure \ref{fig:asf-agency_perception_stage}). Both outputs are then fed into the deliberation stage, which is discussed in the next section.

\input{main/agency_system_framework/figures/agency_perception_stage}

\subsection{Deliberation stage of agency}
\label{sec:asf-overview-deliberation}

The deliberation stage of the agency process is where an actor translates perceived information into mutations of its state, and then uses that new state to select an action to be made.

Updating of the state is an operation that takes on three inputs. Two of which are in fact the output of the perception stage (Section \ref{sec:asf-overview-perception}), representing recent and current perceived activity of outside actors. The third input is implicitly known by the actor itself, as it is a snapshot of the actor's own activity as expressed by three dialog moves. 

The three moves that make up an actor's own activity snapshot are termed: the \textit{recent move}, \textit{target move}, and \textit{actual move}. The recent move refers to the latest move the actor has completely realized, the target move to the move the actor wishes to realize currently, and the actual move to the move the actor is actually in the process of realizing.

Proceeding the update, the first operation  to make use of the new state is the one responsible for action selection. Here, the actor determines what move ought to become its new target move for the duration of the current agency process iteration.

To summarize: the deliberation stage takes into account activity both externally perceived as well as of the self, uses it to determine and perform updates to the state, and then bases its selection of a new target move on  that new state (Figure \ref{fig:asf-agency_deliberation_stage}). Once selected, the target move is forwarded to the action stage where it may or may not be realized. Details are in the upcoming section. 

\input{main/agency_system_framework/figures/agency_deliberation_stage}

\subsection{Action stage of agency}
\label{sec:asf-overview-action}

The action stage is where the decision is made on whether or not to realize the actor's target dialog move, and in the case where it is to be realized, this stage is also the one responsible for the move's expression through output behavior.

Were the realization of a move been an instantaneous event, we would need only deal with a single issue, namely: the recognition of an appropriate moment to execute it. However, seeing as that is not the case and that realization of moves is eventual, not only must we concern ourselves with finding appropriate moments for its initiation, but also recognition of times where it is more appropriate to outright cancel a move in progress. 

To elaborate on that point: As the realization of a dialog move does not interfere with an actor's ability to perceive and process new information, both procedures may execute in tandem. Then, it might so happen that new information acquired yields a change of state which causes the move in progress to lose its status as an appropriate one. In some cases, the loss may be due to bad timing, and so an actor must halt and wait for the next opportune moment to re-initiate the move. In others, the actor's target move may have suddenly changed, in which case the actor must abandon its actual move altogether. Either way, cancellation is the result of a mismatch between what an actor thinks it ought to do and that which it is doing currently. 

More formally: Let $M = \{m_0, m_1, m_2, \ldots, m_n\}$ be the set of all available dialog moves, with $m_0$ being the special \textit{idle} move --- indicating inaction --- and $\{m_1, m_2, m_3, \ldots, m_n\}$ remaining arbitrary for now. In addition, let $m_t \in M$ refer to the \textit{target} move an actor wishes to realize at this time, and $m_a \in M$ refer to the \textit{actual} move currently being realized. Lastly, define the predicate $AT(m)$ to be true if and only if $m \in M$ is an appropriate move to make at this time; a proposition whose truth value is evaluated using a combination of the parameter $m$ as well as the actor's state. Given that, we are now able to frame an actor's timing of moves in the intuitive routine listed in Algorithm \ref{alg:asf-action_timing_routine}.

\input{main/agency_system_framework/tables/action_timing_routine_symbols}
\input{main/agency_system_framework/algorithms/action_timing_routine}

The routine's principal role is to both detect and reconcile the aforementioned disparity between intention and actuality, whenever it occurs. In other words, it ensures that an actor either executes the target move when appropriate (i.e. $m_a = m_t$ when $AT(m_t)$ evaluates to true) or nothing at all (i.e. $m_a = m_0$ when $AT(m_t)$ evaluates to false or when $m_t = m_0$). In either case, the routine concludes with $m_a$ having been assigned a sensible value.

There is however one caveat: as it stands in Algorithm \ref{alg:asf-action_timing_routine}, we operate under the hidden assumption that the idle move $m_0$ is appropriate at all times. Conversely, the same idea can be expressed as the acknowledgment that at a time when no move is known to be appropriate, the idle move is an acceptable \inSingleQuotes{out} from the dilemma. We argue this assumption is justified due to the unmitigated scarcity of any practical examples that contradict it.

Finally, once it is determined what move is to be realized, it is expressed to the outside world through behavior (Figure \ref{fig:asf-agency_action_stage}).

\input{main/agency_system_framework/figures/agency_action_stage}
