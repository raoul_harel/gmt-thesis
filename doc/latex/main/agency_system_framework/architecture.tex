\section{Architecture}
\label{sec:asf-architecture}

In this section we specify the agency system framework's design in detail, where we cover the following topics: The segmentation of the agency process into modules (\cref{sec:asf-architecture-modules,sec:asf-architecture-rap,sec:asf-architecture-cap,sec:asf-architecture-su,sec:asf-architecture-as,sec:asf-architecture-at,sec:asf-architecture-ar}), information state structure (Section \ref{sec:asf-architecture-state}), and representation of dialog moves (Section \ref{sec:asf-architecture-dialog_moves}).

\input{main/agency_system_framework/figures/complete_agency_process}

\subsection{Operation modules}
\label{sec:asf-architecture-modules}

At the base of the framework is a segmentation of the agency process into a set of \textit{operation modules} that closely mirror the same components of the process that were detailed in \cref{sec:asf-overview-perception,sec:asf-overview-deliberation,sec:asf-overview-action} (also visible in Figure \ref{fig:asf-complete_agency_process}). Each module bears responsibility for the execution of a single operation, is (largely) independent from the rest, and has well-defined input/output parameters. Together, an organized execution of these modules is what would form a complete agency system.

There are a total of six modules, below they are categorized under the three main stages of agency introduced in Section \ref{sec:asf-overview}:

\begin{itemize}[]
	\item \textbf{Perception}
		\begin{itemize}[noitemsep]
			\item \textbf{Recent activity perception:} Perceives realized dialog moves.
			\item \textbf{Current activity perception:} Classifies actors as idle/non-idle.
		\end{itemize}
	\item \textbf{Deliberation}
		\begin{itemize}[noitemsep]
			\item \textbf{State update:} Updates the information state based on perceived actor activity.
			\item \textbf{Action selection:} Designates a target move to make.
		\end{itemize}
	\item \textbf{Action}
		\begin{itemize}[noitemsep]
			\item \textbf{Action timing:} Decides whether the target move is appropriate at this time.
			\item \textbf{Action realization:} Expresses moves through behavior.
		\end{itemize}	
\end{itemize}

\noindent
Each module is responsible for the execution of exactly one operation in accordance with given input and the current information state, and is expected to produce appropriate output in return. In the following \cref{sec:asf-architecture-rap,sec:asf-architecture-cap,sec:asf-architecture-su,sec:asf-architecture-as,sec:asf-architecture-at,sec:asf-architecture-ar}, we provide an extended description for all six modules, wherein we elaborate on their respective:

\begin{itemize}[noitemsep]
	\item Operation description.
	\item Input/output specification.
	\item Invocation order in relation to other modules.
	\item State-related permissions.
\end{itemize}

\noindent
On a related note: Some modules have their input/output denoted simply as \inSingleQuotes{custom} and without further details. That is the case when that input/output involves concrete actor behavior. As it is discussed in Section \ref{sec:asf-overview}, the exact form behavior takes depends on details which are beyond the scope of this framework, and therefore it remains amorphous from our perspective. An exact specification of behavior-related parameters is ultimately an issue that is best addressed by the relevant module's implementer, which is the reason we mark input/output involving it as custom-made.

\subsection{Module: Recent activity perception}
\label{sec:asf-architecture-rap}

\begin{tabularx}{\textwidth}{l X}
	\toprule
	\textbf{Description}    	& Determines the set of moves that have taken place recently. \\
	\textbf{Input} 				& \textit{<custom>} \\
	\textbf{Output}       		& Set of dialog move realization events. \\
	\textbf{Invocation index} 	& 0 \\
	\textbf{State access}		& Read-only \\
	\bottomrule
\end{tabularx}

\bigskip\noindent
The recent activity perception module interprets the perceived behavior of other actors in the scene as dialog moves of the near past. That is, in the time since this module's latest previous invocation and up to its current one. Together with the current activity perception module (Section \ref{sec:asf-architecture-cap}), it is the first to be invoked during a process iteration. Its output is a set of move realization events (pairings of dialog moves with the actors who performed them) and is fed into the state update module (Section \ref{sec:asf-architecture-su}).

\paragraph*{Note on output}
When the output set contains multiple moves, they are to be understood as to have taken place simultaneously. This is due to the implicit assumption that the time between successive iterations of the actor's agency process is short enough to justify the \inSingleQuotes{simultaneous} label.

\paragraph*{Note on output}
The output set should not contain the idle move. Actors who are not present in the set will be assumed idle by the framework.

\subsection{Module: Current activity perception}
\label{sec:asf-architecture-cap}

\begin{tabularx}{\textwidth}{l X}
	\toprule
	\textbf{Description}    	& Classifies actors as either passive (idle) or active (non-idle). \\
	\textbf{Input} 				& \textit{<custom>} \\
	\textbf{Output}       		& Mapping of actors onto $\{passive,active\}$. \\
	\textbf{Invocation index} 	& 0 \\
	\textbf{State access}		& Read-only \\
	\bottomrule
\end{tabularx}

\bigskip\noindent
The current activity perception module interprets the perceived current behavior of other actors in the scene as either passive or active. That is, as actors who are either in the midst of realizing the idle move or not, respectively. Together with the recent activity perception module (section  \ref{sec:asf-architecture-rap}), it is the first to be invoked during a process iteration. Its output is fed into the state update module (Section \ref{sec:asf-architecture-su}).

\paragraph*{Note on output}
The produced mapping serves a secondary purpose: its set of keys (actors) can be regarded as the total set of actors the system is aware of. Therefore, changes in this set over consecutive iterations of the agency process may be used to track actor (dis)engagement from the conversation.

\subsection{Module: State update}
\label{sec:asf-architecture-su}

\begin{tabularx}{\textwidth}{l X}
	\toprule
	\textbf{Description}    	& Determines and applies the effect perceived activity has on the information
							      state. \\ 
	\textbf{Input} 				& Combined output from both perception modules, plus the system's own  
	                              activity snapshot. \\
	\textbf{Output}       		& \textit{<none>} \\
	\textbf{Invocation index} 	& 1 \\
	\textbf{State access}		& Read/Write \\
	\bottomrule
\end{tabularx}

\bigskip\noindent
The state update module evaluates perceived activity to determine and apply its effect on the information state. It is the first module to be invoked proceeding the conclusion of the perception stage, and is also the sole module with permission to mutate the state. Proceeding this module's execution and for the remainder of the agency process' current iteration, the newly updated state is that which would be presented to the action selection module (Section \ref{sec:asf-architecture-as}) and the modules of the action stage (\cref{sec:asf-architecture-at,sec:asf-architecture-ar}). In the next iteration, it would be presented to both perception modules (\cref{sec:asf-architecture-rap,sec:asf-architecture-cap}) and the upcoming invocation of the state update module.

\paragraph*{Note on input} The system's own activity snapshot comes in the form of three dialog moves: the \textit{recent move}, \textit{target move}, and \textit{actual move}. The recent move refers to the latest the system has completely realized, the target move to the one the system wishes to realize currently, and the actual move to the one that the system is actually realizing.

\subsection{Module: Action selection}
\label{sec:asf-architecture-as}

\begin{tabularx}{\textwidth}{l X}
	\toprule
	\textbf{Description}    	& Determines the target move to realize. \\
	\textbf{Input} 				& \textit{<none>} \\
	\textbf{Output}       		& Dialog move. \\
	\textbf{Invocation index} 	& 2 \\
	\textbf{State access}		& Read-only \\
	\bottomrule
\end{tabularx}

\bigskip\noindent
The action selection module evaluates the system's state and decides on which dialog move to designate as a desirable one to make --- we call it the \inSingleQuotes{target move}, because it might differ from the one which would end up being realized in actuality. The module is invoked immediately following the conclusion of the state update module, and its output is fed into the action timing module (section   \ref{sec:asf-architecture-at}).

\paragraph*{Note on output} If it is undesirable for the system to make any move, this module's output must be the idle move.

\subsection{Module: Action timing}
\label{sec:asf-architecture-at}

\begin{tabularx}{\textwidth}{l X}
	\toprule
	\textbf{Description}    	& Determines appropriate timing of the specified move. \\
	\textbf{Input} 				& Dialog move. \\
	\textbf{Output}       		& Boolean value. \\
	\textbf{Invocation index} 	& 3 \\
	\textbf{State access}		& Read-only \\
	\bottomrule
\end{tabularx}

\bigskip\noindent
The action timing module decides on the binary question: is now a good time to realize the specified move? It is invoked following execution of the deliberation stage, and its output indirectly implies the input to the action realization module (Section \ref{sec:asf-architecture-ar}).

\paragraph*{Note on output} If the input move is the idle move, the output of this module is ignored --- the framework operates under the assumption that the idle move is always an appropriate move to realize (this assumption and its justification is mentioned in Section \ref{sec:asf-overview-action}).

\subsection{Module: Action realization}
\label{sec:asf-architecture-ar}

\begin{tabularx}{\textwidth}{l X}
	\toprule
	\textbf{Description}    	& Realizes the specified move. \\
	\textbf{Input} 				& Dialog move. \\
	\textbf{Output}       		& \textit{<custom>} \\
	\textbf{Invocation index} 	& 4 \\
	\textbf{State access}		& Read-only \\
	\bottomrule
\end{tabularx}

\bigskip\noindent
The action realization module expresses a specified dialog move through output behavior. It is the last module to be invoked in an iteration. Its input move is indirectly implied by the system's target move and the output of the action timing module (Section \ref{sec:asf-architecture-at}).

\paragraph*{Note on input} The input move is one of $\{$target move$,$ idle move$\}$.

\subsection{Information state}
\label{sec:asf-architecture-state}

The information state object is accessible to all modules. The state is made up of individual data \textit{components}. Components are simply containers of a single type of data, and a state may incorporate at most one component of each type.

During instantiation of a new system, modules can declare which components they require be available to them through the information state. If a system is instantiated with a state that does not satisfy the requirements of one or more modules, the framework terminates it with an error. This compels the system administrator to comply and ensures that when setting up the information state object to use (Section \ref{sec:asf-usage-system_administration}), it indeed supports all needed components.

During their invocation, modules may access each of the components available on the information state, and the state update module is even permitted to alter the values of data stored within them.

\subsection{Dialog moves}
\label{sec:asf-architecture-dialog_moves}

The framework provides an abstract generic representation of dialog moves, one from which concrete moves can be derived. It is made up of a distinction between three types of moves: (1) zero-target, (2) single-target, and (3) multi-target. Zero-target moves are not addressed to any particular actor or group of actors (e.g. laughter, the idle move), single-target moves carry meaning intended for exactly one recipient (e.g. a hand-shake, name inquiry), and multi-target moves are aimed at one or more targets (e.g. greeting a crowd).

Users of the framework are able to take any of these basic archetypes, derive their own moves from it, and are permitted to extend it with additional properties as they see fit.
