\section{Architecture}
\label{sec:cms-architecture}

% What's at the base of te system?
At the base of the system is a single object, the communication \textit{manager}. Its job is to act as a postmaster of sorts: actors submit data they wish be broadcast to the manager, and likewise they are also notified by it to read data broadcast by others. We use the word \inSingleQuotes{notified} here because the relationship between actors and the manager follows the observer pattern. That is, where actors (observers) must first register the types of data they are interested in with the manager, and only from then on will they be granted access to communications of that type --- whenever available.

\subsection{Data synchronization}
\label{sec:cms-architecture-synchronization}

% How is data transmission synchronized? 
Recall from Section \ref{sec:cms-overview} that each actor repeatedly undergoes an individual tri-step update routine, with the steps being: (1) perception, (2) processing, and (3) action. The communication manager itself also contains its own update routine --- a global one --- that coordinates the update routines of each of the actors in the scene and handles any data they may emit. This global update routine is listed below:

\input{main/communication_management_system/algorithms/manager_general_update_routine}

Note that a sort of double-buffering is taking place. In order to ensure that the set of data perceived by all actors ($D_{in}$) remains constant throughout the iteration, we write any output data into a second buffer ($D_{out}$). Notice that come the next iteration, $D_{in}$ will effectively contain the value that $D_{out}$ carries at the end of the current one, and so a buffer swap has taken place.

In addition, note that this routine takes care of the problem of simultaneous transmission mentioned in Section \ref{sec:cms-problem_definition}: actions that happen during a given iteration (i.e. simultaneously) will be presented to actors in the one following it as a set, so they may be processed as such without ambiguity.

\subsection{Packets and channels}
\label{sec:cms-architecture-packets_and_channels}

% How is data stored?
With the general architecture explained, let us get down to the details. Firstly, we must point out that data submitted by actors is not stored inside the communication manager as-is, but as a payload within \textit{packets}. Packets contain --- aside from the data itself --- an additional piece of meta-information: the identity of that data's author. This is so in order for actors on the receiving end to be able to associate perceived data to its source, as is the case in reality.

% Are packets really necessary?
With that being said, it is true that this piece of information could have been embedded within the data itself. After all, the data's underlying structure is supplied by the consumers of the system, and they have total freedom over it. However, seeing as it is such a vital detail, and in practice almost universally required for any dialog system to function, we believe we are justified in unburdening the user of this common task and providing a working solution out of the box.

% Where is data stored?
Once a piece of data is packaged as a packet, it is ready to be stored in a \textit{channel}. A channel is simply a container for packets, and the manager contains one per data type occurring in the scene. In accordance with the routine listed in Algorithm \ref{alg:cms-manager_general_update_routine}, channels are double-buffered. That is to say: all accessor operations act on a front buffer while all mutator operations act on a hidden back buffer.

Taking packets and channels into account, a more detailed version of the global update routine can be described as follows:

\input{main/communication_management_system/algorithms/manager_detailed_update_routine}

A graphical illustration of this procedure is also visible in Figure \ref{fig:cms-manager_update_routine}, with its description available below:

\begin{enumerate}[label=(\alph*), noitemsep]
	\item The actor on the left processes the packets on the front buffer.
	\item The actor on the left emits new data to the back buffer.
	\item The actor on the right processes the packets on the front buffer.
	\item The actor on the right emits new data to the back buffer.
	\item The front and back buffers are swapped.
	\item The new (formerly front) back buffer is cleared of data in anticipation of the next iteration.
\end{enumerate}

\input{main/communication_management_system/figures/manager_update_routine}

\subsection{Centralized vs. distributed design}
\label{sec:cms-architecture-centralized_vs_distributed}

% Why the centralized approach? 
One might ask: why opt for a centralized approach with a single object acting as middle-man? Why not use a distributed scheme, where each actor broadcasts its behavior to all others directly? Two factors contributed to this choice of design: Firstly, direct contact between actors entails that each of them must be aware of all others. This inhibits the representation of eavesdroppers --- actors who are listening in on the conversation without others being aware of them. Secondly, a distributed approach introduces data synchronization issues: e.g. there is no straightforward way to indicate simultaneous data broadcasts because in such a distributed scheme a set of simultaneously occurring actions would be mistakenly perceived as a sequence (of arbitrary order, no less). In contrast, the communication manager formulation allows us to buffer incoming data and then present it as a simultaneous occurrence when queried.
