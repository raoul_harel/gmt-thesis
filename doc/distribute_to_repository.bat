rem ################################################################
rem Distributes relevant chapters/appendices from the main thesis
rem document to relevant doc directories across the repository.
rem
rem Requires PDFtk Server to be installed, see: 
rem https://www.pdflabs.com/tools/pdftk-server/ 
rem
rem Source path: "doc"
rem ################################################################ 

setlocal
set latexOutputFile="latex\main.pdf"
set thesisFile="Towards multi-modal multi-party virtual dialog systems.pdf"
set cmsDocPath=..\core\dialog-communication\doc
set asfDocPath=..\core\dialog-agency\doc
set spbaDocPath=..\demo\dialog-spb\doc
set demoAppDocPath=..\demo\application\doc

copy %latexOutputFile% %thesisFile%

pdftk %thesisFile% cat 19-27 output %cmsDocPath%\communication_management_system.pdf
pdftk %thesisFile% cat 82-86 output %cmsDocPath%\manual.pdf

pdftk %thesisFile% cat 28-44 output %asfDocPath%\framework_for_agency_systems.pdf
pdftk %thesisFile% cat 87-101 output %asfDocPath%\manual.pdf

pdftk %thesisFile% cat 45-61 output %spbaDocPath%\agency_through_social_practice.pdf
pdftk %thesisFile% cat 102-106 output %spbaDocPath%\manual.pdf

pdftk %thesisFile% cat 62-72 output %demoAppDocPath%\demo_application.pdf
pdftk %thesisFile% cat 107-109 output %demoAppDocPath%\manual.pdf

pause
