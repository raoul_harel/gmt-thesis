rem ################################################################
rem Generates HTML documentation sites for all projects.
rem
rem Requires Doxygen to be installed, see: 
rem http://www.stack.nl/~dimitri\doxygen\ 
rem
rem Source path: "doc\"
rem ################################################################ 

pushd ..\core\commons\doc\
doxygen commons.doxyfile
popd 

pushd ..\core\dialog-communication\doc\
doxygen dialog-communication.doxyfile
popd

pushd ..\core\dialog-agency\doc\
doxygen dialog-agency.doxyfile
popd

pushd ..\demo\dialog-spb\doc\
doxygen dialog-spb.doxyfile
popd

pushd ..\demo\application\example-couples-therapy\doc\
doxygen example-couples-therapy.doxyfile
popd

pause
