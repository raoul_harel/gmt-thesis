# Thesis Software Repository

This is the software repository accompanying the thesis referenced in [[1]](#references).

## Directory structure

This repository is structured as follows:

 * __`core/`__
   * `commons/` is the Common Utilities project directory.
   * `dialog-agency/` is the Framework for Agency Systems project directory.
   * `dialog-communication/` is the Communication Management System project directory.
 * __`demo/`__
   * `dialog-spb/` is the Social-Practice-Based Dialog project directory.
   * `application/`
     * `example-couples-therapy/` is the demo application's model project directory.
     * `unity/` is the presentation of the couples therapy project model in the Unity game engine.
 * __`promo/`__ contains the project's promotional web-page source code.
     
Each project contains at least the two directories `library/` and `test/`, hosting the project's source code and automated tests, respectively. In addition, some projects may also contain a `doc/` directory, hosting documentation which often consists of a user manual, a software reference, and a relevant chapter from the thesis.

## Building from source

### Prerequisites

 * The __Visual studio__ IDE (version 2015 and above should work).
 * Required for building the demo application:
   * The __Unity__ game engine.
   * The __Visual Studio Tools for Unity__ extension for Visual Studio.

### Building the demo application

While manually building all of the projects hosted under `core/` is straightforward (simply open in Visual Studio, select a configuration and `Build`), there is an additional note you need to be aware of in order to build the demo application. For further instructions, please refer to the manual at `demo/application/doc/manual.pdf`.

## Contact the author

Name: Raoul Harel

Email: `<raoul.harel@gmail.com>`

## References

1. Raoul Harel. "Towards multi-modal, multi-party virtual dialog systems". _Utrecht University_. 2017.