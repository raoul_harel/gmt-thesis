﻿using NUnit.Framework;

namespace Common.Functional.Options.Test
{
    [TestFixture]
    public sealed class NoneTest
    {
        [Test]
        public void Test_Equality()
        {
            var original = new None<int>();
            var good_copy = new None<int>();
            var flawed_type_copy = new None<string>();

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_type_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
