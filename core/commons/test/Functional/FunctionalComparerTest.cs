﻿using NUnit.Framework;
using System;

namespace Common.Functional.Test
{
    [TestFixture]
    public sealed class FunctionalComparerTest
    {
        private static readonly Func<int, int, int> 
            DEFAULT_COMPARISON = (a, b) => a.CompareTo(b);
        private static readonly Func<int, int, int> 
            REVERSED_COMPARISON = (a, b) => b.CompareTo(a);

        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new FunctionalComparer<int>(null)
            );
        }
        [Test]
        public void Test_Constructor()
        {
            var comparer = new FunctionalComparer<int>(DEFAULT_COMPARISON);

            Assert.AreEqual(DEFAULT_COMPARISON, comparer.Function);
        }

        [Test]
        public void Test_StaticConstructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => FunctionalComparer<int>.Create(null)
            );
        }
        [Test]
        public void Test_StaticConstructor()
        {
            var comparer = FunctionalComparer.Create(DEFAULT_COMPARISON);

            Assert.AreEqual(DEFAULT_COMPARISON, comparer.Function);
        }

        [Test]
        public void Test_Comparison()
        {
            int first = 1;
            int second = 2;

            var comparer = FunctionalComparer.Create<int>
            (
                (a, b) =>
                {
                    Assert.AreEqual(first, a);
                    Assert.AreEqual(second, b);
                    Assert.Pass();

                    return a.CompareTo(b);
                }
            );
            comparer.Compare(first, second);

            Assert.Fail();
        }

        [Test]
        public void Test_Equality()
        {
            var original = 
                new FunctionalComparer<int>(DEFAULT_COMPARISON);
            var good_copy = 
                new FunctionalComparer<int>(original.Function);
            var flawed_function_copy = 
                new FunctionalComparer<int>(REVERSED_COMPARISON);

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_function_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
