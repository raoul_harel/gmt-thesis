﻿using NUnit.Framework;
using System;

namespace Common.Arithmetic.Test
{
    [TestFixture]
    public sealed class ArithmeticUtilityTest
    {
        [Test]
        public void Test_Truncation_WithInvalidArgs()
        {
            Assert.Throws<ArgumentException>
            (
                () => ArithmeticUtility.Truncate(1, 2, 1)
            );
        }
        [Test]
        public void Test_Truncation()
        {
            Assert.AreEqual(1, ArithmeticUtility.Truncate(1, 1, 1));

            Assert.AreEqual(1, ArithmeticUtility.Truncate(1, 1, 3));
            Assert.AreEqual(1, ArithmeticUtility.Truncate(0, 1, 3));

            Assert.AreEqual(3, ArithmeticUtility.Truncate(3, 1, 3));
            Assert.AreEqual(3, ArithmeticUtility.Truncate(4, 1, 3));
        }
    }
}
