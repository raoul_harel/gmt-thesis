﻿using NUnit.Framework;

namespace Common.Hashing.Test
{
    [TestFixture]
    public sealed class HashCombinerTest
    {
        private int _hash;

        [SetUp]
        public void Setup()
        {
            _hash = HashCombiner.Initialize();
        }

        [Test]
        public void Test_HandlingOfNumericOverflow()
        {
            Assert.DoesNotThrow
            (
                () => _hash = HashCombiner.Hash(_hash, int.MaxValue)
            );
        }
    }
}
