﻿using NUnit.Framework;
using System;

namespace Common.Randomization.Distributions.Test
{
    [TestFixture]
    public sealed class PseudoRandomDistributionTest
    {
        private sealed class MockDistribution
            : PseudoRandomDistribution
        {
            public MockDistribution()
                : base()
            { }
            public MockDistribution(Random generator)
                : base(generator)
            { }

            public override double Sample()
            {
                throw new NotImplementedException();
            }
        }
        
        [Test]
        public void Test_DefaultConstructor()
        {
            var distribution = new MockDistribution();

            Assert.IsNotNull(distribution.Generator);
        }
        [Test]
        public void Test_ConstructorWithGenerator_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new MockDistribution(null)
            );
        }
        [Test]
        public void Test_ConstructorWithGenerator()
        { 
            var generator = new Random();
            var distribution = new MockDistribution(generator);

            Assert.AreSame(generator, distribution.Generator);
        }
    }
}
