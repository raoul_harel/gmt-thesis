﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Randomization.Extensions.Test
{
    [TestFixture]
    public sealed class ListExtensionsTest
    {
        [Test]
        public void Test_Shuffling_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => ((IList<int>)null).Shuffle()
            );
        }
        [Test]
        public void Test_Shuffling()
        {
            var sorted = Enumerable.Range(0, 10).ToList();
            var shuffled = sorted.Shuffle();

            Assert.AreNotSame(sorted, shuffled);
            Assert.AreNotEqual(sorted, shuffled);
        }
    }
}
