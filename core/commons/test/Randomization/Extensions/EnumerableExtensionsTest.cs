﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
namespace Common.Randomization.Extensions.Test
{
    [TestFixture]
    public sealed class EnumerableExtensionsTest
    {
        [Test]
        public void Test_RandomItemSelection_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => ((IEnumerable<int>)null).RandomItem()
            );
        }
    }
}
