﻿using Common.Collections.Interfaces;
using Common.Validation;
using System.Collections;
using System.Collections.Generic;

namespace Common.Collections.Views
{
    /// <summary>
    /// This class represents an immutable view of a collection.
    /// </summary>
    /// <typeparam name="T">The type of the collection's items.</typeparam>
    public class CollectionView<T>
        : ImmutableCollection<T>
    {
        /// <summary>
        /// Creates a new view.
        /// </summary>
        /// <param name="collection">The collection to wrap around.</param>
        /// <remarks>
        /// <paramref name="collection"/> must not be null.
        /// </remarks>
        public CollectionView(ICollection<T> collection)
        {
            Require.IsNotNull(collection);

            Collection = collection;
        }

        /// <summary>
        /// Indicates whether the collection is empty.
        /// </summary>
        public bool IsEmpty
        {
            get { return Count == 0; }
        }
        /// <summary>
        /// Gets the item count.
        /// </summary>
        public int Count
        {
            get { return Collection.Count; }
        }

        /// <summary>
        /// Determines whether the collection contains the specified item.
        /// </summary>
        /// <param name="item">The item to search for.</param>
        /// <returns>
        /// True if the collection contains the specified item, otherwise 
        /// false.
        /// </returns>
        public bool Contains(T item)
        {
            return Collection.Contains(item);
        }

        /// <summary>
        /// Gets a generic enumerator over the collection's items.
        /// </summary>
        /// <returns>Generic enumerator on the collection's items.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return Collection.GetEnumerator();
        }
        /// <summary>
        /// Gets a non-generic enumerator over the collection's items.
        /// </summary>
        /// <returns>Enumerator on the collection's items.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)Collection).GetEnumerator();
        }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as CollectionView<T>;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.Collection.Equals(Collection);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Collection.GetHashCode();
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(CollectionView<T>)}(" +
                   $"{nameof(Count)} = {Count})";
        }

        /// <summary>
        /// Gets the internal collection.
        /// </summary>
        protected ICollection<T> Collection { get; private set; }
    }
}
