﻿using Common.Collections.Interfaces;
using Common.Validation;
using System.Collections.Generic;

namespace Common.Collections.Views
{
    /// <summary>
    /// This class represents an immutable view of a list.
    /// </summary>
    /// <typeparam name="T">The type of the collection's items.</typeparam>
    public sealed class ListView<T>
        : CollectionView<T>, ImmutableList<T>
    {
        /// <summary>
        /// Creates a new view.
        /// </summary>
        /// <param name="list">The list to wrap around.</param>
        /// <remarks>
        /// <paramref name="list"/> must not be null.
        /// </remarks>
        public ListView(IList<T> list)
            : base(list)
        {
            Require.IsNotNull(list);

            _list = list;
        }

        /// <summary>
        /// Gets the item at the specified index.
        /// </summary>
        /// <param name="index">The desired item's index.</param>
        /// <returns>The item at the specified index.</returns>
        public T this[int index]
        {
            get { return _list[index]; }
        }
        /// <summary>
        /// Determines the index of the specified item in this list.
        /// </summary>
        /// <param name="item">The item to index.</param>
        /// <returns>The item's index if found, otherwise -1.</returns>
        /// <remarks>
        /// If the list contains multiple occurrences of the item, this method
        /// returns the index of the first one.
        /// </remarks>
        public int IndexOf(T item)
        {
            return _list.IndexOf(item);
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ListView<T>)}(" +
                   $"{nameof(Count)} = {Count})";
        }

        private readonly IList<T> _list;
    }
}
