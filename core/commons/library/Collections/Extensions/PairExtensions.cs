﻿using System;
using System.Collections.Generic;

namespace Common.Collections.Extensions
{
    /// <summary>
    /// A collection of extension methods for pairs.
    /// </summary>
    public static class PairExtensions
    {
        /// <summary>
        /// Allows enumeration over a pair's items.
        /// </summary>
        /// <typeparam name="T">The type of items of the pair.</typeparam>
        /// <param name="pair">The pair to present as an enumeration.</param>
        /// <returns>
        /// An enumerable over the pair's items.
        /// </returns>
        public static IEnumerable<T> AsEnumerable<T>(this Pair<T, T> pair)
        {
            yield return pair.First;
            yield return pair.Second;
        }

        /// <summary>
        /// Maps the specified pair to a new one using the specified 
        /// transformation function.
        /// </summary>
        /// <typeparam name="T">
        /// The type of items of the original pair.
        /// </typeparam>
        /// <typeparam name="TResult">
        /// The type of items of the mapping result.
        /// </typeparam>
        /// <param name="pair">The pair to transform.</param>
        /// <param name="transform">The transformation function.</param>
        /// <returns>The transform of the original pair.</returns>
        public static Pair<TResult, TResult> Map<T, TResult>
        (
            this Pair<T, T> pair, 
            Func<T, TResult> transform
        )
        {
            return new Pair<TResult, TResult>
            (
                transform(pair.First),
                transform(pair.Second)
            );
        }

        /// <summary>
        /// Converts the specified identical pair to an array.
        /// </summary>
        /// <typeparam name="T">The type of items of the pair.</typeparam>
        /// <param name="pair">The pair to convert.</param>
        /// <returns>
        /// An array with the pair's items, in their natural order.
        /// </returns>
        public static T[] ToArray<T>(this Pair<T, T> pair)
        {
            return new T[2] { pair.First, pair.Second };
        }
        /// <summary>
        /// Converts the specified identical pair to a list.
        /// </summary>
        /// <typeparam name="T">The type of items of the pair.</typeparam>
        /// <param name="pair">The pair to convert.</param>
        /// <returns>
        /// A list with the pair's items, in their natural order.
        /// </returns>
        public static List<T> ToList<T>(this Pair<T, T> pair)
        {
            return new List<T> { pair.First, pair.Second };
        }
    }
}
