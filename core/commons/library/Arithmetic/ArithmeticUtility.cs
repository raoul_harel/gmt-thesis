﻿using Common.Validation;
using System.Collections.Generic;

namespace Common.Arithmetic
{
    /// <summary>
    /// A collection of arithmetic utility methods.
    /// </summary>
    public static class ArithmeticUtility
    {
        /// <summary>
        /// Truncates the specified value to the specified range.
        /// </summary>
        /// <typeparam name="T">The truncated object type.</typeparam>
        /// <param name="value">The value to truncate.</param>
        /// <param name="lower_bound">
        /// The range's lower bound (inclusive).
        /// </param>
        /// <param name="upper_bound">
        /// The range's upper bound (inclusive).
        /// </param>
        /// <returns>
        /// The specified value, truncated to the specified range.
        /// </returns>
        /// <remarks>
        /// <paramref name="lower_bound"/> must be less than or equal to
        /// <paramref name="upper_bound"/>.
        /// </remarks>
        public static T Truncate<T>
        (
            T value,
            T lower_bound,
            T upper_bound
        )
        {
            Require.IsAtMost(lower_bound, upper_bound);

            if (Comparer<T>.Default.Compare(value, lower_bound) < 0)
            {
                return lower_bound;
            }
            else if (Comparer<T>.Default.Compare(value, upper_bound) > 0)
            {
                return upper_bound;
            }
            else
            {
                return value;
            }
        }
    }
}
