﻿using Common.Validation;
using System;
using System.Collections.Generic;

namespace Common.Randomization.Extensions
{
    /// <summary>
    /// Extension methods involving pseudo-random operations on lists.
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// Returns a shuffled copy of the specified list.
        /// </summary>
        /// <typeparam name="T">The type of the list's items.</typeparam>
        /// <param name="list">The list to shuffle.</param>
        /// <returns>A shuffled copy of the specified list.</returns>
        /// <remarks>
        /// <paramref name="list"/> must not be null.
        /// </remarks>
        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            Require.IsNotNull(list);

            var shuffled = new List<T>(list);
            Random generator = PseudoRandom.Generator;

            // Shuffle with the Fisher-Yates algorithm:
            for (int i = shuffled.Count - 1; i > 0; --i)
            {
                int j = generator.Next(0, i + 1);

                var buffer = shuffled[i];
                shuffled[i] = shuffled[j];
                shuffled[j] = buffer;
            }

            return shuffled;
        }
    }
}
