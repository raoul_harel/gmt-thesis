﻿using Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Randomization.Extensions
{
    /// <summary>
    /// Extension methods involving pseudo-random operations on enumerables.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Chooses a random item from an enumerable.
        /// </summary>
        /// <typeparam name="T">Type of the enumerable's items.</typeparam>
        /// <param name="enumerable">The enumerable to pick from.</param>
        /// <returns>
        /// A randomly chosen item, or if the enumerable is empty: the default 
        /// value of <typeparamref name="T"/>.
        /// </returns>
        /// <remarks>
        /// <paramref name="enumerable"/> must not be null.
        /// </remarks>
        public static T RandomItem<T>(this IEnumerable<T> enumerable)
        {
            Require.IsNotNull(enumerable);

            if (enumerable.Count() == 0) { return default(T); }

            Random generator = PseudoRandom.Generator;
            int random_index = generator.Next(enumerable.Count());
            return enumerable.ElementAt(random_index);
        }
    }
}
