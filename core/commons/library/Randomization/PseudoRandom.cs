﻿using System;

namespace Common.Randomization
{
    /// <summary>
    /// This class is a global access-point to a pseudo-random number 
    /// generator.
    /// </summary>
    public static class PseudoRandom
    {
        /// <summary>
        /// Gets the generator.
        /// </summary>
        public static Random Generator { get; private set; } = new Random();

        /// <summary>
        /// Seeds the generator with the specified value.
        /// </summary>
        /// <param name="value">The value to seed into the generator.</param>
        public static void Seed(int value)
        {
            Generator = new Random(value);
        }
    }
}
