﻿namespace Common.Randomization.Distributions
{
    /// <summary>
    /// This interface represents a statistical distribution that can be 
    /// sampled.
    /// </summary>
    public interface Distribution
    {
        /// <summary>
        /// Samples the distribution.
        /// </summary>
        /// <returns>The sampled value.</returns>
        double Sample();
    }
}
