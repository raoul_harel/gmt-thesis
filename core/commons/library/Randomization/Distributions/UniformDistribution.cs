﻿using Common.Hashing;
using Common.Validation;
using System;

namespace Common.Randomization.Distributions
{
    /// <summary>
    /// This class represents a uniform random distribution on a given range.
    /// </summary>
    public sealed class UniformDistribution
        : PseudoRandomDistribution
    {
        /// <summary>
        /// Samples from a uniform distribution on the specified range.
        /// </summary>
        /// <param name="lower_bound">
        /// The range's minimum boundary (inclusive).
        /// </param>
        /// <param name="upper_bound">
        /// The range's maximum boundary (inclusive).
        /// </param>
        /// <returns>The sampled value.</returns>
        /// <remarks>
        /// <paramref name="lower_bound"/> must be lesser-than or equal to 
        /// <paramref name="upper_bound"/>
        /// </remarks>
        public static double Sample
        (
            double lower_bound, 
            double upper_bound
        )
        {
            Require.IsAtMost(lower_bound, upper_bound);

            _static_instance.SetRange(lower_bound, upper_bound);

            return _static_instance.Sample();
        }

        private static readonly UniformDistribution _static_instance = 
            new UniformDistribution();

        /// <summary>
        /// Creates a new distribution on the specified range and sampling 
        /// from the specified random number generator.
        /// </summary>
        /// <param name="generator">
        /// The random number generator to sample from.
        /// </param>
        /// <param name="lower_bound">
        /// The range's minimum boundary (inclusive).
        /// </param>
        /// <param name="upper_bound">
        /// The range's maximum boundary (inclusive).
        /// </param>
        /// <remarks>
        /// <paramref name="generator"/> must not be null.
        /// <paramref name="lower_bound"/> must be lesser-than or equal to 
        /// <paramref name="upper_bound"/>
        /// </remarks>
        public UniformDistribution
        (
            Random generator, 
            double lower_bound, 
            double upper_bound
        )
            : base(generator)
        {
            Require.IsAtMost(lower_bound, upper_bound);

            SetRange(lower_bound, upper_bound);
        }
        /// <summary>
        /// Creates a new distribution on the specified range.
        /// </summary>
        /// <param name="lower_bound">
        /// The range's minimum boundary (inclusive).
        /// </param>
        /// <param name="upper_bound">
        /// The range's maximum boundary (inclusive).
        /// </param>
        /// <remarks>
        /// <paramref name="lower_bound"/> must be lesser-than or equal to 
        /// <paramref name="upper_bound"/>
        /// </remarks>
        public UniformDistribution
        (
            double lower_bound, 
            double upper_bound
        )
            : this(new Random(), lower_bound, upper_bound) {}
        /// <summary>
        /// Creates a new distribution on [0, 1], and sampling from the 
        /// specified random number generator.
        /// </summary>
        /// <param name="generator">
        /// The random number generator to sample from.
        /// </param>
        /// <remarks>
        /// <paramref name="generator"/> must not be null.
        /// </remarks>
        public UniformDistribution(Random generator)
            : this(generator, 0.0f, 1.0f) { }
        /// <summary>
        /// Creates a new distribution on [0, 1].
        /// </summary>
        public UniformDistribution()
            : this(0.0f, 1.0f) { }

        /// <summary>
        /// Samples the distribution.
        /// </summary>
        /// <returns>
        /// The sampled value.
        /// </returns>
        public override double Sample()
        {
            return LowerBound + Generator.NextDouble() * _range_size;
        }

        /// <summary>
        /// Gets the distribution's minimum boundary.
        /// </summary>
        public double LowerBound { get; private set; }
        /// <summary>
        /// Gets the distribution's maximum boundary.
        /// </summary>
        public double UpperBound { get; private set; }

        /// <summary>
        /// Sets distribution range boundaries.
        /// </summary>
        /// <param name="lower_bound">
        /// The range's minimum boundary (inclusive).
        /// </param>
        /// <param name="upper_bound">
        /// The range's maximum boundary (inclusive).
        /// </param>
        /// <remarks>
        /// <paramref name="lower_bound"/> must be lesser-than or equal to 
        /// <paramref name="upper_bound"/>
        /// </remarks>
        private void SetRange
        (
            double lower_bound, 
            double upper_bound
        )
        {
            LowerBound = lower_bound;
            UpperBound = upper_bound;
            _range_size = UpperBound - LowerBound;
        }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as UniformDistribution;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.LowerBound.Equals(LowerBound) &&
                   other.UpperBound.Equals(UpperBound);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, LowerBound);
            hash = HashCombiner.Hash(hash, UpperBound);

            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(UniformDistribution)}(" +
                   $"{nameof(LowerBound)} = {LowerBound}," +
                   $"{nameof(UpperBound)} = {UpperBound})";
        }

        private double _range_size;
    }
}
