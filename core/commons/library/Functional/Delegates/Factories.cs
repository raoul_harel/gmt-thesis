﻿namespace Common.Functional.Delegates
{
    /// <summary>
    /// General delegate for non-parametric factories.
    /// </summary>
    /// <typeparam name="T">The type of the created object.</typeparam>
    /// <returns>The created object.</returns>
    public delegate T Factory<T>();
}
