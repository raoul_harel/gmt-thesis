﻿namespace Common.Functional.Delegates
{
    /// <summary>
    /// General delegate for non-parametric predicates.
    /// </summary>
    public delegate bool Predicate();
    /// <summary>
    /// Convenience methods for the creation of common predicates.
    /// </summary>
    public static class Predicates
    {
        /// <summary>
        /// The predicate that always evaluates to true.
        /// </summary>
        public static readonly Predicate Always = () => true;
        /// <summary>
        /// The predicate that never evaluates to true.
        /// </summary>
        public static readonly Predicate Never = () => false;
    }
}
