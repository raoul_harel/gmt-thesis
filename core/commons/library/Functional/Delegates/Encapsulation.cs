﻿namespace Common.Functional.Delegates
{
    /// <summary>
    /// General accessor-method delegate.
    /// </summary>
    /// <typeparam name="T">The type of the accessed field.</typeparam>
    /// <returns>The field's value.</returns>
    public delegate T Getter<T>();
    /// <summary>
    /// General mutator-method delegate.
    /// </summary>
    /// <typeparam name="T">The type of the mutated field.</typeparam>
    /// <param name="value">The field's new value.</param>
    public delegate void Setter<T>(T value);
}
