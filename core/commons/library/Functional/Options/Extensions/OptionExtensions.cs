﻿using Common.Validation;
using System;

namespace Common.Functional.Options.Extensions
{
    /// <summary>
    /// Extension methods for options.
    /// </summary>
    public static class OptionExtensions
    {
        /// <summary>
        /// Determines whether the specified option holds some value.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value held by the option.
        /// </typeparam>
        /// <param name="option">The option to test.</param>
        /// <returns>
        /// True if the specified option holds some value, otherwise false.
        /// </returns>
        public static bool IsSome<T>(this Option<T> option)
        {
            return option is Some<T>;
        }
        /// <summary>
        /// Determines whether the specified option holds no value.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value held by the option.
        /// </typeparam>
        /// <param name="option">The option to test.</param>
        /// <returns>
        /// True if the specified option holds no value, otherwise false.
        /// </returns>
        public static bool IsNone<T>(this Option<T> option)
        {
            return option is None<T>;
        }

        /// <summary>
        /// Invokes an action if the specified option holds some value.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value held by the option.
        /// </typeparam>
        /// <param name="option">The option to test.</param>
        /// <param name="action">
        /// The action to invoke when the option holds some value.
        /// </param>
        /// <returns>
        /// True if the specified option holds some value, otherwise false.
        /// </returns>
        /// <remarks>
        /// <paramref name="action"/> must not be null.
        /// </remarks>
        public static bool IfIsSome<T>
        (
            this Option<T> option,
            Action<T> action
        )
        {
            Require.IsNotNull(action);

            if (option.IsSome())
            {
                action.Invoke(option.GetValue());
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Pattern matches the specified option to an evaluated expression.
        /// </summary>
        /// <typeparam name="TValue">
        /// The type of the value held by the option.
        /// </typeparam>
        /// <typeparam name="TResult">
        /// The type of the result returned by this operation.
        /// </typeparam>
        /// <param name="option">The option to test.</param>
        /// <param name="some">
        /// The function to evaluate when the option holds some value.
        /// </param>
        /// <param name="none">
        /// The function to evaluate when the option holds no value.
        /// </param>
        /// <returns>
        /// True if the specified option holds some value, otherwise false.
        /// </returns>
        /// <remarks>
        /// <paramref name="some"/> must not be null.
        /// <paramref name="none"/> must not be null.
        /// </remarks>
        public static TResult Match<TValue, TResult>
        (
            this Option<TValue> option,
            Func<TValue, TResult> some,
            Func<TResult> none
        )
        {
            Require.IsNotNull(some);
            Require.IsNotNull(none);

            return option.IsSome() ? 
                some.Invoke(option.GetValue()) : 
                none.Invoke(); 
        }
        /// <summary>
        /// Pattern matches the specified option to an action.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value held by the option.
        /// </typeparam>
        /// <param name="option">The option to test.</param>
        /// <param name="some">
        /// The action to invoke when the option holds some value.
        /// </param>
        /// <param name="none">
        /// The action to invoke when the option holds no value.
        /// </param>
        /// <returns>
        /// True if the specified option holds some value, otherwise false.
        /// </returns>
        /// <remarks>
        /// <paramref name="some"/> must not be null.
        /// <paramref name="none"/> must not be null.
        /// </remarks>
        public static bool Match<T>
        (
            this Option<T> option,
            Action<T> some,
            Action none
        )
        {
            Require.IsNotNull(some);
            Require.IsNotNull(none);

            if (option.IsSome())
            {
                some.Invoke(option.GetValue());
                return true;
            }
            else
            {
                none.Invoke();
                return false;
            }
        }

        /// <summary>
        /// Gets the value of the specified option, if it exists.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value held by the option.
        /// </typeparam>
        /// <param name="option">The option.</param>
        /// <returns>The option's value.</returns>
        public static T GetValue<T>(this Option<T> option)
        {
            Require.IsTrue<InvalidOperationException>(option.IsSome());

            return ((Some<T>)option).Value;
        }
    }
}
