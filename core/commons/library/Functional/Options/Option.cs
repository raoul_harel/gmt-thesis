﻿namespace Common.Functional.Options
{
    /// <summary>
    /// This interface represents an optional type with two states, one whose 
    /// value is present (a "some" state) or one where it is not 
    /// (the "none" state).
    /// </summary>
    /// <typeparam name="T">
    /// The type of the value held by the option.
    /// </typeparam>
    public interface Option<T>
    {
        /// <summary>
        /// Determines whether this option contains the specified value.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <returns>
        /// True if this option contains the specified value, otherwise false.
        /// </returns>
        bool Contains(T value);

        /// <summary>
        /// Casts this option to the specified type.
        /// </summary>
        /// <typeparam name="TResult">The type to cast to.</typeparam>
        /// <returns>
        /// A new option containing the cast value of this one.
        /// </returns>
        Option<TResult> Cast<TResult>();
    }
    /// <summary>
    /// Convenience methods for the creation of options.
    /// </summary>
    public static class Option
    {
        /// <summary>
        /// Creates a new option holding some value.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value held by the option.
        /// </typeparam>
        /// <param name="value">The value to hold.</param>
        /// <returns>A new option.</returns>
        public static Option<T> CreateSome<T>(T value)
        {
            return new Some<T>(value);
        }
        /// <summary>
        /// Creates a new option holding no value.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the value held by the option.
        /// </typeparam>
        /// <returns>A new option.</returns>
        public static Option<T> CreateNone<T>()
        {
            return new None<T>();
        }
    }
}
