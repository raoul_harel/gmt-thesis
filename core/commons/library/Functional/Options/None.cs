﻿namespace Common.Functional.Options
{
    /// <summary>
    /// This structure represents the "none" state of an option.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the value held by the option.
    /// </typeparam>
    public struct None<T>
        : Option<T>
    {
        /// <summary>
        /// Determines whether this option contains the specified value.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <returns>
        /// True if this option contains the specified value, otherwise 
        /// false.
        /// </returns>
        public bool Contains(T value)
        {
            return false;
        }

        /// <summary>
        /// Casts this option to the specified type.
        /// </summary>
        /// <typeparam name="TResult">The type to cast to.</typeparam>
        /// <returns>
        /// A new option containing the cast value of this one.
        /// </returns>
        public Option<TResult> Cast<TResult>()
        {
            return new None<TResult>();
        }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return obj is None<T>;
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return 0;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(None<T>)}()";
        }
    }
}
