var struct_common_1_1_functional_1_1_options_1_1_some =
[
    [ "Some", "struct_common_1_1_functional_1_1_options_1_1_some.html#afcca326cc6d4485d2171cc518fa082df", null ],
    [ "Cast< TResult >", "struct_common_1_1_functional_1_1_options_1_1_some.html#aa2a18b16d7fe60169db26c4a4561cda3", null ],
    [ "Contains", "struct_common_1_1_functional_1_1_options_1_1_some.html#a06da2e677b811ee986f10215ba8c4033", null ],
    [ "Equals", "struct_common_1_1_functional_1_1_options_1_1_some.html#a38da9282270289df34752c5b0821e8ee", null ],
    [ "GetHashCode", "struct_common_1_1_functional_1_1_options_1_1_some.html#a63aca62a4080498759f071b1d8caeb84", null ],
    [ "ToString", "struct_common_1_1_functional_1_1_options_1_1_some.html#a80d47ca2640f6a2f1fa08b0551e56ac9", null ],
    [ "Value", "struct_common_1_1_functional_1_1_options_1_1_some.html#a441aedf7466cb58bd101b6308e486a11", null ]
];