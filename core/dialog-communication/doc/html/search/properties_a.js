var searchData=
[
  ['packets',['Packets',['../interface_dialog_1_1_communication_1_1_channels_1_1_data_channel.html#ae12b6fa61ce11353101f4e6b26a187bf',1,'Dialog.Communication.Channels.DataChannel.Packets()'],['../class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html#a7e6da681cfd3709e0805a4875c6ac429',1,'Dialog.Communication.Channels.GenericChannel.Packets()'],['../class_dialog_1_1_communication_1_1_channels_1_1_generic_double_buffered_channel.html#a70569bdd331ff2aa06c1e8db37ab9699',1,'Dialog.Communication.Channels.GenericDoubleBufferedChannel.Packets()']]],
  ['payload',['Payload',['../interface_dialog_1_1_communication_1_1_packets_1_1_data_packet.html#ad061f09c7a76d04a6440d8c5146d53c4',1,'Dialog.Communication.Packets.DataPacket.Payload()'],['../class_dialog_1_1_communication_1_1_packets_1_1_generic_packet.html#ab908dc4855f55fe5283ea0257705da2a',1,'Dialog.Communication.Packets.GenericPacket.Payload()']]]
];
