var class_dialog_1_1_communication_1_1_management_1_1_channel_batch =
[
    [ "Builder", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch_1_1_builder.html", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch_1_1_builder" ],
    [ "Clear", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch.html#a83e093a16aec159cd621b92ca0137135", null ],
    [ "Get< T >", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch.html#a9cc617be0913169001d32747f6148d6e", null ],
    [ "SwapBuffers", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch.html#a6697b4e266018b86780b09697e6c59ac", null ],
    [ "ToString", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch.html#a56b185cc9a822e5b9c28918a509dd5b9", null ],
    [ "Channels", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch.html#a42fca60ca1566acded1b8f04591d69b9", null ],
    [ "DataTypes", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch.html#ae0c49c7a8fde0d6d179e858c7014d19a", null ]
];