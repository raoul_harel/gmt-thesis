﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Validation;
using Dialog.Communication.Packets;
using System;
using System.Collections.Generic;

namespace Dialog.Communication.Channels
{
    /// <summary>
    /// A generic implementation of the <see cref="DataChannel{T}"/> interface. 
    /// </summary>
    /// <typeparam name="T">The type of data the channel carries.</typeparam>
    public sealed class GenericChannel<T> 
        : DataChannel<T>
    {
        /// <summary>
        /// Creates a new channel.
        /// </summary>
        public GenericChannel()
        {
            Packets = new CollectionView<DataPacket<T>>(_packets);
        }

        /// <summary>
        /// Gets the type of data the channel carries.
        /// </summary>
        public Type DataType { get; private set; } = typeof(T);

        /// <summary>
        /// Gets the packets currently live on the channel.
        /// </summary>
        public ImmutableCollection<DataPacket<T>> Packets
        {
            get;
            private set;
        }

        /// <summary>
        /// Clears the channel.
        /// </summary>
        public void Clear()
        {
            _packets.Clear();
        }

        /// <summary>
        /// Posts the specified packet onto the channel.
        /// </summary>
        /// <param name="packet">The packet to post.</param>
        /// <remarks>
        /// <paramref name="packet"/> must not be null.
        /// </remarks>
        public void Post(DataPacket<T> packet)
        {
            Require.IsNotNull(packet);

            _packets.Add(packet);
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(GenericChannel<T>)}(" +
                   $"{nameof(DataType)} = {DataType}, " +
                   $"{nameof(Packets)} = {Packets})";
        }

        private readonly List<DataPacket<T>> 
            _packets = new List<DataPacket<T>>();
    }
}
