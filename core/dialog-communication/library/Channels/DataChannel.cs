﻿using Common.Collections.Interfaces;
using Dialog.Communication.Packets;
using System;

namespace Dialog.Communication.Channels
{
    /// <summary>
    /// This interface represents a communication channel of some data type.
    /// </summary>
    public interface DataChannel
    {
        /// <summary>
        /// Gets the type of data the channel carries.
        /// </summary>
        Type DataType { get; }

        /// <summary>
        /// Clears the channel.
        /// </summary>
        void Clear();
    }
    /// <summary>
    /// This interface represents a communication channel carrying packets of 
    /// data of the specified type.
    /// </summary>
    /// <typeparam name="T">The type of data the channel carries.</typeparam>
    public interface DataChannel<T>
        : DataChannel
    {
        /// <summary>
        /// Gets the packets currently live on the channel.
        /// </summary>
        ImmutableCollection<DataPacket<T>> Packets { get; }

        /// <summary>
        /// Posts the specified packet onto the channel.
        /// </summary>
        /// <param name="packet">The packet to post.</param>
        void Post(DataPacket<T> packet);
    }
}
