﻿namespace Dialog.Communication.Channels
{
    /// <summary>
    /// This interface represents a double-buffered communication channel of
    /// some data type.
    /// </summary>
    public interface DoubleBufferedDataChannel
        : DataChannel
    {
        /// <summary>
        /// Swaps the front and back buffers.
        /// </summary>
        void SwapBuffers();
    }
    /// <summary>
    /// This interface represents a double-buffered communication channel
    /// carrying packets of data of the specified type.
    /// 
    /// Following the same concept from the field of computer graphics, double
    /// buffering here means that all accessor operations act on a front 
    /// buffer, while all mutating operations (such as packet- posting and 
    /// clearing) affect a back buffer. When ready to make the changes made
    /// to the back buffer visible, the two buffers may be swapped.
    /// </summary>
    /// <typeparam name="T">The type of data the channel carries.</typeparam>
    public interface DoubleBufferedDataChannel<T>
        : DataChannel<T>, DoubleBufferedDataChannel
    { }
}
