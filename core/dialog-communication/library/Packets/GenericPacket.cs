﻿using Common.Hashing;
using Common.Validation;
using Dialog.Communication.Actors;

namespace Dialog.Communication.Packets
{
    /// <summary>
    /// A generic implementation of the <see cref="DataPacket{T}"/> interface.
    /// </summary>
    /// <typeparam name="T">The type of the payload data.</typeparam>
    public sealed class GenericPacket<T> 
        : DataPacket<T>
    {
        /// <summary>
        /// Creates a new packet.
        /// </summary>
        /// <param name="author">The packet's author.</param>
        /// <param name="payload">The packet's payload.</param>
        /// <remarks>
        /// <paramref name="author"/> must not be null.
        /// <paramref name="payload"/> must not be null.
        /// </remarks>
        public GenericPacket
        (
            DataAuthor author,
            T payload
        )
        {
            Require.IsNotNull(author);
            Require.IsNotNull(payload);

            Author = author;
            Payload = payload;
        }

        /// <summary>
        /// Gets the packet's author.
        /// </summary>
        public DataAuthor Author { get; private set; }

        /// <summary>
        /// Gets the payload data.
        /// </summary>
        public T Payload { get; private set; }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as GenericPacket<T>;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.Author.Equals(Author) &&
                   other.Payload.Equals(Payload);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            var hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, Author);
            hash = HashCombiner.Hash(hash, Payload);

            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(GenericPacket<T>)}(" +
                   $"{nameof(Author)} = {Author}, " +
                   $"{nameof(Payload)} = {Payload})";
        }
    }
    /// <summary>
    /// Convenience methods for the creation of packets.
    /// </summary>
    public static class GenericPacket
    {
        /// <summary>
        /// Creates a new packet.
        /// </summary>
        /// <typeparam name="T">The type of the payload data.</typeparam>
        /// <param name="author">The packet's author.</param>
        /// <param name="payload">The packet's payload.</param>
        /// <remarks>
        /// <paramref name="author"/> must not be null.
        /// <paramref name="payload"/> must not be null.
        /// </remarks>
        /// <returns>A new packet.</returns>
        public static GenericPacket<T> Create<T>
        (
            DataAuthor author,
            T payload
        )
        {
            Require.IsNotNull(author);
            Require.IsNotNull(payload);

            return new GenericPacket<T>(author, payload);
        }
    }
}
