﻿namespace Dialog.Communication.Actors
{
    /// <summary>
    /// This interface represents an actor capable of authoring data.
    /// </summary>
    public interface DataAuthor { }
}
