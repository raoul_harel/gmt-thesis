﻿using static Dialog.Communication.Management.CommunicationManager;

namespace Dialog.Communication.Actors
{
    /// <summary>
    /// This interface represents an actor managed by the communication manager
    /// system.
    /// </summary>
    public interface ManagedActor
        : DataAuthor
    {
        /// <summary>
        /// Acts through the specified data submission. The submission is used
        /// to relay data back to the communication manager.
        /// </summary>
        /// <param name="submission">The data submission to use.</param>
        void Act(DataSubmission submission);
    }
}
