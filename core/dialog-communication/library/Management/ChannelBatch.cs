﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.DesignPatterns;
using Common.Validation;
using Dialog.Communication.Channels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dialog.Communication.Management
{
    /// <summary>
    /// This class represents a batch of double-buffered channels, organized 
    /// by the type of data they carry.
    /// </summary>
    public sealed class ChannelBatch
    {
        /// <summary>
        /// This class is used to build instances of 
        /// <see cref="ChannelBatch"/>. 
        /// </summary>
        public sealed class Builder
            : ObjectBuilder<ChannelBatch>
        {
            /// <summary>
            /// Specifies that the batch should include a channel for the 
            /// specified type.
            /// </summary>
            /// <typeparam name="T">
            /// The type of data the channel carries.
            /// </typeparam>
            /// <returns>This instance (for method call-chaining).</returns>
            /// <remarks>
            /// Calling this on a built object is invalid.
            /// </remarks>
            public Builder WithChannel<T>()
            {
                Require.IsFalse<InvalidOperationException>(IsBuilt);

                Type type = typeof(T);

                if (_channel_by_data_type.ContainsKey(type)) { return this; }

                _channel_by_data_type.Add
                (
                    type,
                    new GenericDoubleBufferedChannel<T>()
                );

                return this;
            }

            /// <summary>
            /// Creates the object.
            /// </summary>
            /// <returns>
            /// The built object.
            /// </returns>
            protected override ChannelBatch CreateObject()
            {
                return new ChannelBatch(_channel_by_data_type.Values);
            }

            private readonly Dictionary<Type, DoubleBufferedDataChannel>
                _channel_by_data_type =
                new Dictionary<Type, DoubleBufferedDataChannel>();
        }

        /// <summary>
        /// Creates a new batch from the specified channels.
        /// </summary>
        /// <param name="channels">The channels to hold.</param>
        private ChannelBatch(IEnumerable<DoubleBufferedDataChannel> channels)
        {
            foreach (var channel in channels)
            {
                _channel_by_data_type.Add(channel.DataType, channel);
            }
            DataTypes = new CollectionView<Type>(_channel_by_data_type.Keys);
        }

        /// <summary>
        /// Gets the collection of supported data types.
        /// </summary>
        public ImmutableCollection<Type> DataTypes { get; private set; }

        /// <summary>
        /// Gets the channel corresponding to the specified data type.
        /// </summary>
        /// <typeparam name="T">
        /// The type of data the channel carries.
        /// </typeparam>
        /// <returns>
        /// The corresponding channel of the specified data type.
        /// </returns>
        /// <remarks>
        /// Attempting to retrieve a non-existent channel is invalid.
        /// </remarks>
        public DataChannel<T> Get<T>()
        {
            Require.IsTrue(DataTypes.Contains(typeof(T)));

            return
                (DataChannel<T>)
                _channel_by_data_type[typeof(T)];
        }

        /// <summary>
        /// Clears all channel back buffers.
        /// </summary>
        public void Clear()
        {
            foreach (var channel in Channels)
            {
                channel.Clear();
            }
        }

        /// <summary>
        /// Swaps all channel buffers.
        /// </summary>
        public void SwapBuffers()
        {
            foreach (var channel in Channels)
            {
                channel.SwapBuffers();
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            string types = string.Join
            (
                ", ",
                DataTypes
                    .Select(item => item.ToString())
                    .ToArray()
            );
            return $"{nameof(ChannelBatch)}(" +
                   $"{nameof(DataTypes)} = [{types}])";
        }

        /// <summary>
        /// Gets an enumeration of all channels.
        /// </summary>
        private IEnumerable<DoubleBufferedDataChannel> Channels
        {
            get { return _channel_by_data_type.Values; }
        }

        private readonly Dictionary<Type, DoubleBufferedDataChannel> 
            _channel_by_data_type =
            new Dictionary<Type, DoubleBufferedDataChannel>();
    }
}
