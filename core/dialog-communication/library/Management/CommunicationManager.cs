﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.DesignPatterns;
using Common.Functional.Options;
using Common.Validation;
using Dialog.Communication.Actors;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dialog.Communication.Management
{
    /// <summary>
    /// This class represents a management system of data emitted/perceived by
    /// actors in the scene.
    /// </summary>
    public sealed partial class CommunicationManager
    {
        /// <summary>
        /// This class is used to build instances of 
        /// <see cref="CommunicationManager"/>.
        /// </summary>
        public sealed class Builder
            : ObjectBuilder<CommunicationManager>
        {
            /// <summary>
            /// Specifies that the manager should support the specified data 
            /// type.
            /// </summary>
            /// <typeparam name="T">The type of data to support.</typeparam>
            /// <returns>This instance (for method call-chaining).</returns>
            /// <remarks>
            /// Calling this on a built instance is invalid.
            /// </remarks>
            public Builder Support<T>()
            {
                Require.IsFalse<InvalidOperationException>(IsBuilt);

                _channel_batch_builder.WithChannel<T>();

                return this;
            }

            /// <summary>
            /// Creates the object.
            /// </summary>
            /// <returns>
            /// The built object.
            /// </returns>
            protected override CommunicationManager CreateObject()
            {
                var batch = _channel_batch_builder.Build();
                return new CommunicationManager(batch);
            }

            private readonly ChannelBatch.Builder
                _channel_batch_builder = new ChannelBatch.Builder();
        }

        /// <summary>
        /// Creates a new manager with the specified channels.
        /// </summary>
        /// <param name="channels">The channels to support.</param>
        private CommunicationManager(ChannelBatch channels)
        {
            _channels = channels;

            Actors = new CollectionView<ManagedActor>
            (
                _subscription_by_actor.Keys
            );
            ActiveDataSubscriptions = new CollectionView<DataSubscription>
            (
                _subscription_by_actor.Values
            );
        }

        /// <summary>
        /// Gets the collection of supported data types.
        /// </summary>
        public ImmutableCollection<Type> DataTypes
        {
            get { return _channels.DataTypes; }
        }

        /// <summary>
        /// Gets the collection of engaged actors.
        /// </summary>
        public ImmutableCollection<ManagedActor> Actors
        {
            get;
            private set;
        }

        /// <summary>
        /// Engages the specified actor in communications.
        /// </summary>
        /// <param name="actor">The actor to engage.</param>
        /// <returns>
        /// A handle to the actor's subscription preferences.
        /// </returns>
        /// <remarks>
        /// <paramref name="actor"/> must not be null.
        /// </remarks>
        public DataSubscription Engage(ManagedActor actor)
        {
            Require.IsNotNull(actor);

            if (Actors.Contains(actor))
            {
                return _subscription_by_actor[actor];
            }
            else
            {
                var subscription = new DataSubscription(this);
                _subscription_by_actor.Add(actor, subscription);

                return subscription;
            }
        }
        /// <summary>
        /// Disengages the specified actor from communications.
        /// </summary>
        /// <param name="actor">The actor to disengage.</param>
        /// <returns>
        /// True if the actor was disengaged, otherwise false if it wasn't 
        /// engaged to begin with.
        /// </returns>
        /// <remarks>
        /// <paramref name="actor"/> must not be null.
        /// </remarks>
        public bool Disengage(ManagedActor actor)
        {
            Require.IsNotNull(actor);

            if (!Actors.Contains(actor)) { return false; }
            else
            {
                _subscription_by_actor.Remove(actor);
                return true;
            }
        }

        /// <summary>
        /// Performs an iteration of the global update routine.
        /// </summary>
        public void Update()
        {
            foreach (var actor in Actors)
            {
                // Perception and processing:
                _subscription_by_actor[actor].Invoke();
                
                // Processing and action:
                var submission = new DataSubmission(this, actor);
                ActiveDataSubmission = Option.CreateSome(submission);
                actor.Act(submission);
            }

            ActiveDataSubmission = Option.CreateNone<DataSubmission>();

            _channels.SwapBuffers();
            _channels.Clear();
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            string types = string.Join
            (
                ", ",
                DataTypes
                    .Select(item => item.ToString())
                    .ToArray()
            );
            return $"{nameof(CommunicationManager)}(" +
                   $"{nameof(DataTypes)} = [{types}])";
        }

        /// <summary>
        /// Gets or sets all active subscriptions.
        /// </summary>
        private ImmutableCollection<DataSubscription> 
            ActiveDataSubscriptions { get; set; }
        
        /// <summary>
        /// Gets or sets the active data submission.
        /// </summary>
        private Option<DataSubmission> ActiveDataSubmission { get; set; } = 
            Option.CreateNone<DataSubmission>();

        private readonly ChannelBatch _channels;
        private readonly Dictionary<ManagedActor, DataSubscription>
            _subscription_by_actor =
            new Dictionary<ManagedActor, DataSubscription>();
    }
}
