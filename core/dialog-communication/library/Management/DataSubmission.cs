﻿using Common.Hashing;
using Common.Validation;
using Dialog.Communication.Actors;
using Dialog.Communication.Packets;
using System;

namespace Dialog.Communication.Management
{
    public sealed partial class CommunicationManager
    {
        /// <summary>
        /// This class represents a front for actors to submit data to the 
        /// communication manager.
        /// </summary>
        public sealed class DataSubmission
        {
            /// <summary>
            /// Creates a new submission with the specified manager and owner.
            /// Any data submitted through this instance will be in the 
            /// owner's name.
            /// </summary>
            /// <param name="manager">The submission's manager.</param>
            /// <param name="owner">The actor in whose name to submit.</param>
            /// <remarks>
            /// <paramref name="manager"/> must not be null.
            /// <paramref name="owner"/> must not be null.
            /// </remarks>
            internal DataSubmission
            (
                CommunicationManager manager, 
                ManagedActor owner
            )
            {
                Require.IsNotNull(manager);
                Require.IsNotNull(owner);

                Manager = manager;
                Owner = owner;
            }

            /// <summary>
            /// Gets this submission's manager.
            /// </summary>
            public CommunicationManager Manager { get; private set; }

            /// <summary>
            /// Gets this submission's owner (the actor in whose name data is
            /// submitted).
            /// </summary>
            public ManagedActor Owner { get; private set; }

            /// <summary>
            /// Indicates whether this submission is active. That is, whether
            /// it is permitted to submit data at this time.
            /// </summary>
            public bool IsActive
            {
                get { return Manager.ActiveDataSubmission.Contains(this); }
            }

            /// <summary>
            /// Adds the specified data to the submission.
            /// </summary>
            /// <typeparam name="T">The type of data to submit.</typeparam>
            /// <param name="data">The data to submit.</param>
            /// <remarks>
            /// This submission must be active.
            /// In the case of given data of an unsupported type, the data is 
            /// discarded.
            /// </remarks>
            public void Add<T>(T data)
            {
                Require.IsTrue<InvalidOperationException>(IsActive);

                if (!Manager.DataTypes.Contains(typeof(T))) { return; }
                else
                {
                    var packet = GenericPacket.Create(Owner, data);
                    Manager._channels.Get<T>().Post(packet);
                }
            }

            /// <summary>
            /// Determines whether the specified object is equal to this instance.
            /// </summary>
            /// <param name="obj">The object to compare with.</param>
            /// <returns>
            /// True if the specified object is equal to this instance; 
            /// otherwise false.
            /// </returns>
            public override bool Equals(object obj)
            {
                var other = obj as DataSubmission;

                if (ReferenceEquals(other, null)) { return false; }
                if (ReferenceEquals(other, this)) { return true; }

                return other.Manager.Equals(Manager) &&
                       other.Owner.Equals(Owner);
            }
            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <returns>
            /// A hash code for this instance, suitable for use in hashing 
            /// algorithms and data structures like a hash table. 
            /// </returns>
            public override int GetHashCode()
            {
                var hash = HashCombiner.Initialize();
                hash = HashCombiner.Hash(hash, Manager);
                hash = HashCombiner.Hash(hash, Owner);

                return hash;
            }

            /// <summary>
            /// Returns a string that represents this instance.
            /// </summary>
            /// <returns>
            /// A human-readable string.
            /// </returns>
            public override string ToString()
            {
                return $"{nameof(DataSubmission)}(" +
                       $"{nameof(Manager)} = {Manager}, " +
                       $"{nameof(Owner)} = {Owner})";
            }
        }
    }
}
