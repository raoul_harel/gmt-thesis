﻿using Dialog.Communication.Actors;
using Moq;
using NUnit.Framework;
using System;

namespace Dialog.Communication.Packets.Test
{
    [TestFixture]
    public sealed class GenericPacketTest
    {
        private static readonly DataAuthor 
            AUTHOR = new Mock<DataAuthor>().Object;
        private static readonly string PAYLOAD = "Hello world!";
        
        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new GenericPacket<string>(null, PAYLOAD)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new GenericPacket<string>(AUTHOR, null)
            );
        }
        [Test]
        public void Test_Constructor()
        {
            var packet = new GenericPacket<string>(AUTHOR, PAYLOAD);

            Assert.AreEqual(AUTHOR, packet.Author);
            Assert.AreEqual(PAYLOAD, packet.Payload);
        }

        [Test]
        public void Test_StaticConstructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => GenericPacket.Create(null, PAYLOAD)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => GenericPacket.Create(AUTHOR, (string)null)
            );
        }
        [Test]
        public void Test_StaticConstructor()
        {
            var packet = GenericPacket.Create(AUTHOR, PAYLOAD);

            Assert.AreEqual(AUTHOR, packet.Author);
            Assert.AreEqual(PAYLOAD, packet.Payload);
        }

        [Test]
        public void Test_Equality()
        {
            var original = new GenericPacket<string>(AUTHOR, PAYLOAD);
            var good_copy = new GenericPacket<string>
            (
                original.Author, 
                original.Payload
            );
            var flawed_author_copy = new GenericPacket<string>
            (
                new Mock<DataAuthor>().Object, 
                original.Payload
            );
            var flawed_payload_copy = new GenericPacket<string>
            (
                original.Author,
                $"wrong {original.Payload}"
            );

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_author_copy);
            Assert.AreNotEqual(original, flawed_payload_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
