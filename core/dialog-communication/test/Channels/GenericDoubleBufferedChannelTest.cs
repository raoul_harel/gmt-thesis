﻿using Dialog.Communication.Packets;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace Dialog.Communication.Channels.Test
{
    [TestFixture]
    public sealed class GenericDoubleBufferedChannelTest
    {
        private static readonly Mock<DataPacket<string>> 
            PACKET = new Mock<DataPacket<string>>();

        private GenericDoubleBufferedChannel<string> _channel;

        [SetUp]
        public void Setup()
        {
            _channel = new GenericDoubleBufferedChannel<string>();
        }

        [Test]
        public void Test_InitialState()
        {
            Assert.AreEqual(typeof(string), _channel.DataType);
            Assert.AreEqual(0, _channel.Packets.Count());
        }

        [Test]
        public void Test_Posting_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _channel.Post(null)
            );
        }
        [Test]
        public void Test_Posting()
        {
            _channel.Post(PACKET.Object);

            Assert.AreEqual(0, _channel.Packets.Count);

            _channel.SwapBuffers();

            Assert.AreEqual(1, _channel.Packets.Count);
            Assert.AreSame(PACKET.Object, _channel.Packets.First());
        }

        [Test]
        public void Test_Clearing()
        {
            _channel.Post(PACKET.Object);
            _channel.SwapBuffers();
            _channel.Post(PACKET.Object);
            _channel.Clear();

            Assert.AreEqual(1, _channel.Packets.Count);
            Assert.AreSame(PACKET.Object, _channel.Packets.First());

            _channel.SwapBuffers();

            Assert.AreEqual(0, _channel.Packets.Count);
        }
    }
}
