﻿using Dialog.Communication.Actors;
using Moq;
using NUnit.Framework;
using System;
using static Dialog.Communication.Management.CommunicationManager.DataSubscription;

namespace Dialog.Communication.Management.Test
{
    public sealed partial class CommunicationManagerTest
    {
        [Test]
        public void Test_Subscription_InitialState()
        {
            var actor = new Mock<ManagedActor>().Object;

            var subscription = _manager.Engage(actor);

            Assert.IsTrue(subscription.IsActive);
            Assert.AreSame(_manager, subscription.Manager);
            Assert.AreEqual(0, subscription.DataTypes.Count);
        }

        [Test]
        public void Test_Subscription_AfterDisengagement()
        {
            var actor = new Mock<ManagedActor>().Object;

            var subscription = _manager.Engage(actor);
            _manager.Disengage(actor);

            Assert.IsFalse(subscription.IsActive);
        }

        [Test]
        public void Test_Subscription_TypeInclusion_WithInvalidArgs()
        {
            var actor = new Mock<ManagedActor>().Object;
            Handler<string> callback = (manager, packets) => { };

            var subscription = _manager.Engage(actor);

            Assert.Throws<ArgumentNullException>
            (
                () => subscription.Include<string>(null)
            );
            Assert.Throws<ArgumentException>
            (
                () => subscription.Include(callback)
            );
        }
        [Test]
        public void Test_Subscription_TypeInclusion()
        {
            var actor = new Mock<ManagedActor>().Object;
            Handler<int> callback = (manager, packets) => { };

            var subscription = _manager.Engage(actor);
            subscription.Include(callback);

            Assert.AreEqual(1, subscription.DataTypes.Count);
            Assert.IsTrue(subscription.DataTypes.Contains(typeof(int)));
        }

        [Test]
        public void Test_Subscription_TypeExclusion_Generic()
        {
            var actor = new Mock<ManagedActor>().Object;
            Handler<int> callback = (manager, packets) => { };

            var subscription = _manager.Engage(actor);
            subscription.Include(callback);
            subscription.Exclude<int>();

            Assert.AreEqual(0, subscription.DataTypes.Count);
        }
        [Test]
        public void Test_Subscription_TypeExclusion()
        {
            var actor = new Mock<ManagedActor>().Object;
            Handler<int> callback = (manager, packets) => { };

            var subscription = _manager.Engage(actor);
            subscription.Include(callback);
            subscription.Exclude(typeof(int));

            Assert.AreEqual(0, subscription.DataTypes.Count);
        }
    }
}
