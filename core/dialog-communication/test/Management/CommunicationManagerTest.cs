﻿using Dialog.Communication.Actors;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using static Dialog.Communication.Management.CommunicationManager;
using static Dialog.Communication.Management.CommunicationManager.DataSubscription;

namespace Dialog.Communication.Management.Test
{
    [TestFixture]
    public sealed partial class CommunicationManagerTest
    {
        private CommunicationManager _manager;

        [SetUp]
        public void Setup()
        {
            _manager = new CommunicationManager.Builder()
                .Support<int>()
                .Build();
        }

        [Test]
        public void Test_InitialState()
        {
            Assert.AreEqual(1, _manager.DataTypes.Count);
            Assert.IsTrue(_manager.DataTypes.Contains(typeof(int)));

            Assert.AreEqual(0, _manager.Actors.Count);
        }

        [Test]
        public void Test_ActorEngagement_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _manager.Engage(null)
            );
        }
        [Test]
        public void Test_ActorEngagement()
        {
            var actor = new Mock<ManagedActor>().Object;

            _manager.Engage(actor);

            Assert.AreEqual(1, _manager.Actors.Count);
            Assert.IsTrue(_manager.Actors.Contains(actor));
        }

        [Test]
        public void Test_ActorDisengagement_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _manager.Disengage(null)
            );
        }
        [Test]
        public void Test_ActorDisengagement_OfDisengagedActor()
        {
            var actor = new Mock<ManagedActor>().Object;

            Assert.IsFalse(_manager.Disengage(actor));
        }
        [Test]
        public void Test_ActorDisengagement()
        {
            var actor = new Mock<ManagedActor>().Object;

            _manager.Engage(actor);

            Assert.IsTrue(_manager.Disengage(actor));
            Assert.AreEqual(0, _manager.Actors.Count);
        }

        [Test]
        public void Test_Update()
        {
            var actor = new Mock<ManagedActor>();
            int data = 1;

            Handler<int> perception_callback_a = 
                (manager, packets) =>
            {
                Assert.AreSame(_manager, manager);
                Assert.AreEqual(0, packets.Count());
            };
            Handler<int> perception_callback_b = 
                (manager, packets) =>
            {
                Assert.AreSame(_manager, manager);
                Assert.AreEqual(1, packets.Count());

                var packet = packets.First();

                Assert.AreSame(actor.Object, packet.Author);
                Assert.AreEqual(data, packet.Payload);
            };

            DataSubmission data_submission = null;
            Action<DataSubmission> action_callback = submission =>
            {
                Assert.IsTrue(submission.IsActive);
                Assert.AreSame(_manager, submission.Manager);

                submission.Add(data);
                data_submission = submission;
            };

            actor
                .Setup(self => self.Act(It.IsAny<DataSubmission>()))
                .Callback(action_callback);

            var subscription = _manager.Engage(actor.Object);
            subscription.Include(perception_callback_a);

            _manager.Update();

            Assert.IsFalse(data_submission.IsActive);

            subscription.Include(perception_callback_b);

            _manager.Update();

            Assert.IsFalse(data_submission.IsActive);
        }
    }
}
