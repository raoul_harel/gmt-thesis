var searchData=
[
  ['idlemove',['IdleMove',['../class_dialog_1_1_agency_1_1_dialog___moves_1_1_idle_move.html',1,'Dialog::Agency::Dialog_Moves']]],
  ['immutablecollection',['ImmutableCollection',['../interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html',1,'Common.Collections.Interfaces.ImmutableCollection'],['../interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html',1,'Common.Collections.Interfaces.ImmutableCollection&lt; T &gt;']]],
  ['immutablecollection_3c_20actor_20_3e',['ImmutableCollection&lt; Actor &gt;',['../interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html',1,'Common::Collections::Interfaces']]],
  ['immutablecollection_3c_20t_20_3e',['ImmutableCollection&lt; T &gt;',['../interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html',1,'Common::Collections::Interfaces']]],
  ['immutablelist',['ImmutableList',['../interface_common_1_1_collections_1_1_interfaces_1_1_immutable_list.html',1,'Common::Collections::Interfaces']]],
  ['immutablestate',['ImmutableState',['../interface_dialog_1_1_agency_1_1_state_1_1_immutable_state.html',1,'Dialog::Agency::State']]],
  ['informationstate',['InformationState',['../class_dialog_1_1_agency_1_1_state_1_1_information_state.html',1,'Dialog::Agency::State']]]
];
