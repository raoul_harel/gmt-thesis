var namespace_common_1_1_functional_1_1_options =
[
    [ "None", "struct_common_1_1_functional_1_1_options_1_1_none.html", "struct_common_1_1_functional_1_1_options_1_1_none" ],
    [ "Option", "interface_common_1_1_functional_1_1_options_1_1_option.html", "interface_common_1_1_functional_1_1_options_1_1_option" ],
    [ "Some", "struct_common_1_1_functional_1_1_options_1_1_some.html", "struct_common_1_1_functional_1_1_options_1_1_some" ]
];