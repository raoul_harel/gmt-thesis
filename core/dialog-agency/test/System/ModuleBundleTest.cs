﻿using Dialog.Agency.Modules.Action;
using Dialog.Agency.Modules.Perception;
using Dialog.Agency.Modules.Deliberation;
using Dialog.Agency.State;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace Dialog.Agency.System.Test
{
    [TestFixture]
    public sealed class ModuleBundleTest
    {
        private static readonly RecentActivityPerceptionModule
            RAP_MODULE = new RecentActivityPerceptionModuleStub();
        private static readonly CurrentActivityPerceptionModule
            CAP_MODULE = new CurrentActivityPerceptionModuleStub();
        private static readonly StateUpdateModule
            STATE_UPDATE_MODULE = new StateUpdateModuleStub();
        private static readonly ActionSelectionModule
            ACTION_SELECTION_MODULE = new ActionSelectionModuleStub();
        private static readonly ActionTimingModule
            ACTION_TIMING_MODULE = new ActionTimingModuleStub();
        private static readonly ActionRealizationModule
            ACTION_REALIZATION_MODULE = new ActionRealizationModuleStub();

        private ModuleBundle.Builder _builder;
        private ModuleBundle _bundle;

        [SetUp]
        public void Setup()
        {
            _builder = new ModuleBundle.Builder();
            _bundle = new ModuleBundle.Builder()
                .WithRecentActivityPerceptionBy(RAP_MODULE)
                .WithCurrentActivityPerceptionBy(CAP_MODULE)
                .WithStateUpdateBy(STATE_UPDATE_MODULE)
                .WithActionSelectionBy(ACTION_SELECTION_MODULE)
                .WithActionTimingBy(ACTION_TIMING_MODULE)
                .WithActionRealizationBy(ACTION_REALIZATION_MODULE)
                .Build();
        }

        [Test]
        public void Test_Builder_WithInvalidArgs_Null()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _builder.WithRecentActivityPerceptionBy(null)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => _builder.WithCurrentActivityPerceptionBy(null)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => _builder.WithStateUpdateBy(null)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => _builder.WithActionSelectionBy(null)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => _builder.WithActionTimingBy(null)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => _builder.WithActionRealizationBy(null)
            );
        }
        [Test]
        public void Test_Builder_WithInvalidArgs_InitializedModules()
        {
            var state = new Mock<MutableState>().Object;

            var RAP_stub = new RecentActivityPerceptionModuleStub();
            var CAP_stub = new CurrentActivityPerceptionModuleStub();
            var state_update_stub = new StateUpdateModuleStub();
            var action_selection_stub = new ActionSelectionModuleStub();
            var action_timing_stub = new ActionTimingModuleStub();
            var action_realization_stub = new ActionRealizationModuleStub();

            RAP_stub.Initialize(state);
            CAP_stub.Initialize(state);
            state_update_stub.Initialize(state);
            action_selection_stub.Initialize(state);
            action_timing_stub.Initialize(state);
            action_realization_stub.Initialize(state);

            Assert.Throws<ArgumentException>
            (
                () => _builder.WithRecentActivityPerceptionBy(RAP_stub)
            );
            Assert.Throws<ArgumentException>
            (
                () => _builder.WithCurrentActivityPerceptionBy(CAP_stub)
            );
            Assert.Throws<ArgumentException>
            (
                () => _builder.WithStateUpdateBy(state_update_stub)
            );
            Assert.Throws<ArgumentException>
            (
                () => _builder.WithActionSelectionBy(action_selection_stub)
            );
            Assert.Throws<ArgumentException>
            (
                () => _builder.WithActionTimingBy(action_timing_stub)
            );
            Assert.Throws<ArgumentException>
            (
                () => _builder.WithActionRealizationBy(action_realization_stub)
            );
        }

        [Test]
        public void Test_Constructor()
        {
            Assert.AreSame(RAP_MODULE, _bundle.RecentActivityPerception);
            Assert.AreSame(CAP_MODULE, _bundle.CurrentActivityPerception);
            Assert.AreSame(STATE_UPDATE_MODULE, _bundle.StateUpdate);
            Assert.AreSame(ACTION_SELECTION_MODULE, _bundle.ActionSelection);
            Assert.AreSame(ACTION_TIMING_MODULE, _bundle.ActionTiming);
            Assert.AreSame
            (
                ACTION_REALIZATION_MODULE, 
                _bundle.ActionRealization
            );
        }

        [Test]
        public void Test_Enumeration()
        {
            var list = _bundle.ToList();

            Assert.AreEqual(6, list.Count);

            Assert.IsTrue(list.Contains(_bundle.RecentActivityPerception));
            Assert.IsTrue(list.Contains(_bundle.CurrentActivityPerception));
            Assert.IsTrue(list.Contains(_bundle.StateUpdate));
            Assert.IsTrue(list.Contains(_bundle.ActionSelection));
            Assert.IsTrue(list.Contains(_bundle.ActionTiming));
            Assert.IsTrue(list.Contains(_bundle.ActionRealization));
        }
    }
}
