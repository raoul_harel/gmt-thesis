﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Dialog_Moves.Test.Utilities;
using Dialog.Agency.Modules.Action;
using Dialog.Agency.Modules.Deliberation;
using Dialog.Agency.Modules.Perception;
using Dialog.Agency.State;
using Moq;
using NUnit.Framework;
using System;

namespace Dialog.Agency.System.Test
{
    [TestFixture]
    public sealed class AgencySystemTest
    {
        private static readonly DialogMove 
            TARGET_MOVE = new MockDialogMove(MockMoveKind.A);

        private sealed class MockRAPModule
            : RecentActivityPerceptionModule
        {
            public Mock<RecentActivityPerceptionModule> Mock
            {
                get;
                private set;
            } = new Mock<RecentActivityPerceptionModule>();

            public int InvocationCount { get; private set; } = 0;
            public RecentActivityReport SubmittedReport { get; private set; }

            protected override void ComposeActivityReport
            (
                RecentActivityReport report
            )
            {
                var mock_report = Mock.Object.PerceiveActivity();
                
                foreach (var @event in mock_report.Events)
                {
                    report.Add(@event);
                }

                ++ InvocationCount;
                SubmittedReport = report;

            }
        }
        private sealed class MockCAPModule
            : CurrentActivityPerceptionModule
        {
            public Mock<CurrentActivityPerceptionModule> Mock
            {
                get;
                private set;
            } = new Mock<CurrentActivityPerceptionModule>();

            public int InvocationCount { get; private set; } = 0;
            public CurrentActivityReport SubmittedReport { get; private set; }

            protected override void ComposeActivityReport
            (
                CurrentActivityReport report
            )
            {
                var mock_report = Mock.Object.PerceiveActivity();

                foreach (var actor in mock_report.ActiveActors)
                {
                    report.AddAsActive(actor);
                }
                foreach (var actor in mock_report.PassiveActors)
                {
                    report.AddAsPassive(actor);
                }

                ++ InvocationCount;
                SubmittedReport = report;
        }
    }
        private sealed class MockStateUpdateModule
            : StateUpdateModule
        {
            public Mock<StateUpdateModule> Mock
            {
                get;
                private set;
            } = new Mock<StateUpdateModule>();

            public override void PerformUpdate
            (
                RecentActivityReport recent_activity, 
                CurrentActivityReport current_activity, 
                SystemActivitySnapshot system_activity
            )
            {
                Mock.Object.PerformUpdate
                (
                    recent_activity,
                    current_activity,
                    system_activity
                );
            }
        }
        private sealed class MockActionSelectionModule
            : ActionSelectionModule
        {
            public Mock<ActionSelectionModule> Mock
            {
                get;
                private set;
            } = new Mock<ActionSelectionModule>();

            public override DialogMove SelectMove()
            {
                return Mock.Object.SelectMove();
            }
        }
        private sealed class MockActionTimingModule
            : ActionTimingModule
        {
            public Mock<ActionTimingModule> Mock
            {
                get;
                private set;
            } = new Mock<ActionTimingModule>();

            public override bool IsValidMoveNow(DialogMove move)
            {
                return Mock.Object.IsValidMoveNow(move);
            }
        }
        private sealed class MockActionRealizationModule
            : ActionRealizationModule
        {
            public Mock<ActionRealizationModule> Mock
            {
                get;
                private set;
            } = new Mock<ActionRealizationModule>();

            public override ActionRealizationStatus RealizeMove
            (
                DialogMove move
            )
            {
                return Mock.Object.RealizeMove(move);
            }
        }

        private InformationState _state;

        private MockRAPModule _RAP_module;
        private MockCAPModule _CAP_module;
        private MockStateUpdateModule _state_update_module;
        private MockActionSelectionModule _action_selection_module;
        private MockActionTimingModule _action_timing_module;
        private MockActionRealizationModule _action_realization_module;
        private ModuleBundle _modules;

        private AgencySystem _system;

        [SetUp]
        public void Setup()
        {
            _state = new InformationState.Builder().Build();

            _RAP_module = new MockRAPModule();
            _CAP_module = new MockCAPModule();
            _state_update_module = new MockStateUpdateModule();
            _action_selection_module = new MockActionSelectionModule();
            _action_timing_module = new MockActionTimingModule();
            _action_realization_module = new MockActionRealizationModule();

            _modules = new ModuleBundle.Builder()
                .WithRecentActivityPerceptionBy(_RAP_module)
                .WithCurrentActivityPerceptionBy(_CAP_module)
                .WithStateUpdateBy(_state_update_module)
                .WithActionSelectionBy(_action_selection_module)
                .WithActionTimingBy(_action_timing_module)
                .WithActionRealizationBy(_action_realization_module)
                .Build();

            _system = new AgencySystem(_state, _modules);

            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(IdleMove.Instance);
            _action_timing_module.Mock
                .Setup(x => x.IsValidMoveNow(It.IsAny<DialogMove>()))
                .Returns(true);
            _action_realization_module.Mock
                .Setup(x => x.RealizeMove(It.IsAny<DialogMove>()))
                .Returns(ActionRealizationStatus.Complete);
        }
        
        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new AgencySystem(null, _modules)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new AgencySystem(_state, null)
            );
        }

        [Test]
        public void Test_InitialState()
        {
            Assert.AreEqual(IdleMove.Instance, _system.RecentMove);
            Assert.AreEqual(IdleMove.Instance, _system.TargetMove);
            Assert.AreEqual(IdleMove.Instance, _system.ActualMove);
        }

        [Test]
        public void Test_ModuleInvocation_RecentActivityPerception()
        {
            _system.Step();

            Assert.AreEqual(1, _RAP_module.InvocationCount);
        }
        [Test]
        public void Test_ModuleInvocation_CurrentActivityPerception()
        {
            _system.Step();

            Assert.AreEqual(1, _CAP_module.InvocationCount);
        }
        [Test]
        public void Test_ModuleInvocation_StateUpdate()
        {
            var system_activity = new SystemActivitySnapshot
            (
                _system.RecentMove,
                _system.TargetMove,
                _system.ActualMove
            );

            _system.Step();

            _state_update_module.Mock.Verify
            (
                x => x.PerformUpdate
                (
                    _RAP_module.SubmittedReport, 
                    _CAP_module.SubmittedReport, 
                    system_activity
                ), 
                Times.Once
            );
        }
        [Test]
        public void Test_ModuleInvocation_ActionSelection()
        {
            _system.Step();

            _action_selection_module.Mock
                .Verify(x => x.SelectMove(), Times.Once);
        }
        [Test]
        public void Test_ModuleInvocation_ActionTiming()
        {
            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(TARGET_MOVE);

            _system.Step();

            _action_timing_module.Mock.Verify
            (
                x => x.IsValidMoveNow(TARGET_MOVE), 
                Times.Once
            );
        }
        [Test]
        public void Test_ModuleInvocation_ActionRealization()
        {
            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(TARGET_MOVE);

            _system.Step();

            _action_realization_module.Mock.Verify
            (
                x => x.RealizeMove(TARGET_MOVE), 
                Times.Once
            );
        }

        [Test]
        public void Test_Stepping_WithNullTargetMove()
        {
            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns((DialogMove)null);

            Assert.Throws<InvalidOperationException>
            (
                () => _system.Step()
            );
        }
        [Test]
        public void Test_Stepping_WithIdleTargetMove()
        {
            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(IdleMove.Instance);

            _system.Step();

            Assert.AreEqual(IdleMove.Instance, _system.RecentMove);
            Assert.AreEqual(IdleMove.Instance, _system.TargetMove);
            Assert.AreEqual(IdleMove.Instance, _system.ActualMove);
        }
        [Test]
        public void Test_Stepping_WithInvalidTimingOfTargetMove()
        {
            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(TARGET_MOVE);
            _action_timing_module.Mock
                .Setup(x => x.IsValidMoveNow(TARGET_MOVE))
                .Returns(false);

            _system.Step();

            Assert.AreEqual(IdleMove.Instance, _system.RecentMove);
            Assert.AreEqual(TARGET_MOVE, _system.TargetMove);
            Assert.AreEqual(IdleMove.Instance, _system.ActualMove);
        }
        [Test]
        public void Test_Stepping_WithTargetMoveInProgress()
        {
            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(TARGET_MOVE);
            _action_timing_module.Mock
                .Setup(x => x.IsValidMoveNow(TARGET_MOVE))
                .Returns(true);
            _action_realization_module.Mock
                .Setup(x => x.RealizeMove(TARGET_MOVE))
                .Returns(ActionRealizationStatus.InProgress);

            _system.Step();

            Assert.AreEqual(IdleMove.Instance, _system.RecentMove);
            Assert.AreEqual(TARGET_MOVE, _system.TargetMove);
            Assert.AreEqual(TARGET_MOVE, _system.ActualMove);
        }
        [Test]
        public void Test_Stepping_WithTargetMoveComplete()
        {
            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(TARGET_MOVE);
            _action_timing_module.Mock
                .Setup(x => x.IsValidMoveNow(TARGET_MOVE))
                .Returns(true);
            _action_realization_module.Mock
                .Setup(x => x.RealizeMove(TARGET_MOVE))
                .Returns(ActionRealizationStatus.Complete);

            _system.Step();

            Assert.AreEqual(TARGET_MOVE, _system.RecentMove);
            Assert.AreEqual(TARGET_MOVE, _system.TargetMove);
            Assert.AreEqual(TARGET_MOVE, _system.ActualMove);
        }
        [Test]
        public void Test_Stepping_WithMoveCancellationByInvalidTiming()
        {
            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(TARGET_MOVE);
            _action_timing_module.Mock
                .Setup(x => x.IsValidMoveNow(TARGET_MOVE))
                .Returns(true);
            _action_realization_module.Mock
                .Setup(x => x.RealizeMove(TARGET_MOVE))
                .Returns(ActionRealizationStatus.InProgress);
            _action_realization_module.Mock
                .Setup(x => x.RealizeMove(IdleMove.Instance))
                .Returns(ActionRealizationStatus.Complete);

            _system.Step();

            _action_timing_module.Mock
                .Setup(x => x.IsValidMoveNow(TARGET_MOVE))
                .Returns(false);

            _system.Step();

            Assert.AreEqual(IdleMove.Instance, _system.RecentMove);
            Assert.AreEqual(TARGET_MOVE, _system.TargetMove);
            Assert.AreEqual(IdleMove.Instance, _system.ActualMove);
        }
        [Test]
        public void Test_Stepping_WithMoveCancellationByChangeOfTarget()
        {
            var target_move_b = new MockDialogMove(MockMoveKind.B);
            var non_instant_moves = new DialogMove[]
            {
                TARGET_MOVE,
                target_move_b
            };

            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(TARGET_MOVE);
            _action_timing_module.Mock
                .Setup(x => x.IsValidMoveNow(It.IsAny<DialogMove>()))
                .Returns(true);
            _action_realization_module.Mock
                .Setup(x => x.RealizeMove(It.IsIn(non_instant_moves)))
                .Returns(ActionRealizationStatus.InProgress);
            _action_realization_module.Mock
                .Setup(x => x.RealizeMove(IdleMove.Instance))
                .Returns(ActionRealizationStatus.Complete);

            _system.Step();

            _action_selection_module.Mock
                .Setup(x => x.SelectMove())
                .Returns(target_move_b);

            _system.Step();

            Assert.AreEqual(IdleMove.Instance, _system.RecentMove);
            Assert.AreEqual(target_move_b, _system.TargetMove);
            Assert.AreEqual(target_move_b, _system.ActualMove);
        }
    }
}
