﻿using Common.Functional.Options.Extensions;
using Dialog.Agency.Dialog_Moves.Test.Utilities;
using Dialog.Agency.Scene;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Dialog.Agency.Dialog_Moves.Test
{
    [TestFixture]
    public sealed class DialogMoveTest
    {
        private static readonly List<Actor> 
            TARGETS = new List<Actor>
            {
                new Mock<Actor>().Object,
                new Mock<Actor>().Object
            };
        private static readonly Actor TARGET = TARGETS[0];

        private MockDialogMove _multiple_target_move;
        private MockDialogMove _single_target_move;
        private MockDialogMove _zero_target_move;

        [SetUp]
        public void Setup()
        {
            _multiple_target_move = new MockDialogMove(MockMoveKind.A, TARGETS);
            _single_target_move = new MockDialogMove(MockMoveKind.B, TARGET);
            _zero_target_move = new MockDialogMove(MockMoveKind.C);
        }

        [Test]
        public void Test_MultipleTargetConstructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new MockDialogMove(null, TARGETS)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new MockDialogMove
                (
                    MockMoveKind.A, 
                    (IEnumerable<Actor>)null
                )
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new MockDialogMove
                (
                    MockMoveKind.A, 
                    new Actor[1] { null }
                )
            );
        }
        [Test]
        public void Test_MultipleTargetConstructor_WithDuplicates()
        {
            var duplicate_targets = new Actor[2] { TARGETS[0], TARGETS[0] };
            _multiple_target_move = 
                new MockDialogMove(MockMoveKind.A, duplicate_targets);

            Assert.AreEqual(1, _multiple_target_move.Targets.Count);
            Assert.IsTrue(_multiple_target_move.Target.IsSome());
            Assert.IsTrue(_multiple_target_move.Target.Contains(TARGETS[0]));
        }
        [Test]
        public void Test_MultipleTargetConstructor()
        {
            Assert.IsFalse(_multiple_target_move.Target.IsSome());

            Assert.AreEqual(TARGETS, _multiple_target_move.TargetEnumeration);
        }

        [Test]
        public void Test_SingleTargetConstructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new MockDialogMove(null, TARGET)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new MockDialogMove(MockMoveKind.A, (Actor)null)
            );
        }
        [Test]
        public void Test_SingleTargetConstructor()
        {
            Assert.IsTrue(_single_target_move.Target.Contains(TARGET));

            Assert.AreEqual(1, _single_target_move.Targets.Count);
            Assert.IsTrue(_single_target_move.Targets.Contains(TARGET));
        }

        [Test]
        public void Test_ZeroTargetConstructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new MockDialogMove(null)
            );
        }
        [Test]
        public void Test_ZeroTargetConstructor()
        {
            Assert.IsFalse(_zero_target_move.Target.IsSome());

            Assert.AreEqual(0, _zero_target_move.Targets.Count);
        }

        [Test]
        public void Test_Equality()
        {
            var original = _multiple_target_move;
            var good_copy = new MockDialogMove(original.Kind, original.TargetEnumeration);
            var flawed_kind_copy =
                new MockDialogMove(MockMoveKind.C, original.TargetEnumeration);
            var flawed_targets_copy = 
                new MockDialogMove(original.Kind, new List<Actor>());

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_kind_copy);
            Assert.AreNotEqual(original, flawed_targets_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
