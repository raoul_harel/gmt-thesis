﻿using Dialog.Agency.Scene;
using System.Collections.Generic;

namespace Dialog.Agency.Dialog_Moves.Test.Utilities
{
    public sealed class MockMoveKind
         : DialogMoveKind
    {
        public static readonly DialogMoveKind A =
            new MockMoveKind("A");
        public static readonly DialogMoveKind B =
            new MockMoveKind("B");
        public static readonly DialogMoveKind C =
            new MockMoveKind("C");

        private MockMoveKind(string label)
            : base(label)
        { }
    }
    public sealed class MockDialogMove
        : DialogMove<Actor>
    {
        public MockDialogMove
        (
            DialogMoveKind kind,
            IEnumerable<Actor> targets
        )
            : base(kind, targets)
        { }
        public MockDialogMove
        (
            DialogMoveKind kind,
            Actor target
        )
            : base(kind, target)
        { }
        public MockDialogMove(DialogMoveKind kind)
            : base(kind)
        { }
    }
}
