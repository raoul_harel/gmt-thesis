﻿using Common.Collections.Extensions;
using Common.Functional.Options.Extensions;
using NUnit.Framework;

namespace Dialog.Agency.Dialog_Moves.Test
{
    [TestFixture]
    public sealed class IdleMoveTest
    {
        [Test]
        public void Test_Singleton_Access()
        {
            Assert.IsNotNull(IdleMove.Instance);
        }
        [Test]
        public void Test_Singleton_Instance()
        {
            var move = IdleMove.Instance;

            Assert.AreSame(DialogMoveKind.Idle, move.Kind);
            Assert.IsTrue(move.Targets.IsEmpty());
            Assert.IsFalse(move.Target.IsSome());
        }
    }
}
