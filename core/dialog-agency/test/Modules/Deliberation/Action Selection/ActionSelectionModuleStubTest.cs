﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Dialog_Moves.Extensions;
using Dialog.Agency.State;
using Moq;
using NUnit.Framework;

namespace Dialog.Agency.Modules.Deliberation.Test
{
    [TestFixture]
    public sealed class ActionSelectionModuleStubTest
    {
        private static readonly ImmutableState 
            STATE = new Mock<ImmutableState>().Object;

        private ActionSelectionModuleStub _stub;

        [SetUp]
        public void Setup()
        {
            _stub = new ActionSelectionModuleStub();
            _stub.Initialize(STATE);
        }

        [Test]
        public void Test_MoveSelection()
        {
            DialogMove target_move = _stub.SelectMove();

            Assert.IsTrue(target_move.IsIdle());
        }
    }
}
