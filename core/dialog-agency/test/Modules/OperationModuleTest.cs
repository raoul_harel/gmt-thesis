﻿using Common.Collections.Views;
using Dialog.Agency.State;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Dialog.Agency.Modules.Test
{
    [TestFixture]
    public sealed class OperationModuleTest
    {
        private sealed class MockModule
            : OperationModule<ImmutableState>
        {
            public bool SetupWasInvoked { get; private set; } = false;

            public override void GetRequiredStateComponents
            (
                ICollection<Type> types
            )
            {
                types.Add(typeof(int));
            }

            public override void Setup()
            {
                Assert.IsTrue(IsInitialized);
                Assert.IsNotNull(State);

                var required_state_data_types = new HashSet<Type>();
                GetRequiredStateComponents(required_state_data_types);

                foreach (var type in required_state_data_types)
                {
                    Assert.IsTrue(State.SupportedComponents.Contains(type));
                }

                SetupWasInvoked = true;
            }
        }

        private Mock<ImmutableState> _state;
        private MockModule _module;

        [SetUp]
        public void Setup()
        {
            var supported_state_data_types = new CollectionView<Type>
            (
                new Type[1] { typeof(int) }
            );
            _state = new Mock<ImmutableState>();
            _state
                .SetupGet(x => x.SupportedComponents)
                .Returns(supported_state_data_types);

            _module = new MockModule();
        }

        [Test]
        public void Test_Initialization_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _module.Initialize(null)
            );
        }
        [Test]
        public void Test_Initialization_WhenAlreadyInitialized()
        {
            _module.Initialize(_state.Object);

            Assert.Throws<InvalidOperationException>
            (
                () => _module.Initialize(_state.Object)
            );
        }
        [Test]
        public void Test_Initialization_WithIncompleteState()
        {
            var empty_collection = new CollectionView<Type>(new Type[0]);
            _state
                .SetupGet(x => x.SupportedComponents)
                .Returns(empty_collection);

            Assert.Throws<ArgumentException>
            (
                () => _module.Initialize(_state.Object)
            );
        }
        [Test]
        public void Test_Initialization()
        {
            _module.Initialize(_state.Object);

            Assert.IsTrue(_module.IsInitialized);
            Assert.AreSame(_state.Object, _module.State);
            Assert.IsTrue(_module.SetupWasInvoked);
        }
    }
}
