﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.State;
using Moq;
using NUnit.Framework;

namespace Dialog.Agency.Modules.Action.Test
{
    [TestFixture]
    public sealed class ActionRealizationModuleStubTest
    {
        private static readonly ImmutableState 
            STATE = new Mock<ImmutableState>().Object;
        private static readonly DialogMove
            MOVE = new Mock<DialogMove>().Object;

        private ActionRealizationModuleStub _stub;

        [SetUp]
        public void Setup()
        {
            _stub = new ActionRealizationModuleStub();
            _stub.Initialize(STATE);
        }

        [Test]
        public void Test_MoveRealization()
        {
            ActionRealizationStatus 
                realization_status = _stub.RealizeMove(MOVE);

            Assert.AreEqual
            (
                ActionRealizationStatus.Complete, 
                realization_status
            );
        }
    }
}
