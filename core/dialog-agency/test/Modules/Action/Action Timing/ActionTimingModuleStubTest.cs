﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.State;
using Moq;
using NUnit.Framework;

namespace Dialog.Agency.Modules.Action.Test
{
    [TestFixture]
    public sealed class ActionTimingModuleStubTest
    {
        private static readonly ImmutableState 
            STATE = new Mock<ImmutableState>().Object;
        private static readonly DialogMove
            MOVE = new Mock<DialogMove>().Object;

        private ActionTimingModuleStub _stub;

        [SetUp]
        public void Setup()
        {
            _stub = new ActionTimingModuleStub();
            _stub.Initialize(STATE);
        }

        [Test]
        public void Test_ValidityCheck()
        {
            Assert.IsTrue(_stub.IsValidMoveNow(MOVE));
        }
    }
}
