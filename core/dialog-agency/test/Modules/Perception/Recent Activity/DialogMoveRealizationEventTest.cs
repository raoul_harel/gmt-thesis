﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Scene;
using Moq;
using NUnit.Framework;
using System;

namespace Dialog.Agency.Modules.Perception.Test
{
    [TestFixture]
    public sealed class DialogMoveRealizationEventTest
    {
        private static readonly Actor 
            SOURCE = new Mock<Actor>().Object;
        private static readonly DialogMove 
            MOVE = new Mock<DialogMove>().Object;

        private DialogMoveRealizationEvent _event;

        [SetUp]
        public void Setup()
        {
            _event = new DialogMoveRealizationEvent(SOURCE, MOVE);
        }

        [Test]
        public void Test_Construcor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new DialogMoveRealizationEvent(null, MOVE)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new DialogMoveRealizationEvent(SOURCE, null)
            );
        }
        [Test]
        public void Test_Constructor()
        {
            Assert.AreSame(SOURCE, _event.Source);
            Assert.AreSame(MOVE, _event.Move);
        }

        [Test]
        public void Test_Equality()
        {
            var original = _event;
            var good_copy = new DialogMoveRealizationEvent
            (
                original.Source, 
                original.Move
            );
            var flawed_source_copy = new DialogMoveRealizationEvent
            (
                new Mock<Actor>().Object,
                original.Move
            );
            var flawed_move_copy = new DialogMoveRealizationEvent
            (
                original.Source,
                new Mock<DialogMove>().Object
            );

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_source_copy);
            Assert.AreNotEqual(original, flawed_move_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
