﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Scene;
using Moq;
using NUnit.Framework;
using System;

namespace Dialog.Agency.Modules.Perception.Test
{
    [TestFixture]
    public sealed class RecentActivityReportTest
    {
        private static readonly Actor 
            ALICE = new Mock<Actor>().Object;
        private static readonly DialogMove 
            GREETING = new Mock<DialogMove>().Object;
        private static readonly DialogMoveRealizationEvent 
            ALICE_GREETING = new DialogMoveRealizationEvent(ALICE, GREETING);

        private RecentActivityReport _report;

        [SetUp]
        public void Setup()
        {
            _report = new RecentActivityReport();
        }

        [Test]
        public void Test_InitialState()
        {
            Assert.AreEqual(0, _report.Events.Count);
        }

        [Test]
        public void Test_IndirectEventAddition_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _report.Add(null, GREETING)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => _report.Add(ALICE, null)
            );
        }
        [Test]
        public void Test_IndirectEventAddition_WithIdleMove()
        {
            bool success = _report.Add(ALICE, IdleMove.Instance);

            Assert.IsFalse(success);
        }
        [Test]
        public void Test_IndirectEventAddition_OfExistingEvent()
        {
            _report.Add(ALICE, GREETING);
            bool success = _report.Add(ALICE, GREETING);

            Assert.IsFalse(success);
            Assert.AreEqual(1, _report.Events.Count);
        }
        [Test]
        public void Test_IndirectEventAddition()
        {
            bool success = _report.Add(ALICE, GREETING);

            Assert.IsTrue(success);
            Assert.AreEqual(1, _report.Events.Count);
            Assert.IsTrue(_report.Events.Contains(ALICE_GREETING));
        }

        [Test]
        public void Test_EventAddition_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _report.Add(null)
            );
        }
        [Test]
        public void Test_EventAddition_WithIdleMove()
        {
            var @event = new DialogMoveRealizationEvent(ALICE, IdleMove.Instance);
            bool success = _report.Add(@event);

            Assert.IsFalse(success);
        }
        [Test]
        public void Test_EventAddition_OfExistingEvent()
        {
            _report.Add(ALICE_GREETING);
            bool success = _report.Add(ALICE_GREETING);

            Assert.IsFalse(success);
            Assert.AreEqual(1, _report.Events.Count);
        }
        [Test]
        public void Test_EventAddition()
        {
            bool success = _report.Add(ALICE_GREETING);

            Assert.IsTrue(success);
            Assert.AreEqual(1, _report.Events.Count);
            Assert.IsTrue(_report.Events.Contains(ALICE_GREETING));
        }
    }
}
