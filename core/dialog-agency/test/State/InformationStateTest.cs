﻿using NUnit.Framework;
using System;

namespace Dialog.Agency.State.Test
{
    [TestFixture]
    public sealed class InformationStateTest
    {
        private static object VALUE = new object();

        private InformationState _state;

        [SetUp]
        public void Setup()
        {
            _state = new InformationState.Builder()
                .Support(VALUE)
                .Build();
        }

        [Test]
        public void Test_DataPresence()
        {
            Assert.IsTrue(_state.SupportedComponents.Contains(typeof(object)));
            Assert.IsFalse(_state.SupportedComponents.Contains(typeof(int)));
        }

        [Test]
        public void Test_DataAccess_WithInvalidArgs()
        {
            Assert.Throws<ArgumentException>
            (
                () => _state.Get<int>()
            );
        }
        [Test]
        public void Test_DataAccess()
        {
            var value = _state.Get<object>();

            Assert.AreEqual(VALUE, value);
        }

        [Test]
        public void Test_DataMutation_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _state.Set<object>(null)
            );
        }
        [Test]
        public void Test_DataMutation()
        {
            var new_value = new object();
            _state.Set(new_value);

            Assert.AreEqual(new_value, _state.Get<object>());
        }
    }
}
