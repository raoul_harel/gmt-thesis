﻿using NUnit.Framework;
using System;

namespace Dialog.Agency.State.Test
{
    [TestFixture]
    public sealed class StateComponentTest
    {
        private static object VALUE = new object();

        private StateComponent<object> _component;

        [SetUp]
        public void Setup()
        {
            _component = new StateComponent<object>(VALUE);
        }

        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new StateComponent<object>(null)
            );
        }
        [Test]
        public void Test_Consructor()
        {
            Assert.AreEqual(typeof(object), _component.DataType);
            Assert.AreSame(VALUE, _component.Value);
        }

        [Test]
        public void Test_Equality()
        {
            var original = _component;
            var good_copy = new StateComponent<object>(VALUE);
            var flawed_value_copy = new StateComponent<object>(new object());

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_value_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
