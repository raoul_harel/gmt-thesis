﻿using Common.Validation;

namespace Dialog.Agency.Dialog_Moves
{
    /// <summary>
    /// This class represents an extendable enumeration of dialog move kinds.
    /// </summary>
    public class DialogMoveKind
    {
        /// <summary>
        /// The idle move, indicating inaction.
        /// </summary>
        public static readonly DialogMoveKind Idle = 
            new DialogMoveKind("idle");

        /// <summary>
        /// Creates a new dialog move kind.
        /// </summary>
        /// <param name="label">The label associated with this kind.</param>
        /// <remarks>
        /// <paramref name="label"/> must not be blank.
        /// </remarks>
        protected DialogMoveKind(string label)
        {
            Require.IsNotBlank(label);

            Label = label;
        }

        /// <summary>
        /// Gets the item's label.
        /// </summary>
        public string Label { get; private set; }
        
        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(DialogMoveKind)}(" +
                   $"{nameof(Label)} = '{Label}')";
        }
    }
}
