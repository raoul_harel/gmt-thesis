﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Functional.Options;
using Common.Hashing;
using Common.Validation;
using Dialog.Agency.Scene;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dialog.Agency.Dialog_Moves
{
    /// <summary>
    /// Dialog moves are a way to refer to the act of communicating meanings 
    /// through behavior, independent of the concrete form(s) such behavior may 
    /// take. To illustrate: both a hand-wave as well as a verbal "hello" are 
    /// concrete forms of the same move: a greeting. 
    /// </summary>
    public interface DialogMove
    {
        /// <summary>
        /// Gets the kind associated with this move.
        /// </summary>
        DialogMoveKind Kind { get; }
        /// <summary>
        /// Gets the targeted actors.
        /// </summary>
        ImmutableCollection<Actor> Targets { get; }
    }
    /// <summary>
    /// An abstract base class implementing the <see cref="DialogMove"/>
    /// interface.
    /// </summary>
    /// <typeparam name="TActor">The type of the actors involved.</typeparam>
    public class DialogMove<TActor>
        : DialogMove
        where TActor : Actor
    {
        /// <summary>
        /// Creates a new dialog move.
        /// </summary>
        /// <param name="kind">The move's kind designation.</param>
        /// <param name="targets">The move's targets.</param>
        /// <remarks>
        /// <paramref name="kind"/> must not be null.
        /// <paramref name="targets"/> must not be null.
        /// <paramref name="targets"/> must not contain null.
        /// </remarks>
        public DialogMove
        (
            DialogMoveKind kind, 
            IEnumerable<TActor> targets
        )
        {
            Require.IsNotNull(kind);
            Require.IsNotNull(targets);
            Require.IsFalse<ArgumentNullException>
            (
                targets.Any(actor => actor == null)
            );

            Kind = kind;

            _target_set.UnionWith(targets.Cast<Actor>());
            _target_collection = new CollectionView<Actor>(_target_set);

            TargetEnumeration = _target_set.Cast<TActor>();
            if (_target_collection.Count == 1)
            {
                Target = Option.CreateSome(TargetEnumeration.First());
            }
        }
        /// <summary>
        /// Creates a new single-target dialog move.
        /// </summary>
        /// <param name="kind">The move's kind designation.</param>
        /// <param name="target">The move's target.</param>
        /// <remarks>
        /// <paramref name="kind"/> must not be null.
        /// <paramref name="target"/> must not be null.
        /// </remarks>
        public DialogMove(DialogMoveKind kind, TActor target)
            : this(kind, new TActor[] { target })
        { }
        /// <summary>
        /// Creates a new zero-target dialog move.
        /// </summary>
        /// <param name="kind">The move's kind designation.</param>
        /// <remarks>
        /// <paramref name="kind"/> must not be null.
        /// </remarks>
        public DialogMove(DialogMoveKind kind)
            : this(kind, new TActor[0])
        { }

        /// <summary>
        /// Gets the kind associated with this move.
        /// </summary>
        public DialogMoveKind Kind { get; private set; }

        /// <summary>
        /// Gets the targeted actors.
        /// </summary>
        public ImmutableCollection<Actor> Targets
        {
            get { return _target_collection; }
        }
        /// <summary>
        /// Gets the targeted actors.
        /// </summary>
        public IEnumerable<TActor> TargetEnumeration { get; private set; }                                  

        /// <summary>
        /// Gets the targeted actor (if this move has a single target).
        /// </summary>
        public Option<TActor> Target { get; private set; } = 
            Option.CreateNone<TActor>();

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as DialogMove;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.Kind.Equals(Kind) &&
                   other.Targets.Count == Targets.Count &&
                   other.Targets.All(target => Targets.Contains(target));
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, Kind);
            hash = HashCombiner.Hash(hash, TargetEnumeration);
            
            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(DialogMove<TActor>)}(" +
                   $"{nameof(Kind)} = {Kind}, " +
                   $"{nameof(Targets)} = {Targets})";
        }

        private readonly HashSet<Actor> _target_set = new HashSet<Actor>();
        private readonly ImmutableCollection<Actor> _target_collection;
    }
}
