﻿using Common.Validation;
using Dialog.Agency.Scene;
using System.Linq;

namespace Dialog.Agency.Dialog_Moves.Extensions
{
    /// <summary>
    /// A collection of extension methods for dialog moves.
    /// </summary>
    public static class DialogMoveExtensions
    {
        /// <summary>
        /// Determines whether the specified move is the idle move.
        /// </summary>
        /// <param name="move">The move to test.</param>
        /// <returns>
        /// True if the specified move is the idle move, otherwise false.
        /// </returns>
        /// <remarks>
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public static bool IsIdle(this DialogMove move)
        {
            Require.IsNotNull(move);

            return move.Kind == DialogMoveKind.Idle;
        }

        /// <summary>
        /// Casts a dialog move to one using the specified actor type.
        /// </summary>
        /// <typeparam name="TResult">The actor type to cast to.</typeparam>
        /// <param name="move">The move to cast.</param>
        /// <returns>A new move with the cast actor of the original.</returns>
        /// <remarks>
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public static DialogMove<TResult> Cast<TResult>(this DialogMove move)
            where TResult : Actor
        {
            Require.IsNotNull(move);

            return new DialogMove<TResult>
            (
                move.Kind,
                move.Targets.Cast<TResult>()
            );
        }
    }
}
