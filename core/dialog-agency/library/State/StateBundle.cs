﻿using Common.Validation;
using Dialog.Agency.Modules;
using Dialog.Agency.Modules.Action;
using Dialog.Agency.Modules.Deliberation;
using Dialog.Agency.Modules.Perception;
using System.Collections;
using System.Collections.Generic;

namespace Dialog.Agency.State
{
    /// <summary>
    /// This structure represents a bundle of state objects, one for each 
    /// module of the agency system.
    /// </summary>
    /// <typeparam name="T_RAP_State">
    /// The type of the recent activity perception module's state.
    /// </typeparam>
    /// <typeparam name="T_CAP_State">
    /// The type of the current activity perception module's state.
    /// </typeparam>
    /// <typeparam name="T_SU_State">
    /// The type of the state update module's state.
    /// </typeparam>
    /// <typeparam name="T_AS_State">
    /// The type of the action selection module's state.
    /// </typeparam>
    /// <typeparam name="T_AT_State">
    /// The type of the action timing module's state.
    /// </typeparam>
    /// <typeparam name="T_AR_State">
    /// The type of the action realization module's state.
    /// </typeparam>
    public sealed class StateBundle
        <
            T_RAP_State,
            T_CAP_State,
            T_SU_State,
            T_AS_State,
            T_AT_State,
            T_AR_State
        >
    {
        /// <summary>
        /// Creates a new bundle from the specified objects.
        /// </summary>
        /// <param name="RAP_state">
        /// The recent activity perception module's state object.
        /// </param>
        /// <param name="CAP_state">
        /// The current activity perception module's state object.
        /// </param>
        /// <param name="SU_state">
        /// The information state upadte module's state object.
        /// </param>
        /// <param name="AS_state">
        /// The action_selection module's state object.
        /// </param>
        /// <param name="AT_state">
        /// The action timing module's state object.
        /// </param>
        /// <param name="AR_state">
        /// The action realization module's state object.
        /// </param>
        /// <remarks>
        /// <paramref name="RAP_state"/> must not be null.
        /// <paramref name="CAP_state"/> must not be null.
        /// <paramref name="SU_state"/> must not be null.
        /// <paramref name="AS_state"/> must not be null.
        /// <paramref name="AT_state"/> must not be null.
        /// <paramref name="AR_state"/> must not be null.
        /// </remarks>
        private ModuleBundle
        (
            RecentActivityPerceptionModule RAP_state,
            CurrentActivityPerceptionModule CAP_state,
            StateUpdateModule SU_state,
            ActionSelectionModule AS_state,
            ActionTimingModule AT_state,
            ActionRealizationModule AR_state
        )
        {
            Require.IsNotNull(RAP_state);
            Require.IsNotNull(CAP_state);
            Require.IsNotNull(SU_state);
            Require.IsNotNull(AS_state);
            Require.IsNotNull(AT_state);
            Require.IsNotNull(AR_state);

            RecentActivityPerception = RAP_state;
            CurrentActivityPerception = CAP_state;
            StateUpdate = SU_state;
            ActionSelection = AS_state;
            ActionTiming = AT_state;
            ActionRealization = AR_state;
        }

        /// <summary>
        /// Gets the recent activity perception module.
        /// </summary>
        public RecentActivityPerceptionModule RecentActivityPerception
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the current activity perception module.
        /// </summary>
        public CurrentActivityPerceptionModule CurrentActivityPerception
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the state update module.
        /// </summary>
        public StateUpdateModule StateUpdate
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the action selection module.
        /// </summary>
        public ActionSelectionModule ActionSelection
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the action timing module.
        /// </summary>
        public ActionTimingModule ActionTiming
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the action realization module.
        /// </summary>
        public ActionRealizationModule ActionRealization
        {
            get;
            private set;
        }

        /// <summary>
        /// Enumerates the modules contained in this bundle.
        /// </summary>
        /// <returns>An enumerator of operation modules.</returns>
        public IEnumerator<OperationModule> GetEnumerator()
        {
            yield return RecentActivityPerception;
            yield return CurrentActivityPerception;
            yield return StateUpdate;
            yield return ActionSelection;
            yield return ActionTiming;
            yield return ActionRealization;
        }
        /// <summary>
        /// Enumerates the modules contained in this bundle.
        /// </summary>
        /// <returns>An enumerator of operation modules.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ModuleBundle)}(" +

                   $"{nameof(RecentActivityPerception)} = " +
                   $"{RecentActivityPerception}, " +

                   $"{nameof(CurrentActivityPerception)} = " +
                   $"{CurrentActivityPerception}, " +

                   $"{nameof(StateUpdate)} = {StateUpdate}, " +
                   $"{nameof(ActionSelection)} = {ActionSelection}, " +
                   $"{nameof(ActionTiming)} = {ActionTiming}, " +
                   $"{nameof(ActionRealization)} = {ActionRealization})";
        }
    }
}
