﻿using Common.Hashing;
using Common.Validation;
using System;

namespace Dialog.Agency.State
{
    /// <summary>
    /// This interface represents a container for data.
    /// </summary>
    public interface StateComponent
    {
        /// <summary>
        /// Gets the type of data being held.
        /// </summary>
        Type DataType { get; }
    }
    /// <summary>
    /// A generic implementation of the <see cref="StateComponent"/> interface. 
    /// </summary>
    /// <typeparam name="T">The type of data the component holds.</typeparam>
    public sealed class StateComponent<T>
        : StateComponent
    {
        /// <summary>
        /// Creates a new component holding the specified data.
        /// </summary>
        /// <param name="value">The data to hold.</param>
        /// <remarks>
        /// <paramref name="value"/> must not be null.
        /// </remarks>
        public StateComponent(T value)
        {
            Require.IsNotNull(value);

            DataType = typeof(T);
            Value = value;
        }

        /// <summary>
        /// Gets the type of data being held.
        /// </summary>
        public Type DataType { get; private set; }

        /// <summary>
        /// Gets the data being held.
        /// </summary>
        public T Value { get; private set; }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as StateComponent<T>;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.Value.Equals(Value);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, DataType);
            hash = HashCombiner.Hash(hash, Value);

            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(StateComponent<T>)}(" +
                   $"{nameof(DataType)} = {DataType}, " +
                   $"{nameof(Value)} = {Value})";
        }
    }
}
