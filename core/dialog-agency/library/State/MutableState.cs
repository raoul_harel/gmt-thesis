﻿namespace Dialog.Agency.State
{
    /// <summary>
    /// This interface represents a mutable information state.
    /// </summary>
    public interface MutableState
        : ImmutableState
    {
        /// <summary>
        /// Sets the component corresponding to the specified type.
        /// </summary>
        /// <typeparam name="T">The type of component to set.</typeparam>
        /// <param name="value">The value to assign.</param>
        void Set<T>(T value);
    }
}
