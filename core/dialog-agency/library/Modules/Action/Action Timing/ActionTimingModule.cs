﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.State;

namespace Dialog.Agency.Modules.Action
{
    /// <summary>
    /// This module is responsible for timing the realization of actions.
    /// </summary>
    public abstract class ActionTimingModule
        : OperationModule<ImmutableState>
    {
        /// <summary>
        /// Determines whether now is a valid time to realize the specified 
        /// dialog move.
        /// </summary>
        /// <param name="move">The desired dialog move to make.</param>
        /// <returns>
        /// True if now is a valid time to realize the specified dialog move,
        /// otherwise false.
        /// </returns>
        public abstract bool IsValidMoveNow(DialogMove move);

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ActionTimingModule)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
