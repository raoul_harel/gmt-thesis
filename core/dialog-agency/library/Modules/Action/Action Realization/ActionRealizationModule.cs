﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.State;

namespace Dialog.Agency.Modules.Action
{
    /// <summary>
    /// This module is responsible for realizing a specified action.
    /// </summary>
    public abstract class ActionRealizationModule
        : OperationModule<ImmutableState>
    {
        /// <summary>
        /// Realizes the specified dialog move.
        /// </summary>
        /// <param name="move">The dialog move to realize.</param>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        public abstract ActionRealizationStatus RealizeMove(DialogMove move);

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ActionRealizationModule)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
