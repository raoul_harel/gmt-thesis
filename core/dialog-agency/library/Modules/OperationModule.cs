﻿using Common.Validation;
using Dialog.Agency.State;
using System;
using System.Collections.Generic;

namespace Dialog.Agency.Modules
{
    /// <summary>
    /// Modules are a way to isolate implementations of operations used in the 
    /// agency process from each other. Each module is responsible for the 
    /// execution of a single operation, with well-defined input and output.
    /// </summary>
    public interface OperationModule
    {
        /// <summary>
        /// Indicates whether this module is initialized.
        /// </summary>
        bool IsInitialized { get; }
    }
    /// <summary>
    /// This class serves as a base for all operation modules.
    /// </summary>
    /// <typeparam name="TState">
    /// The type of the information state used by the module.
    /// </typeparam>
    public abstract class OperationModule<TState>
        : OperationModule
        where TState : ImmutableState
    {
        /// <summary>
        /// Indicates whether this module is initialized.
        /// </summary>
        public bool IsInitialized { get; private set; } = false;

        /// <summary>
        /// Gets the information state referenced by this module.
        /// </summary>
        public TState State { get; private set; }

        /// <summary>
        /// Initializes this module to reference the specified state.
        /// </summary>
        /// <param name="state">The state to reference.</param>
        /// <remarks>
        /// Initializing an already initialized module is invalid.
        /// <paramref name="state"/> must not be null.
        /// </remarks>
        public void Initialize(TState state)
        {
            Require.IsFalse<InvalidOperationException>(IsInitialized);
            Require.IsNotNull(state);

            var required_state_components = new HashSet<Type>();
            GetRequiredStateComponents(required_state_components);

            foreach (var type in required_state_components)
            {
                Require.IsTrue(state.SupportedComponents.Contains(type));
            }

            State = state;
            IsInitialized = true;

            Setup();
        }

        /// <summary>
        /// Gets the collection of data types required by this module to be 
        /// available through the information state.
        /// </summary>
        /// <param name="types">The collection of types.</param>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public virtual void GetRequiredStateComponents(ICollection<Type> types)
        { }
        /// <summary>
        /// Sets up this module for operation.
        /// </summary>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public virtual void Setup() { }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(OperationModule<TState>)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
