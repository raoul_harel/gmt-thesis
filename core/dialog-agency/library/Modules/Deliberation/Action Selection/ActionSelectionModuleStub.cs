﻿using Dialog.Agency.Dialog_Moves;

namespace Dialog.Agency.Modules.Deliberation
{
    /// <summary>
    /// This class is a stub implementation of the 
    /// <see cref="ActionSelectionModule"/>.
    /// </summary>
    public sealed class ActionSelectionModuleStub
        : ActionSelectionModule
    {
        /// <summary>
        /// Selects a target dialog move for the system to perform.
        /// </summary>
        /// <returns>The idle move.</returns>
        public override DialogMove SelectMove()
        {
            return IdleMove.Instance;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ActionSelectionModuleStub)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
