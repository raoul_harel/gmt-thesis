﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.State;

namespace Dialog.Agency.Modules.Deliberation
{
    /// <summary>
    /// This module is responsible for selecting the system's target dialog 
    /// move to realize.
    /// </summary>
    public abstract class ActionSelectionModule
        : OperationModule<ImmutableState>
    {
        /// <summary>
        /// Selects a target dialog move for the system to perform.
        /// </summary>
        /// <returns>The selected target dialog move.</returns>
        public abstract DialogMove SelectMove();

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ActionSelectionModule)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
