﻿using Dialog.Agency.State;

namespace Dialog.Agency.Modules.Perception
{
    /// <summary>
    /// This module is responsible for perceiving the current activity status
    /// of actors in the scene.
    /// </summary>
    public abstract class CurrentActivityPerceptionModule
        : OperationModule<ImmutableState>
    {
        /// <summary>
        /// Perceives current actor activity and compiles it into a 
        /// <see cref="CurrentActivityReport"/>.
        /// </summary>
        public CurrentActivityReport PerceiveActivity()
        {
            var output = new CurrentActivityReport();
            ComposeActivityReport(output);

            return output;
        }

        /// <summary>
        /// Fills in an activity report.
        /// </summary>
        /// <param name="report">The report to fill in.</param>
        protected abstract void ComposeActivityReport
        (
            CurrentActivityReport report
        );

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(CurrentActivityPerceptionModule)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
