﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Scene;
using System.Collections.Generic;

namespace Dialog.Agency.Modules.Perception
{
    /// <summary>
    /// This class is the medium through which the 
    /// <see cref="RecentActivityPerceptionModule"/> reports its output.
    /// </summary>
    public sealed class RecentActivityReport
    {
        /// <summary>
        /// Creates a new report.
        /// </summary>
        public RecentActivityReport()
        {
            Events = new CollectionView<DialogMoveRealizationEvent>
            (
                _perceived_events
            );
        }

        /// <summary>
        /// Gets the collection of reported events.
        /// </summary>
        public ImmutableCollection<DialogMoveRealizationEvent> Events
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates a <see cref="DialogMoveRealizationEvent"/> from the 
        /// specified actor-move pair and adds it to this report.
        /// </summary>
        /// <param name="source">The actor who produced the move.</param>
        /// <param name="move">The move that was made.</param>
        /// <returns>
        /// True if the event was added successfully, otherwise false.
        /// </returns>
        /// <remarks>
        /// <paramref name="source"/> must not be null.
        /// <paramref name="move"/> must not be null.
        /// An addition is unsuccessful when either the event has already been
        /// submitted before, or if it contains the idle move.
        /// </remarks>
        public bool Add(Actor source, DialogMove move)
        {
            Require.IsNotNull(source);
            Require.IsNotNull(move);

            return Add(new DialogMoveRealizationEvent(source, move));
        }
        /// <summary>
        /// Adds the specified event to this report.
        /// </summary>
        /// <param name="event">The event to add.</param>
        /// <returns>
        /// True if the event was added successfully, otherwise false.
        /// </returns>
        /// <remarks>
        /// <paramref name="event"/> must not be null.
        /// An addition is unsuccessful when either the event has already been
        /// submitted before, or if it contains the idle move.
        /// </remarks>
        public bool Add(DialogMoveRealizationEvent @event)
        {
            Require.IsNotNull(@event);

            return @event.Move != IdleMove.Instance &&
                   _perceived_events.Add(@event);
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(RecentActivityReport)}(" +
                   $"{nameof(Events)} = {Events})";
        }

        private readonly HashSet<DialogMoveRealizationEvent> 
            _perceived_events = new HashSet<DialogMoveRealizationEvent>();
    }
}
