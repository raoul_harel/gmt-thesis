﻿using Common.Hashing;
using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Dialog_Moves.Extensions;

namespace Dialog.Agency.System
{
    /// <summary>
    /// This class represents a snapshot of the system's activity.
    /// </summary>
    public sealed class SystemActivitySnapshot
    {
        /// <summary>
        /// Creates a new snapshot.
        /// </summary>
        /// <param name="recent_move">
        /// The latest dialog move realized by the system.
        /// </param>
        /// <param name="target_move">
        /// The system's target dialog move.
        /// </param>
        /// <param name="actual_move">
        /// The system's actual dialog move that is being realized 
        /// currently.
        /// </param>
        /// <remarks>
        /// <paramref name="recent_move"/> must not be null.
        /// <paramref name="target_move"/> must not be null.
        /// <paramref name="actual_move"/> must not be null.
        /// </remarks>
        public SystemActivitySnapshot
        (
            DialogMove recent_move,
            DialogMove target_move,
            DialogMove actual_move
        )
        {
            Require.IsNotNull(recent_move);
            Require.IsNotNull(target_move);
            Require.IsNotNull(actual_move);

            RecentMove = recent_move;
            TargetMove = target_move;
            ActualMove = actual_move;
        }

        /// <summary>
        /// Gets the latest dialog move realized by the system.
        /// </summary>
        public DialogMove RecentMove { get; private set; }
        /// <summary>
        /// Gets the system's target dialog move.
        /// </summary>
        public DialogMove TargetMove { get; private set; }
        /// <summary>
        /// Gets the system's actual dialog move that is being realized 
        /// currently.
        /// </summary>
        public DialogMove ActualMove { get; private set; }

        /// <summary>
        /// Indicates whether the system is currenly idle.
        /// </summary>
        public bool IsPassive
        {
            get { return ActualMove.IsIdle(); }
        }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as SystemActivitySnapshot;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.RecentMove.Equals(RecentMove) &&
                   other.TargetMove.Equals(TargetMove) &&
                   other.ActualMove.Equals(ActualMove);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, RecentMove);
            hash = HashCombiner.Hash(hash, TargetMove);
            hash = HashCombiner.Hash(hash, ActualMove);

            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(SystemActivitySnapshot)}(" +
                   $"{nameof(RecentMove)} = {RecentMove}, " +
                   $"{nameof(TargetMove)} = {TargetMove}, " +
                   $"{nameof(ActualMove)} = {ActualMove})";
        }
    }
}
