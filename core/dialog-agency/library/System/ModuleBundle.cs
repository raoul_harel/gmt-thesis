﻿using Common.DesignPatterns;
using Common.Validation;
using Dialog.Agency.Modules;
using Dialog.Agency.Modules.Action;
using Dialog.Agency.Modules.Perception;
using Dialog.Agency.Modules.Deliberation;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Dialog.Agency.System
{
    /// <summary>
    /// This structure represents a bundle of modules that may be used together
    /// to form a complete agency system.
    /// </summary>
    public sealed class ModuleBundle
        : IEnumerable<OperationModule>
    {
        /// <summary>
        /// This class is used to build instances of 
        /// <see cref="ModuleBundle"/>.
        /// </summary>
        public sealed class Builder
            : ObjectBuilder<ModuleBundle>
        {
            /// <summary>
            /// Assigns the specified module to be the one responsible for 
            /// recent activity perception.
            /// </summary>
            /// <param name="module">The module to assign.</param>
            /// <returns>This instance (for method call-chaining).</returns>
            /// <remarks>
            /// Calling this on a built object is invalid.
            /// <paramref name="module"/> must not be null.
            /// <paramref name="module"/> must not be initialized already.
            /// </remarks>
            public Builder WithRecentActivityPerceptionBy
            (
                RecentActivityPerceptionModule module
            )
            {
                Require.IsFalse<InvalidOperationException>(IsBuilt);
                Require.IsNotNull(module);
                Require.IsFalse(module.IsInitialized);

                _RAP_module = module;

                return this;
            }
            /// <summary>
            /// Assigns the specified module to be the one responsible for 
            /// current activity perception.
            /// </summary>
            /// <param name="module">The module to assign.</param>
            /// <returns>This instance (for method call-chaining).</returns>
            /// <remarks>
            /// Calling this on a built object is invalid.
            /// <paramref name="module"/> must not be null.
            /// <paramref name="module"/> must not be initialized already.
            /// </remarks>
            public Builder WithCurrentActivityPerceptionBy
            (
                CurrentActivityPerceptionModule module
            )
            {
                Require.IsFalse<InvalidOperationException>(IsBuilt);
                Require.IsNotNull(module);
                Require.IsFalse(module.IsInitialized);

                _CAP_module = module;

                return this;
            }
            /// <summary>
            /// Assigns the specified module to be the one responsible for 
            /// infomation state update.
            /// </summary>
            /// <param name="module">The module to assign.</param>
            /// <returns>This instance (for method call-chaining).</returns>
            /// <remarks>
            /// Calling this on a built object is invalid.
            /// <paramref name="module"/> must not be null.
            /// <paramref name="module"/> must not be initialized already.
            /// </remarks>
            public Builder WithStateUpdateBy(StateUpdateModule module)
            {
                Require.IsFalse<InvalidOperationException>(IsBuilt);
                Require.IsNotNull(module);
                Require.IsFalse(module.IsInitialized);

                _state_update_module = module;

                return this;
            }
            /// <summary>
            /// Assigns the specified module to be the one responsible for 
            /// action selection.
            /// </summary>
            /// <param name="module">The module to assign.</param>
            /// <returns>This instance (for method call-chaining).</returns>
            /// <remarks>
            /// Calling this on a built object is invalid.
            /// <paramref name="module"/> must not be null.
            /// <paramref name="module"/> must not be initialized already.
            /// </remarks>
            public Builder WithActionSelectionBy(ActionSelectionModule module)
            {
                Require.IsFalse<InvalidOperationException>(IsBuilt);
                Require.IsNotNull(module);
                Require.IsFalse(module.IsInitialized);

                _action_selection_module = module;

                return this;
            }
            /// <summary>
            /// Assigns the specified module to be the one responsible for 
            /// action timing.
            /// </summary>
            /// <param name="module">The module to assign.</param>
            /// <returns>This instance (for method call-chaining).</returns>
            /// <remarks>
            /// Calling this on a built object is invalid.
            /// <paramref name="module"/> must not be null.
            /// <paramref name="module"/> must not be initialized already.
            /// </remarks>
            public Builder WithActionTimingBy(ActionTimingModule module)
            {
                Require.IsFalse<InvalidOperationException>(IsBuilt);
                Require.IsNotNull(module);
                Require.IsFalse(module.IsInitialized);

                _action_timing_module = module;

                return this;
            }
            /// <summary>
            /// Assigns the specified module to be the one responsible for 
            /// action realization.
            /// </summary>
            /// <param name="module">The module to assign.</param>
            /// <returns>This instance (for method call-chaining).</returns>
            /// <remarks>
            /// Calling this on a built object is invalid.
            /// <paramref name="module"/> must not be null.
            /// <paramref name="module"/> must not be initialized already.
            /// </remarks>
            public Builder WithActionRealizationBy
            (
                ActionRealizationModule module
            )
            {
                Require.IsFalse<InvalidOperationException>(IsBuilt);
                Require.IsNotNull(module);
                Require.IsFalse(module.IsInitialized);

                _action_realization_module = module;

                return this;
            }

            /// <summary>
            /// Creates the object.
            /// </summary>
            /// <returns>The built object.</returns>
            protected override ModuleBundle CreateObject()
            {
                return new ModuleBundle
                (
                    _RAP_module,
                    _CAP_module,
                    _state_update_module,
                    _action_selection_module,
                    _action_timing_module,
                    _action_realization_module
                );
            }

            private RecentActivityPerceptionModule 
                _RAP_module = new RecentActivityPerceptionModuleStub();
            private CurrentActivityPerceptionModule
                _CAP_module = new CurrentActivityPerceptionModuleStub();
            private StateUpdateModule
                _state_update_module = new StateUpdateModuleStub();
            private ActionSelectionModule
                _action_selection_module = new ActionSelectionModuleStub();
            private ActionTimingModule
                _action_timing_module = new ActionTimingModuleStub();
            private ActionRealizationModule
                _action_realization_module = new ActionRealizationModuleStub();
        }

        /// <summary>
        /// Creates a new bundle from the specified modules.
        /// </summary>
        /// <param name="recent_activity_perception">
        /// The recent activity perception module.
        /// </param>
        /// <param name="current_activity_perception">
        /// The current activity perception module.
        /// </param>
        /// <param name="state_update">
        /// The information state upadte module.
        /// </param>
        /// <param name="action_selection">
        /// The action_selection module.
        /// </param>
        /// <param name="action_timing">
        /// The action timing module.
        /// </param>
        /// <param name="action_realization">
        /// The action realization module.
        /// </param>
        /// <remarks>
        /// <paramref name="recent_activity_perception"/> must not be null.
        /// <paramref name="current_activity_perception"/> must not be null.
        /// <paramref name="state_update"/> must not be null.
        /// <paramref name="action_selection"/> must not be null.
        /// <paramref name="action_timing"/> must not be null.
        /// <paramref name="action_realization"/> must not be null.
        /// </remarks>
        private ModuleBundle
        (
            RecentActivityPerceptionModule recent_activity_perception,
            CurrentActivityPerceptionModule current_activity_perception,
            StateUpdateModule state_update,
            ActionSelectionModule action_selection,
            ActionTimingModule action_timing,
            ActionRealizationModule action_realization
        )
        {
            Require.IsNotNull(recent_activity_perception);
            Require.IsNotNull(current_activity_perception);
            Require.IsNotNull(state_update);
            Require.IsNotNull(action_selection);
            Require.IsNotNull(action_timing);
            Require.IsNotNull(action_realization);

            RecentActivityPerception = recent_activity_perception;
            CurrentActivityPerception = current_activity_perception;
            StateUpdate = state_update;
            ActionSelection = action_selection;
            ActionTiming = action_timing;
            ActionRealization = action_realization;
        }

        /// <summary>
        /// Gets the recent activity perception module.
        /// </summary>
        public RecentActivityPerceptionModule RecentActivityPerception
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the current activity perception module.
        /// </summary>
        public CurrentActivityPerceptionModule CurrentActivityPerception
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the state update module.
        /// </summary>
        public StateUpdateModule StateUpdate
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the action selection module.
        /// </summary>
        public ActionSelectionModule ActionSelection
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the action timing module.
        /// </summary>
        public ActionTimingModule ActionTiming
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the action realization module.
        /// </summary>
        public ActionRealizationModule ActionRealization
        {
            get;
            private set;
        }

        /// <summary>
        /// Enumerates the modules contained in this bundle.
        /// </summary>
        /// <returns>An enumerator of operation modules.</returns>
        public IEnumerator<OperationModule> GetEnumerator()
        {
            yield return RecentActivityPerception;
            yield return CurrentActivityPerception;
            yield return StateUpdate;
            yield return ActionSelection;
            yield return ActionTiming;
            yield return ActionRealization;
        }
        /// <summary>
        /// Enumerates the modules contained in this bundle.
        /// </summary>
        /// <returns>An enumerator of operation modules.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ModuleBundle)}(" +

                   $"{nameof(RecentActivityPerception)} = " +
                   $"{RecentActivityPerception}, " +

                   $"{nameof(CurrentActivityPerception)} = " +
                   $"{CurrentActivityPerception}, " +

                   $"{nameof(StateUpdate)} = {StateUpdate}, " +
                   $"{nameof(ActionSelection)} = {ActionSelection}, " +
                   $"{nameof(ActionTiming)} = {ActionTiming}, " +
                   $"{nameof(ActionRealization)} = {ActionRealization})";
        }
    }
}
