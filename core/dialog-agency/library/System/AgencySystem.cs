﻿using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Modules.Action;
using Dialog.Agency.Modules.Perception;
using Dialog.Agency.State;
using System;

namespace Dialog.Agency.System
{
    /// <summary>
    /// This class represents the agency process of one actor, as it is  
    /// expressed through a set of user-supplied operation modules and an 
    /// information state object.
    /// </summary>
    public sealed class AgencySystem
    {
        /// <summary>
        /// Creates a new process.
        /// </summary>
        /// <param name="state">The default information state.</param>
        /// <param name="modules">The operation modules to use.</param>
        /// <remarks>
        /// <paramref name="state"/> must not be null.
        /// <paramref name="modules"/> must not be null.
        /// </remarks>
        public AgencySystem(InformationState state, ModuleBundle modules)
        {
            Require.IsNotNull(state);
            Require.IsNotNull(modules);

            State = state;
            Modules = modules;

            Modules.RecentActivityPerception.Initialize(State);
            Modules.CurrentActivityPerception.Initialize(State);
            Modules.StateUpdate.Initialize(State);
            Modules.ActionSelection.Initialize(State);
            Modules.ActionTiming.Initialize(State);
            Modules.ActionRealization.Initialize(State);
        }

        /// <summary>
        /// Gets the common information state object all operation modules 
        /// of this process reference.
        /// </summary>
        public InformationState State { get; private set; }

        /// <summary>
        /// Gets the operation modules that make up this process.
        /// </summary>
        public ModuleBundle Modules { get; private set; }

        /// <summary>
        /// Gets the latest dialog move realized by the system.
        /// </summary>
        public DialogMove RecentMove { get; private set; } = IdleMove.Instance;
        /// <summary>
        /// Gets the system's target dialog move.
        /// </summary>
        public DialogMove TargetMove { get; private set; } = IdleMove.Instance;
        /// <summary>
        /// Gets the system's actual dialog move that is being realized 
        /// currently.
        /// </summary>
        public DialogMove ActualMove { get; private set; } = IdleMove.Instance;

        /// <summary>
        /// Performs one iteration of the agency process.
        /// </summary>
        public void Step()
        {
            RecentActivityReport recent_activity = 
                Modules.RecentActivityPerception.PerceiveActivity();
            CurrentActivityReport current_activity =
                Modules.CurrentActivityPerception.PerceiveActivity();

            SystemActivitySnapshot system_activity = TakeActivitySnapshot();

            Modules.StateUpdate.PerformUpdate
            (
                recent_activity,
                current_activity,
                system_activity
            );

            TargetMove = Modules.ActionSelection.SelectMove();
            Require.IsNotNull<InvalidOperationException>(TargetMove);

            if (TargetMove != IdleMove.Instance &&
                Modules.ActionTiming.IsValidMoveNow(TargetMove))
            {
                ActualMove = TargetMove;
            }
            else
            {
                ActualMove = IdleMove.Instance;
            }

            ActionRealizationStatus realization_status =
                Modules.ActionRealization.RealizeMove(ActualMove);

            if (realization_status == ActionRealizationStatus.Complete)
            {
                RecentMove = ActualMove;
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(AgencySystem)}(" +
                   $"{nameof(State)} = {State}, " +
                   $"{nameof(Modules)} = {Modules})";
        }

        /// <summary>
        /// Takes an activity snapshot of the system at its current state.
        /// </summary>
        /// <returns>An activity snapshot.</returns>
        private SystemActivitySnapshot TakeActivitySnapshot()
        {
            return new SystemActivitySnapshot
            (
                RecentMove, 
                TargetMove, 
                ActualMove
            );
        }
    }
}
