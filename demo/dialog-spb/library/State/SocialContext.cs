﻿using Common.Hashing;
using Common.Validation;
using Dialog.Agency.Scene;
using Dialog.SPB.Program;

namespace Dialog.SPB.State
{
    /// <summary>
    /// This class represents the information state required by the agency 
    /// modules defined in this project.
    /// </summary>
    public sealed class SocialContext
    {
        /// <summary>
        /// Creates a new state for the specified actor and using the specified
        /// social practice program.
        /// </summary>
        /// <param name="self">The actor representing the system.</param>
        /// <param name="program">The active social practice program.</param>
        /// <remarks>
        /// <paramref name="self"/> must not be null.
        /// <paramref name="program"/> must not be null.
        /// </remarks>
        public SocialContext(Actor self, Node program)
        {
            Require.IsNotNull(self);
            Require.IsNotNull(program);

            Self = self;
            Program = program;

            Program.Reset();
        }

        /// <summary>
        /// Gets the actor representing the system.
        /// </summary>
        public Actor Self { get; private set; }

        /// <summary>
        /// Gets the active social practice program.
        /// </summary>
        public Node Program { get; private set; }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as SocialContext;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.Self.Equals(Self) &&
                   other.Program.Equals(Program);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, Self);
            hash = HashCombiner.Hash(hash, Program);

            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(SocialContext)}(" +
                   $"{nameof(Self)} = {Self}, " +
                   $"{nameof(Program)} = {Program})";
        }
    }
}
