﻿namespace Dialog.SPB.Program
{
    /// <summary>
    /// Enumerates possible states of an expectation.
    /// </summary>
    public enum ResolutionStatus : int
    {
        /// <summary>
        /// The expectation has not been satisfied yet, but could be in the 
        /// future.
        /// </summary>
        Pending,
        /// <summary>
        /// The expectation has not been satisfied, and there is no possibility
        /// for satisfaction in the future.
        /// </summary>
        Failure,
        /// <summary>
        /// The expectation has been satisfied.
        /// </summary>
        Satisfaction
    }
}
