﻿using Common.Functional.Delegates;
using Common.Functional.Options;
using Common.Validation;
using System.Collections.Generic;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Dialog.SPB.Program
{
    /// <summary>
    /// This class represents an internal node expecting to repeatedly satisfy
    /// an expectation.
    /// </summary>
    public sealed class Repeat
        : Node
    {
        /// <summary>
        /// Creates a new node with the specified title, and body.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="body">The node's body.</param>
        /// <remarks>
        /// <paramref name="title"/> must not be blank.
        /// <paramref name="body"/> must not be null.
        /// </remarks>
        public Repeat(string title, Node body)
            : base(title, new Node[1] { body })
        {
            Require.IsNotNull(body);

            Body = Children[0];

            Reset();
        }

        /// <summary>
        /// Gets this node's body.
        /// </summary>
        public Node Body { get; private set; }

        /// <summary>
        /// Gets the index of this node's active scope child (if it has any).
        /// </summary>
        public override Option<int> ScopeCarrierChildIndex
        {
            get
            {
                if (IsResolved)
                {
                    return Option.CreateNone<int>();
                }
                else
                {
                    return Option.CreateSome(0);
                }
            }
        }

        /// <summary>
        /// Called by <see cref="Node.Witness(MoveEvent)"/> to 
        /// update this node's expectations with regard to the witnessed dialog 
        /// move realization event.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// <see cref="ResolutionStatus.Satisfaction"/> if witnessing of this event 
        /// has caused all expectations of this node to be satisfied,
        /// <see cref="ResolutionStatus.Failure"/> if satisfaction of those
        /// expectation is no longer possible, or 
        /// <see cref="ResolutionStatus.Pending"/> otherwise.
        /// </returns>
        protected override ResolutionStatus OnWitness(MoveEvent @event)
        {
            ResolutionStatus body_status = Body.Witness(@event);

            if (body_status == ResolutionStatus.Failure ||
                body_status == ResolutionStatus.Pending)
            {
                return body_status;
            }
            else  // body is satisfied
            {
                Body.Reset();
                return ResolutionStatus.Pending;
            }
        }

        /// <summary>
        /// Finds and adds to the specified collection this node's expected 
        /// dialog move realization events that are indicated by the specified 
        /// function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <param name="result">The collection to add events to.</param>
        /// <remarks>
        /// <paramref name="indicator"/> must not be null.
        /// <paramref name="result"/> must not be null.
        /// </remarks>
        public override void FindExpectedEvents
        (
            Indicator<MoveEvent> indicator,
            ICollection<MoveEvent> result
        )
        {
            Require.IsFalse(IsResolved);
            Require.IsNotNull(indicator);
            Require.IsNotNull(result);

            Body.FindExpectedEvents(indicator, result);
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Repeat)}(" +
                   $"{nameof(Title)} = '{Title}', " +
                   $"{nameof(Body)} = {Body})";
        }
    }
}
