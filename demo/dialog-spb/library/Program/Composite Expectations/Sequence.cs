﻿using Common.Functional.Delegates;
using Common.Functional.Options;
using Common.Validation;
using System.Collections.Generic;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Dialog.SPB.Program
{
    /// <summary>
    /// This class represents an internal node expecting to satisfy all of its
    /// children in sequence.
    /// </summary>
    public sealed class Sequence 
        : Node
    {
        /// <summary>
        /// Creates a new node with the specified title and children.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="children">The node's children.</param>
        /// <remarks>
        /// <paramref name="title"/> must not be blank.
        /// <paramref name="children"/> must not be null.
        /// <paramref name="children"/> must contain at least two items.
        /// </remarks>
        public Sequence(string title, IEnumerable<Node> children)
            : base(title, children)
        {
            Require.IsAtLeast(Children.Count, 2);

            Reset();
        }

        /// <summary>
        /// Gets the index of this node's active scope child (if it has any).
        /// </summary>
        public override Option<int> ScopeCarrierChildIndex
        {
            get
            {
                if (IsResolved)
                {
                    return Option.CreateNone<int>();
                }
                else
                {
                    return Option.CreateSome(_active_child_index);
                }
            }
        }

        /// <summary>
        /// Called by <see cref="Node.Reset"/> to reset this node's expectations
        /// back to their initial state.
        /// </summary>
        protected override void OnReset()
        {
            _active_child_index = 0;
        }
        /// <summary>
        /// Called by <see cref="Node.Witness(MoveEvent)"/> to 
        /// update this node's expectations with regard to the witnessed dialog 
        /// move realization event.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// <see cref="ResolutionStatus.Satisfaction"/> if witnessing of this event 
        /// has caused all expectations of this node to be satisfied,
        /// <see cref="ResolutionStatus.Failure"/> if satisfaction of those
        /// expectation is no longer possible, or 
        /// <see cref="ResolutionStatus.Pending"/> otherwise.
        /// </returns>
        protected override ResolutionStatus OnWitness(MoveEvent @event)
        {
            ResolutionStatus child_status = ActiveChild.Witness(@event);

            if (child_status == ResolutionStatus.Pending ||
                child_status == ResolutionStatus.Failure)
            {
                return child_status;
            }
            else  // child is satisfied
            {
                // Child was satisfied, so proceed to next:
                ++ _active_child_index;

                // Unless there are no more children left:
                return
                    _active_child_index == Children.Count ?
                    ResolutionStatus.Satisfaction :
                    ResolutionStatus.Pending;
            }
        }

        /// <summary>
        /// Finds and adds to the specified collection this node's expected 
        /// dialog move realization events that are indicated by the specified 
        /// function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <param name="result">The collection to add events to.</param>
        /// <remarks>
        /// <paramref name="indicator"/> must not be null.
        /// <paramref name="result"/> must not be null.
        /// </remarks>
        public override void FindExpectedEvents
        (
            Indicator<MoveEvent> indicator,
            ICollection<MoveEvent> result
        )
        {
            Require.IsFalse(IsResolved);
            Require.IsNotNull(indicator);
            Require.IsNotNull(result);

            ActiveChild.FindExpectedEvents(indicator, result);
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Sequence)}(" +
                   $"{nameof(Title)} = '{Title}', " +
                   $"{nameof(ActiveChild)} = {ActiveChild}, " +
                   $"{nameof(Children)} = {Children})";
        }

        /// <summary>
        /// Gets the current child to be satisfied.
        /// </summary>
        private Node ActiveChild
        {
            get { return Children[_active_child_index]; }
        }

        private int _active_child_index;
    }
}
