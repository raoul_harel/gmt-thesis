﻿using Common.Functional.Delegates;
using Common.Validation;
using System.Collections.Generic;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Dialog.SPB.Program
{
    /// <summary>
    /// This class represents an internal node expecting to satisfy any of its
    /// children.
    /// </summary>
    public sealed class Disjunction 
        : Node
    {
        /// <summary>
        /// Creates a new node with the specified title and children.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="children">The node's children.</param>
        /// <remarks>
        /// <paramref name="title"/> must not be blank.
        /// <paramref name="children"/> must not be null.
        /// <paramref name="children"/> must contain at least two items.
        /// </remarks>
        public Disjunction(string title, IEnumerable<Node> children)
            : base(title, children)
        {
            Require.IsAtLeast(Children.Count, 2);

            Reset();
        }

        /// <summary>
        /// Called by <see cref="Node.Reset"/> to reset this node's expectations
        /// back to their initial state.
        /// </summary>
        protected override void OnReset()
        {
            _children_to_be_satisfied.Clear();
            _children_to_be_satisfied.AddRange(Children);
        }
        /// <summary>
        /// Called by <see cref="Node.Witness(MoveEvent)"/> to 
        /// update this node's expectations with regard to the witnessed dialog 
        /// move realization event.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// <see cref="ResolutionStatus.Satisfaction"/> if witnessing of this event 
        /// has caused all expectations of this node to be satisfied,
        /// <see cref="ResolutionStatus.Failure"/> if satisfaction of those
        /// expectation is no longer possible, or 
        /// <see cref="ResolutionStatus.Pending"/> otherwise.
        /// </returns>
        protected override ResolutionStatus OnWitness(MoveEvent @event)
        {
            foreach (var child in _children_to_be_satisfied)
            {
                if (child.Witness(@event) == ResolutionStatus.Satisfaction)
                {
                    return ResolutionStatus.Satisfaction;
                }
            }

            // Clear out children who failed in their expectations:
            _children_to_be_satisfied.RemoveAll(child => child.IsResolved);

            return
                _children_to_be_satisfied.Count == 0 ?
                ResolutionStatus.Failure :
                ResolutionStatus.Pending;
        }

        /// <summary>
        /// Finds and adds to the specified collection this node's expected 
        /// dialog move realization events that are indicated by the specified 
        /// function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <param name="result">The collection to add events to.</param>
        /// <remarks>
        /// <paramref name="indicator"/> must not be null.
        /// <paramref name="result"/> must not be null.
        /// </remarks>
        public override void FindExpectedEvents
        (
            Indicator<MoveEvent> indicator,
            ICollection<MoveEvent> result
        )
        {
            Require.IsFalse(IsResolved);
            Require.IsNotNull(indicator);
            Require.IsNotNull(result);

            foreach (var node in _children_to_be_satisfied)
            {
                node.FindExpectedEvents(indicator, result);
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Disjunction)}(" +
                   $"{nameof(Title)} = '{Title}', " +
                   $"{nameof(Children)} = {Children})";
        }

        private readonly List<Node> 
            _children_to_be_satisfied = new List<Node>();
    }
}
