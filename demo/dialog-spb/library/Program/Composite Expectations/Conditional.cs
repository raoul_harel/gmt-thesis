﻿using Common.Functional.Delegates;
using Common.Functional.Options;
using Common.Validation;
using System.Collections.Generic;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Dialog.SPB.Program
{
    /// <summary>
    /// This class represents an internal node expecting to satisfy an
    /// expectation only while a specified condition holds.
    /// </summary>
    public sealed class Conditional
        : Node
    {
        /// <summary>
        /// Creates a new node with the specified title, condition, and body.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="condition">The node's condition.</param>
        /// <param name="body">The node's body.</param>
        /// <remarks>
        /// <paramref name="title"/> must not be blank.
        /// <paramref name="condition"/> must not be null.
        /// <paramref name="body"/> must not be null.
        /// </remarks>
        public Conditional(string title, Predicate condition, Node body)
            : base(title, new Node[1] { body })
        {
            Require.IsNotNull(condition);
            Require.IsNotNull(body);

            Condition = condition;
            Body = Children[0];

            Reset();
        }

        /// <summary>
        /// Gets this node's condition predicate.
        /// </summary>
        public Predicate Condition { get; private set; }
        /// <summary>
        /// Gets this node's body.
        /// </summary>
        public Node Body { get; private set; }

        /// <summary>
        /// Gets the index of this node's active scope child (if it has any).
        /// </summary>
        public override Option<int> ScopeCarrierChildIndex
        {
            get
            {
                if (IsResolved || !Condition.Invoke())
                {
                    return Option.CreateNone<int>();
                }
                else
                {
                    return Option.CreateSome(0);
                }
            }
        }

        /// <summary>
        /// Called by <see cref="Node.Witness(MoveEvent)"/> to 
        /// update this node's expectations with regard to the witnessed dialog 
        /// move realization event.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// <see cref="ResolutionStatus.Satisfaction"/> if witnessing of this event 
        /// has caused all expectations of this node to be satisfied,
        /// <see cref="ResolutionStatus.Failure"/> if satisfaction of those
        /// expectation is no longer possible, or 
        /// <see cref="ResolutionStatus.Pending"/> otherwise.
        /// </returns>
        protected override ResolutionStatus OnWitness(MoveEvent @event)
        {
            if (!Condition.Invoke()) { return ResolutionStatus.Pending; }

            return Body.Witness(@event);
        }

        /// <summary>
        /// Finds and adds to the specified collection this node's expected 
        /// dialog move realization events that are indicated by the specified 
        /// function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <param name="result">The collection to add events to.</param>
        /// <remarks>
        /// <paramref name="indicator"/> must not be null.
        /// <paramref name="result"/> must not be null.
        /// </remarks>
        public override void FindExpectedEvents
        (
            Indicator<MoveEvent> indicator,
            ICollection<MoveEvent> result
        )
        {
            Require.IsFalse(IsResolved);
            Require.IsNotNull(indicator);
            Require.IsNotNull(result);

            if (Condition.Invoke())
            {
                Body.FindExpectedEvents(indicator, result);
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Conditional)}(" +
                   $"{nameof(Title)} = '{Title}', " +
                   $"{nameof(Body)} = {Body})";
        }
    }
}
