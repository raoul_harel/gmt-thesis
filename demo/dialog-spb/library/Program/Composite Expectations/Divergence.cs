﻿using Common.Functional.Delegates;
using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Validation;
using System.Collections.Generic;
using System.Linq;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Dialog.SPB.Program
{
    /// <summary>
    /// This class represents an internal node expecting to satisfy exactly
    /// one of its children. The child selected to be the one satisfied is the
    /// first whose active scope descendent is satisfied.
    /// </summary>
    public sealed class Divergence 
        : Node
    {
        /// <summary>
        /// Creates a new node with the specified title and children.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="children">The node's children.</param>
        /// <remarks>
        /// <paramref name="title"/> must not be blank.
        /// <paramref name="children"/> must not be null.
        /// <paramref name="children"/> must contain at least two items.
        /// </remarks>
        public Divergence(string title, IEnumerable<Node> children)
            : base(title, children)
        {
            Require.IsAtLeast(Children.Count, 2);

            // Cache descendent scopes:
            foreach (var child in Children)
            {
                _descendent_scope_carriers
                    .Add(child.GetDescendentScopeCarriers().Last());
            }
            // Initialize internal disjunction:
            _disjunction = new Disjunction
            (
                "<divergence disjunction>", 
                Children
            );

            Reset();
        }

        /// <summary>
        /// Gets the index of this node's active scope child (if it has any).
        /// </summary>
        public override Option<int> ScopeCarrierChildIndex
        {
            get
            {
                if (IsResolved)
                {
                    return Option.CreateNone<int>();
                }
                else
                {
                    return _active_child_index;
                }
            }
        }

        /// <summary>
        /// Called by <see cref="Node.Reset"/> to reset this node's expectations
        /// back to their initial state.
        /// </summary>
        protected override void OnReset()
        {
            _disjunction.Reset(do_recurse: false);
            _active_child_index = Option.CreateNone<int>();
        }
        /// <summary>
        /// Called by <see cref="Node.Witness(MoveEvent)"/> to 
        /// update this node's expectations with regard to the witnessed dialog 
        /// move realization event.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// <see cref="ResolutionStatus.Satisfaction"/> if witnessing of this event 
        /// has caused all expectations of this node to be satisfied,
        /// <see cref="ResolutionStatus.Failure"/> if satisfaction of those
        /// expectation is no longer possible, or 
        /// <see cref="ResolutionStatus.Pending"/> otherwise.
        /// </returns>
        protected override ResolutionStatus OnWitness(MoveEvent @event)
        {
            return ActiveChild.Match
            (
                none: () =>
                {
                    _disjunction.Witness(@event);
                    if (_disjunction.IsResolved)
                    {
                        return _disjunction.ResolutionStatus;
                    }

                    // Check if any of the descendent scopes have been
                    // satisfied:
                    int index = _descendent_scope_carriers.FindIndex
                    (
                        node =>
                            node.ResolutionStatus ==
                            ResolutionStatus.Satisfaction
                    );
                    if (index != -1)
                    {
                        _active_child_index = Option.CreateSome(index);
                    }
                    return ResolutionStatus.Pending;
                },
                some: node =>
                {
                    return node.Witness(@event);
                }
            );
        }

        /// <summary>
        /// Finds and adds to the specified collection this node's expected 
        /// dialog move realization events that are indicated by the specified 
        /// function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <param name="result">The collection to add events to.</param>
        /// <remarks>
        /// <paramref name="indicator"/> must not be null.
        /// <paramref name="result"/> must not be null.
        /// </remarks>
        public override void FindExpectedEvents
        (
            Indicator<MoveEvent> indicator,
            ICollection<MoveEvent> result
        )
        {
            Require.IsFalse(IsResolved);
            Require.IsNotNull(indicator);
            Require.IsNotNull(result);

            ActiveChild.Match
            (
                none: () => _disjunction.FindExpectedEvents(indicator, result),
                some: node => node.FindExpectedEvents(indicator, result)
            );
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Divergence)}(" +
                   $"{nameof(Title)} = '{Title}', " +
                   $"{nameof(ActiveChild)} = {ActiveChild}, " +
                   $"{nameof(Children)} = {Children})";
        }

        /// <summary>
        /// Gets the child to be satisfied.
        /// </summary>
        private Option<Node> ActiveChild
        {
            get
            {
                return _active_child_index.Match
                (
                    none: () => Option.CreateNone<Node>(),
                    some: index => Option.CreateSome(Children[index])
                );
            }
        }

        private readonly List<Node> _descendent_scope_carriers = new List<Node>();
        private readonly Disjunction _disjunction;
        private Option<int> _active_child_index;
    }
}
