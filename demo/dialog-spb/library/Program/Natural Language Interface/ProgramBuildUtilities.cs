﻿using Common.Functional.Delegates;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Modules.Perception;
using Dialog.Agency.Scene;
using System.Collections.Generic;

namespace Dialog.SPB.Program.NLI
{
    /// <summary>
    /// This class contains utility methods to help make program composition
    /// easier via a natural language interface.
    /// </summary>
    public static class ProgramBuildUtilities
    {
        /// <summary>
        /// Creates a zero target event expectation.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="move_kind">The dialog move kind.</param>
        /// <param name="source">The move's source actor.</param>
        /// <param name="resolved">
        /// Callback for <see cref="Node.Resolved"/>.
        /// </param>
        /// <param name="failed">
        /// Callback for <see cref="Node.Failed"/>.
        /// </param>
        /// <param name="satisfied">
        /// Callback for <see cref="Node.Satisfied"/>.
        /// </param>
        /// <returns>A move realization event expectation.</returns>
        public static EventExpectation ExpectEvent<TActor>
        (
            DialogMoveKind move_kind,
            TActor source,
            string title = "untitled",
            EventHandler<Node> resolved = null,
            EventHandler<Node> failed = null,
            EventHandler<Node> satisfied = null
        )
            where TActor : Actor
        {
            return ExpectEvent
            (
                move_kind,
                source, new TActor[0],
                title,
                resolved, failed, satisfied
            );
        }
        /// <summary>
        /// Creates a single target event expectation.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="move_kind">The dialog move kind.</param>
        /// <param name="source">The move's source actor.</param>
        /// <param name="target">The move's target actor.</param>
        /// <param name="resolved">
        /// Callback for <see cref="Node.Resolved"/>.
        /// </param>
        /// <param name="failed">
        /// Callback for <see cref="Node.Failed"/>.
        /// </param>
        /// <param name="satisfied">
        /// Callback for <see cref="Node.Satisfied"/>.
        /// </param>
        /// <returns>A move realization event expectation.</returns>
        public static EventExpectation ExpectEvent<TActor>
        (
            DialogMoveKind move_kind,
            TActor source,
            TActor target,
            string title = "untitled",
            EventHandler<Node> resolved = null,
            EventHandler<Node> failed = null,
            EventHandler<Node> satisfied = null
        )
            where TActor: Actor
        {
            return ExpectEvent
            (
                move_kind, 
                source, new TActor[] { target }, 
                title,
                resolved, failed, satisfied
            );
        }
        /// <summary>
        /// Creates a multi target event expectation.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="move_kind">The dialog move kind.</param>
        /// <param name="source">The move's source actor.</param>
        /// <param name="targets">The move's target actors.</param>
        /// <param name="resolved">
        /// Callback for <see cref="Node.Resolved"/>.
        /// </param>
        /// <param name="failed">
        /// Callback for <see cref="Node.Failed"/>.
        /// </param>
        /// <param name="satisfied">
        /// Callback for <see cref="Node.Satisfied"/>.
        /// </param>
        /// <returns>A move realization event expectation.</returns>
        public static EventExpectation ExpectEvent<TActor>
        (
            DialogMoveKind move_kind,
            TActor source,
            IEnumerable<TActor> targets,
            string title = "untitled",
            EventHandler<Node> resolved = null,
            EventHandler<Node> failed = null,
            EventHandler<Node> satisfied = null
        )
            where TActor : Actor
        {
            return ExpectEvent
            (
                new DialogMove<TActor>(move_kind, targets),
                source,
                title,
                resolved, failed, satisfied
            );
        }
        /// <summary>
        /// Creates an event expectation.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="move">The dialog move perfomed.</param>
        /// <param name="source">The move's source actor.</param>
        /// <param name="resolved">
        /// Callback for <see cref="Node.Resolved"/>.
        /// </param>
        /// <param name="failed">
        /// Callback for <see cref="Node.Failed"/>.
        /// </param>
        /// <param name="satisfied">
        /// Callback for <see cref="Node.Satisfied"/>.
        /// </param>
        /// <returns>A move realization event expectation.</returns>
        public static EventExpectation ExpectEvent
        (
            DialogMove move,
            Actor source,
            string title = "untitled",
            EventHandler<Node> resolved = null,
            EventHandler<Node> failed = null,
            EventHandler<Node> satisfied = null
        )
        {
            var node = new EventExpectation
            (
                title,
                @event: new DialogMoveRealizationEvent(source, move)
            );

            if (resolved != null) { node.Resolved += resolved; }
            if (failed != null) { node.Failed += failed; }
            if (satisfied != null) { node.Satisfied += satisfied; }

            return node;
        }

        /// <summary>
        /// Creates a conjunction expectation node.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="nodes">The node's children.</param>
        /// <returns>A conjunctive expectation.</returns>
        public static Conjunction ExpectAll
        (
            string title,
            params Node[] nodes
        )
        {
            return new Conjunction(title, nodes);
        }
        /// <summary>
        /// Creates a disjunction expectation node.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="nodes">The node's children.</param>
        /// <returns>A disjunctive expectation.</returns>
        public static Disjunction ExpectAny
        (
            string title,
            params Node[] nodes
        )
        {
            return new Disjunction(title, nodes);
        }
        /// <summary>
        /// Creates a sequence expectation node.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="nodes">The node's children.</param>
        /// <returns>A sequential expectation.</returns>
        public static Sequence ExpectSequence
        (
            string title,
            params Node[] nodes
        )
        {
            return new Sequence(title, nodes);
        }
        /// <summary>
        /// Creates a divergence expectation node.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="nodes">The node's children.</param>
        /// <returns>A divergent expectation.</returns>
        public static Divergence ExpectOne
        (
            string title,
            params Node[] nodes
        )
        {
            return new Divergence(title, nodes);
        }

        /// <summary>
        /// Creates a repeating expectation node.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="body">The node's body.</param>
        /// <returns>A repeating expectation.</returns>
        public static Repeat ExpectRepeat
        (
            Node body, 
            string title = "untitled"
        )
        {
            return new Repeat(title, body);
        }
        /// <summary>
        /// Creates a conditional expectation node.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="condition">The node's condition.</param>
        /// <param name="body">The node's body.</param>
        /// <returns>A repeating expectation.</returns>
        public static Conditional ExpectIf
        (
            Predicate condition,
            Node body,
            string title = "untitled"
        )
        {
            return new Conditional(title, condition, body);
        }
    }
}
