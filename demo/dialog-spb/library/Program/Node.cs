﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Functional.Delegates;
using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Validation;
using System.Collections.Generic;
using System.Linq;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Dialog.SPB.Program
{
    /// <summary>
    /// This class represents a node of a social practice program.
    /// </summary>
    public abstract class Node
    {
        /// <summary>
        /// Occurs when the node's expectation status is assigned a value 
        /// different from <see cref="ResolutionStatus.Pending"/>.
        /// </summary>
        public event EventHandler<Node> Resolved = delegate { };
        /// <summary>
        /// Occurs when the node's expectation status is assigned a value of
        /// <see cref="ResolutionStatus.Failure"/>.
        /// </summary>
        public event EventHandler<Node> Failed = delegate { };
        /// <summary>
        /// Occurs when the node's expectation status is assigned a value of
        /// <see cref="ResolutionStatus.Satisfaction"/>.
        /// </summary>
        public event EventHandler<Node> Satisfied = delegate { };
        
        /// <summary>
        /// Creates a new node with the specified title and children.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="children">The node's children.</param>
        /// <remarks>
        /// <paramref name="title"/> must not be blank.
        /// <paramref name="children"/> must not be null.
        /// </remarks>
        protected Node(string title, IEnumerable<Node> children)
        {
            Require.IsNotBlank(title);
            Require.IsNotNull(children);

            Title = title;
            Children = new ListView<Node>(children.ToList());
        }

        /// <summary>
        /// Gets this node's title.
        /// </summary>
        public string Title { get; private set; }
        /// <summary>
        /// Gets this node's children.
        /// </summary>
        public ImmutableList<Node> Children { get; private set; }

        /// <summary>
        /// Gets the index of this node's active scope child (if it has any).
        /// </summary>
        public virtual Option<int> ScopeCarrierChildIndex
            { get; protected set; } = Option.CreateNone<int>();
        
        /// <summary>
        /// Gets this node's expectation status.
        /// </summary>
        public ResolutionStatus ResolutionStatus
            { get; private set; } = ResolutionStatus.Pending;

        /// <summary>
        /// Indicates whether this node's expectations have been resolved in 
        /// one way or another.
        /// </summary>
        public bool IsResolved
        {
            get { return ResolutionStatus != ResolutionStatus.Pending; }
        }

        /// <summary>
        /// Resets this node's expectations to their initial state 
        /// (i.e. pending).
        /// </summary>
        /// <param name="do_recurse">
        /// Indicates whether this node's children should be recursively reset 
        /// as well.
        /// </param>
        public void Reset(bool do_recurse = true)
        {
            ResolutionStatus = ResolutionStatus.Pending;

            if (do_recurse)
            {
                foreach (var child in Children) { child.Reset(); }
            }

            OnReset();
        }
        /// <summary>
        /// Called by <see cref="Reset"/> to reset this node's expectations
        /// back to their initial state.
        /// </summary>
        protected virtual void OnReset() { }

        /// <summary>
        /// Sets this node's expectation status to 
        /// <see cref="ResolutionStatus.Failure"/> and invokes related events.
        /// </summary>
        public void Fail()
        {
            ResolutionStatus = ResolutionStatus.Failure;
            Resolved.Invoke(this);
            Failed.Invoke(this);
        }
        /// <summary>
        /// Sets this node's expectation status to
        /// <see cref="ResolutionStatus.Satisfaction"/> and invokes related 
        /// events.
        /// </summary>
        public void Satisfy()
        {
            ResolutionStatus = ResolutionStatus.Satisfaction;
            Resolved.Invoke(this);
            Satisfied.Invoke(this);
        }

        /// <summary>
        /// Processes the specified dialog move realization event, and updates 
        /// this node's expectations accordingly.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// This node's expectation status.
        /// </returns>
        public ResolutionStatus Witness(MoveEvent @event)
        {
            if (IsResolved) { return ResolutionStatus; }
            else
            {
                switch (OnWitness(@event))
                {
                    case ResolutionStatus.Failure: Fail(); break;
                    case ResolutionStatus.Satisfaction: Satisfy(); break;
                }
                return ResolutionStatus;
            }
        }
        /// <summary>
        /// Called by <see cref="Witness(MoveEvent)"/> to 
        /// update this node's expectations with regard to the witnessed dialog 
        /// move realization event.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// <see cref="ResolutionStatus.Satisfaction"/> if witnessing of this event 
        /// has caused all expectations of this node to be satisfied,
        /// <see cref="ResolutionStatus.Failure"/> if satisfaction of those
        /// expectation is no longer possible, or 
        /// <see cref="ResolutionStatus.Pending"/> otherwise.
        /// </returns>
        protected abstract ResolutionStatus OnWitness(MoveEvent @event);

        /// <summary>
        /// Finds and returns this node's expected dialog move realization 
        /// events that are indicated by the specified function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <returns>
        /// An enumeration of events indicated by the specified function.
        /// </returns>
        public IEnumerable<MoveEvent> 
            FindExpectedEvents(Indicator<MoveEvent> indicator)
        {
            var result = new List<MoveEvent>();

            FindExpectedEvents(indicator, result);

            return result;
        }
        /// <summary>
        /// Finds and adds to the specified collection this node's expected 
        /// dialog move realization events that are indicated by the specified 
        /// function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <param name="result">The collection to add events to.</param>
        public abstract void FindExpectedEvents
        (
            Indicator<MoveEvent> indicator,
            ICollection<MoveEvent> result
        );

        /// <summary>
        /// Gets this node's active scope-descendants.
        /// </summary>
        /// <returns>
        /// An enumeration of nodes, ordered from shallow to deep, starting 
        /// with this node itself.
        /// </returns>
        public IEnumerable<Node> GetDescendentScopeCarriers()
        {
            var result = new List<Node>();

            GetActiveScopeDescendants(result);

            return result;
        }
        /// <summary>
        /// Adds this node's active scope-descendants to the specified 
        /// collection.
        /// </summary>
        /// <param name="result">The collection to add nodes to.</param>
        /// <remarks>
        /// <paramref name="result"/> must not be null.
        /// </remarks>
        public void GetActiveScopeDescendants(ICollection<Node> result)
        {
            Require.IsNotNull(result);

            ScopeCarrierChildIndex.Match
            (
                none: () => result.Add(this),
                some: index =>
                {
                    result.Add(this);
                    Children[index].GetActiveScopeDescendants(result);
                }
            );
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Node)}(" +
                   $"{nameof(Title)} = '{Title}', " +
                   $"{nameof(ResolutionStatus)} = {ResolutionStatus})";
        }
    }
}
