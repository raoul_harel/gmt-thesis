﻿using Common.Functional.Delegates;
using Common.Validation;
using Dialog.Agency.Dialog_Moves.Extensions;
using System.Collections.Generic;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Dialog.SPB.Program
{
    /// <summary>
    /// This class represents a node with the expectation of witnessing a 
    /// concrete dialog move realization event.
    /// </summary>
    public sealed class EventExpectation 
        : Node
    {
        /// <summary>
        /// Creates a new node with the specified title and expecting the 
        /// specified event.
        /// </summary>
        /// <param name="event">The expected event.</param>
        /// <param name="title">The node's title.</param>
        /// <remarks>
        /// <paramref name="event"/> must not be null.
        /// <paramref name="event"/> must not refer to the idle move.
        /// </remarks>
        public EventExpectation(string title, MoveEvent @event)
            : base(title, new Node[0])
        {
            Require.IsNotNull(@event);
            Require.IsFalse(@event.Move.IsIdle());

            Event = @event;

            Reset();
        }

        /// <summary>
        /// Gets this node's expected event.
        /// </summary>
        public MoveEvent Event { get; private set; }

        /// <summary>
        /// Called by <see cref="Node.Witness(MoveEvent)"/> to 
        /// update this node's expectations with regard to the witnessed dialog 
        /// move realization event.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// <see cref="ResolutionStatus.Satisfaction"/> if witnessing of this event 
        /// has caused all expectations of this node to be satisfied,
        /// <see cref="ResolutionStatus.Failure"/> if satisfaction of those
        /// expectation is no longer possible, or 
        /// <see cref="ResolutionStatus.Pending"/> otherwise.
        /// </returns>
        protected override ResolutionStatus OnWitness(MoveEvent @event)
        {
            if (@event.Equals(Event))
            {
                return ResolutionStatus.Satisfaction;
            }
            else
            {
                return ResolutionStatus.Pending;
            }
        }

        /// <summary>
        /// Finds and adds to the specified collection this node's expected 
        /// dialog move realization events that are indicated by the specified 
        /// function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <param name="result">The collection to add events to.</param>
        /// <remarks>
        /// <paramref name="indicator"/> must not be null.
        /// <paramref name="result"/> must not be null.
        /// </remarks>
        public override void FindExpectedEvents
        (
            Indicator<MoveEvent> indicator,
            ICollection<MoveEvent> result
        )
        {
            Require.IsFalse(IsResolved);
            Require.IsNotNull(indicator);
            Require.IsNotNull(result);

            if (indicator.Invoke(Event))
            {
                result.Add(Event);
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(EventExpectation)}(" +
                   $"{nameof(Title)} = '{Title}', " +
                   $"{nameof(Event)} = {Event}, " +
                   $"{nameof(ResolutionStatus)} = {ResolutionStatus})";
        }
    }
}
