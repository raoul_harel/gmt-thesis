﻿namespace Dialog.SPB.Timing_Conflicts
{
    /// <summary>
    /// Enumerates interruption resolution strategies.
    /// </summary>
    public enum InterruptionSurrender : int
    {
        /// <summary>
        /// Don't surrender the floor to an interruption by (an)other actor(s).
        /// </summary>
        Dont = 0,
        /// <summary>
        /// Do surrender the floor to an interruption by (an)other actor(s).
        /// </summary>
        Do = 1
    }
}
