﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Functional.Delegates;
using Common.Validation;
using Dialog.Agency.Scene;
using System.Collections.Generic;
using System.Linq;
using IIRule = Dialog.SPB.Rules.Rule<Dialog.SPB.Timing_Conflicts.InterruptionInitiation>;
using ISRule = Dialog.SPB.Rules.Rule<Dialog.SPB.Timing_Conflicts.InterruptionSurrender>;

namespace Dialog.SPB.Timing_Conflicts
{
    /// <summary>
    /// This structure represents a collection of interruption rules regarding 
    /// both interruption-inititation as well as -surrender.
    /// </summary>
    public sealed class InterruptionRules
    {
        /// <summary>
        /// Rule for global indifference to conflict.
        /// </summary>
        public static readonly InterruptionRules CONFLICT_INDIFFERENCE =
            new InterruptionRules
            (
                new IIRule[1]
                {
                    new IIRule  // Always initiate interruption
                    (
                        precondition: Predicates.Always,
                        affects: Indicators<Actor>.Any,
                        implication: InterruptionInitiation.Do,
                        weight: 1.0f
                    )
                },
                new ISRule[1]
                {
                    new ISRule  // Never surrender to interruption
                    (
                        precondition: Predicates.Always,
                        affects: Indicators<Actor>.Any,
                        implication: InterruptionSurrender.Dont,
                        weight: 1.0f
                    )
                }
            );
        /// <summary>
        /// Rules for global avoidance of conflict.
        /// </summary>
        public static readonly InterruptionRules CONFLICT_AVOIDANCE =
            new InterruptionRules
            (
                new IIRule[1]
                {
                    new IIRule  // Never initiate interruption
                    (
                        precondition: Predicates.Always,
                        affects: Indicators<Actor>.Any,
                        implication: InterruptionInitiation.Dont,
                        weight: 1.0f
                    )
                },
                new ISRule[1]
                {
                    new ISRule  // Always surrender to interruption
                    (
                        precondition: Predicates.Always,
                        affects: Indicators<Actor>.Any,
                        implication: InterruptionSurrender.Do,
                        weight: 1.0f
                    )
                }
            );

        /// <summary>
        /// Creates a new rule set.
        /// </summary>
        /// <param name="initiation_rules">Initiation rules.</param>
        /// <param name="surrender_rules">Surrender rules.</param>
        /// <remarks>
        /// <paramref name="initiation_rules"/> must not be null.
        /// <paramref name="surrender_rules"/> must not be null.
        /// </remarks>
        public InterruptionRules
        (
            IEnumerable<IIRule> initiation_rules,
            IEnumerable<ISRule> surrender_rules
        )
        {
            Require.IsNotNull(initiation_rules);
            Require.IsNotNull(surrender_rules);

            Initiation = 
                new CollectionView<IIRule>(initiation_rules.ToList());
            Surrender =
                new CollectionView<ISRule>(surrender_rules.ToList());
        }
        /// <summary>
        /// Creates a new empty rule set.
        /// </summary>
        public InterruptionRules()
            : this(new IIRule[0], new ISRule[0])
        { }

        /// <summary>
        /// Gets the interruption initiation rules.
        /// </summary>
        public ImmutableCollection<IIRule> Initiation { get; private set; }
        /// <summary>
        /// Gets the interruption surrender rules.
        /// </summary>
        public ImmutableCollection<ISRule> Surrender { get; private set; }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(InterruptionRules)}(" +
                   $"{nameof(Initiation)} = {Initiation}, " +
                   $"{nameof(Surrender)} = {Surrender})";
        }
    }
}
