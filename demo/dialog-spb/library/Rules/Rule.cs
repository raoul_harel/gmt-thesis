﻿using Common.Functional.Delegates;
using Common.Hashing;
using Common.Validation;
using Dialog.Agency.Scene;

namespace Dialog.SPB.Rules
{
    /// <summary>
    /// This class represents some social rule. Social rules weigh-in on some
    /// decision problem and imply a response. They contain the following 
    /// components:
    ///     1. A precondition determining when it applies.
    ///     2. An indicator function determining onto which actors it applies.
    ///     3. The response the rule implies to the decision problem.
    ///     4. A numeric weight, indicating how dominating this rule is
    ///        relative to others.
    /// </summary>
    /// <typeparam name="T">
    /// The type of implication the rule entails.
    /// </typeparam>
    public sealed class Rule<T>
    {
        /// <summary>
        /// Creates a new rule.
        /// </summary>
        /// <param name="precondition">
        /// The precondition required to activate this rule.
        /// </param>
        /// <param name="affects">
        /// An indicator of the actors this rule affects.
        /// </param>
        /// <param name="implication">This rule's implication.</param>
        /// <param name="weight">This rule's weight.</param>
        /// <remarks>
        /// <paramref name="precondition"/> must not be null.
        /// <paramref name="affects"/> must not be null. 
        /// <paramref name="weight"/> must be at least 0.
        /// </remarks>
        public Rule
        (
            Predicate precondition, 
            Indicator<Actor> affects,
            T implication,
            float weight = 1.0f
        )
        {
            Require.IsNotNull(precondition);
            Require.IsNotNull(affects);
            Require.IsAtLeast(weight, 0.0f);

            _precondition = precondition;
            _affected_actors_indicator = affects;
            Implication = implication;
            Weight = weight;
        }

        /// <summary>
        /// Gets this rule's implication.
        /// </summary>
        public T Implication { get; private set; }

        /// <summary>
        /// Gets this rule's weight.
        /// </summary>
        public float Weight { get; private set; }

        /// <summary>
        /// Determines whether this rule is relevant at this time, i.e. if its
        /// precondition is satisfied.
        /// </summary>
        /// <returns>
        /// True if this rule is relevant at this time, otherwise false.
        /// </returns>
        public bool IsRelevant()
        {
            return _precondition.Invoke();
        }
        /// <summary>
        /// Determines whether the specified actor is affected by this rule.
        /// </summary>
        /// <param name="actor">The actor to test.</param>
        /// <returns>
        /// True if the specified actor is affected by this rule, otherwise 
        /// false.
        /// </returns>
        public bool IsAffecting(Actor actor)
        {
            return _affected_actors_indicator.Invoke(actor);
        }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as Rule<T>;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other._precondition.Equals(_precondition) &&
                   other._affected_actors_indicator
                        .Equals(_affected_actors_indicator) &&
                   Equals(other.Implication, Implication) &&
                   other.Weight.Equals(Weight);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, _precondition);
            hash = HashCombiner.Hash(hash, _affected_actors_indicator);
            hash = HashCombiner.Hash(hash, Implication);
            hash = HashCombiner.Hash(hash, Weight);

            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Rule<T>)}(" +
                   $"{nameof(Implication)} = {Implication}, " +
                   $"{nameof(Weight)} = {Weight})";
        }

        private readonly Predicate _precondition;
        private readonly Indicator<Actor> _affected_actors_indicator;
    }
    /// <summary>
    /// Convenience methods for the creation of rules.
    /// </summary>
    public static class Rule
    {
        /// <summary>
        /// Creates a new rule.
        /// </summary>
        /// <typeparam name="T">
        /// The type of implication the rule entails.
        /// </typeparam>
        /// <param name="precondition">
        /// The precondition required to activate this rule.
        /// </param>
        /// <param name="affects">
        /// An indicator of the actors this rule affects.
        /// </param>
        /// <param name="implication">This rule's implication.</param>
        /// <param name="weight">This rule's weight.</param>
        /// <returns>A new rule.</returns>
        /// <remarks>
        /// <paramref name="precondition"/> must not be null.
        /// <paramref name="affects"/> must not be null. 
        /// <paramref name="weight"/> must be at least 0.
        /// </remarks>
        public static Rule<T> Create<T>
        (
            Predicate precondition,
            Indicator<Actor> affects,
            T implication,
            float weight = 1.0f
        )
        {
            Require.IsNotNull(precondition);
            Require.IsNotNull(affects);
            Require.IsAtLeast(weight, 0.0f);

            return new Rule<T>
            (
                precondition, 
                affects, 
                implication, 
                weight
            );
        }
    }
}
