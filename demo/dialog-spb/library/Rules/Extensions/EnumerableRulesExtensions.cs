﻿using Common.Validation;
using Dialog.Agency.Scene;
using System.Collections.Generic;
using System.Linq;

namespace Dialog.SPB.Rules.Extensions
{
    /// <summary>
    /// Extension methods involving rules in enumerations.
    /// </summary>
    public static class EnumerableRulesExtensions
    {
        /// <summary>
        /// Evaluates an enumeration of rules for the specified actor at this 
        /// time.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the implication the rules derive.
        /// </typeparam>
        /// <param name="enumerable">The enumerable to evaluate.</param>
        /// <param name="actor">
        /// The actor for against which the rules should be evaluated.
        /// </param>
        /// <returns>The implication supported most heavily.</returns>
        /// <remarks>
        /// The rules each vote for their own implication with their weight.
        /// The rule with the most weight is the one whose implication will
        /// be returned. In case of a tie, the rule appearing last in the 
        /// enumeration wins.
        /// </remarks>
        public static T EvaluateFor<T>
        (
            this IEnumerable<Rule<T>> enumerable,
            Actor actor
        )
        {
            Require.IsNotNull(enumerable);
            Require.IsNotNull(actor);

            var ballot = new Dictionary<T, float>();
            foreach (Rule<T> rule in enumerable)
            {
                if (!rule.IsRelevant() || 
                    !rule.IsAffecting(actor))
                {
                    continue;
                }
                if (!ballot.ContainsKey(rule.Implication))
                {
                    ballot.Add(rule.Implication, 0.0f);
                }
                ballot[rule.Implication] += rule.Weight;
            }

            if (ballot.Count == 0) { return default(T); }
            else
            {
                return ballot.Aggregate
                (
                    (a, b) => a.Value > b.Value ? a : b
                ).Key;
            }
        }
    }
}
