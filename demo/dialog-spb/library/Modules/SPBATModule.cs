﻿using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Modules.Action;
using Dialog.Agency.Modules.Perception;
using Dialog.SPB.Rules;
using Dialog.SPB.Rules.Extensions;
using Dialog.SPB.State;
using Dialog.SPB.Timing_Conflicts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dialog.SPB.Modules
{
    /// <summary>
    /// This class is a stub implementation of the 
    /// <see cref="ActionTimingModule"/> that uses social practices to derive
    /// action timing.
    /// </summary>
    public sealed class SPBATModule
        : ActionTimingModule
    {
        /// <summary>
        /// Creates a new module with the specified interruption rules.
        /// </summary>
        /// <param name="interruption_rules">
        /// Associates program node names to interruption rules.
        /// </param>
        /// <remarks>
        /// <paramref name="interruption_rules"/> must not be null.
        /// </remarks>
        public SPBATModule
        (
            IEnumerable<KeyValuePair<string, InterruptionRules>> 
            interruption_rules
        )
        {
            Require.IsNotNull(interruption_rules);

            foreach (var kv in interruption_rules)
            {
                _interruption_rules_by_node_title.Add(kv.Key, kv.Value);
            }
        }
        /// <summary>
        /// Creates a new module.
        /// </summary>
        public SPBATModule()
            : this(new KeyValuePair<string, InterruptionRules>[0])
        { }

        /// <summary>
        /// Gets the collection of data types required by this module to be 
        /// available through the information state.
        /// </summary>
        /// <param name="types">The collection of types.</param>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override
            void GetRequiredStateComponents(ICollection<Type> types)
        {
            types.Add(typeof(CurrentActivityReport));
            types.Add(typeof(SocialContext));
        }

        /// <summary>
        /// Determines whether now is a valid time to realize the specified 
        /// dialog move.
        /// </summary>
        /// <param name="move">The desired dialog move to make.</param>
        /// <returns>True.</returns>
        /// <remarks>
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public override bool IsValidMoveNow(DialogMove move)
        {
            Require.IsNotNull(move);

            var current_activity = State.Get<CurrentActivityReport>();
            var context = State.Get<SocialContext>();

            if (context.Program.IsResolved) { return false; }

            bool am_speaking =
                current_activity.ActiveActors.Contains(context.Self);
            bool floor_is_free = 
                current_activity.ActiveActors.Count == 0;
            bool am_only_one_active =
                current_activity.ActiveActors.Count == 1 && am_speaking;

            if (floor_is_free || am_only_one_active)
            {
                return true;
            }
            
            if (!am_speaking)
            {
                // Decide whether to initiate an interruption:
                var rule_nodes = context.Program
                    .GetDescendentScopeCarriers()
                    .Where(node => _interruption_rules_by_node_title.ContainsKey(node.Title));

                IEnumerable<Rule<InterruptionInitiation>> rules;
                if (rule_nodes.Count() > 0)
                {
                    rules = rule_nodes
                        .Select(node => _interruption_rules_by_node_title[node.Title]
                                        .Initiation.AsEnumerable())
                        .Aggregate((total, part) => total.Concat(part));
                }
                else
                {
                    rules = InterruptionRules.CONFLICT_AVOIDANCE.Initiation;
                }

                return
                    rules.EvaluateFor(context.Self) ==
                    InterruptionInitiation.Do ?
                    true : false;
            }
            else
            {
                // Decide whether to surrender to an interruption:
                var rule_nodes = context.Program
                    .GetDescendentScopeCarriers()
                    .Where(node => _interruption_rules_by_node_title.ContainsKey(node.Title));

                IEnumerable<Rule<InterruptionSurrender>> rules;
                if (rule_nodes.Count() > 0)
                {
                    rules = rule_nodes
                        .Select(node => _interruption_rules_by_node_title[node.Title]
                                        .Surrender.AsEnumerable())
                        .Aggregate((total, part) => total.Concat(part));
                }
                else
                {
                    rules = InterruptionRules.CONFLICT_AVOIDANCE.Surrender;
                }

                return
                    rules.EvaluateFor(context.Self) == 
                    InterruptionSurrender.Do ?
                    false: true;
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(SPBATModule)}(" +
                   $"{nameof(State)} = {State})";
        }

        private readonly Dictionary<string, InterruptionRules>
            _interruption_rules_by_node_title =
            new Dictionary<string, InterruptionRules>();
    }
}
