﻿using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Modules.Deliberation;
using Dialog.Agency.Modules.Perception;
using Dialog.Agency.State;
using Dialog.SPB.State;
using System;
using System.Collections.Generic;

namespace Dialog.SPB.Modules
{
    /// <summary>
    /// This class is an implementation of the 
    /// <see cref="ActionSelectionModule"/> that uses social practices to 
    /// derive action. A list of candidate moves is derived from a practice
    /// program, and then a user-supplied method selects the winning candidate
    /// out of it.
    /// </summary>
    public sealed class SPBASModule
        : ActionSelectionModule
    {
        /// <summary>
        /// This delegate defines a method for selection of one winner out of a 
        /// set of candidate dialog moves.
        /// </summary>
        /// <param name="candidates">The candidate list.</param>
        /// <param name="state">The information state.</param>
        /// <returns>
        /// The index of the winning move in the candidate list.
        /// </returns>
        public delegate int SelectionMethod
        (
            List<DialogMove> candidates, 
            ImmutableState state
        );

        /// <summary>
        /// Creates a new module using the specfied winner candidate selector.
        /// </summary>
        /// <param name="winner_candidate_selector">
        /// The selection method to use when choosing a winner from the 
        /// candidate move list.
        /// </param>
        /// <remarks>
        /// <paramref name="winner_candidate_selector"/> must not be null.
        /// </remarks>
        public SPBASModule(SelectionMethod winner_candidate_selector)
        {
            Require.IsNotNull(winner_candidate_selector);

            WinnerCandidateSelector = winner_candidate_selector;
        }
        /// <summary>
        /// Creates a new module with a default winner candidate selector.
        /// </summary>
        /// <remarks>
        /// The default selector always selects the first candidate as the 
        /// winner.
        /// </remarks>
        public SPBASModule()
            : this((candidates, _) => 0)
        { }

        /// <summary>
        /// Gets the selection method used with the candidate move list.
        /// </summary>
        public SelectionMethod WinnerCandidateSelector { get; private set; }

        /// <summary>
        /// Gets the collection of data types required by this module to be 
        /// available through the information state.
        /// </summary>
        /// <param name="types">The collection of types.</param>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override 
            void GetRequiredStateComponents(ICollection<Type> types)
        {
            types.Add(typeof(SocialContext));
        }

        /// <summary>
        /// Selects a target dialog move for the system to perform.
        /// </summary>
        /// <returns>
        /// A move derived from the active social practice program.
        /// </returns>
        public override DialogMove SelectMove()
        {
            var context = State.Get<SocialContext>();

            if (context.Program.IsResolved) { return IdleMove.Instance; }

            _acceptable_set.Clear();
            context.Program.FindExpectedEvents
            (
                @event => @event.Source == context.Self,
                _acceptable_set
            );
            var candidates = _acceptable_set.ConvertAll(@event => @event.Move);

            if (candidates.Count == 0) { return IdleMove.Instance; }
            else if (candidates.Count == 1) { return candidates[0]; }
            else
            {
                int winner_index = 
                    WinnerCandidateSelector.Invoke(candidates, State);

                return candidates[winner_index];
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(SPBASModule)}(" +
                   $"{nameof(State)} = {State})";
        }

        private List<DialogMoveRealizationEvent> 
            _acceptable_set = new List<DialogMoveRealizationEvent>();
    }
}
