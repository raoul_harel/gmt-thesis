﻿using Common.Functional.Delegates;
using Dialog.Agency.Scene;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Dialog.SPB.Rules.Extensions.Test
{
    [TestFixture]
    public sealed class EnumerableRulesExtensionsTest
    {
        private static readonly Actor ACTOR = new Mock<Actor>().Object;

        [Test]
        public void Test_Evaluation_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => ((IEnumerable<Rule<int>>)null).EvaluateFor(ACTOR)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new List<Rule<int>>().EvaluateFor(null)
            );
        }
        [Test]
        public void Test_Evaluation_ForAnEmptySequence()
        {
            var rules = new List<Rule<int>>();

            Assert.AreEqual(default(int), rules.EvaluateFor(ACTOR));
        }
        [Test]
        public void Test_Evaluation_ForIrrelevantRule()
        {
            var rule = new Rule<int>
            (
                precondition: Predicates.Never,
                affects: Indicators<Actor>.Any,
                implication: 42,
                weight: 1.0f
            );
            var rules = new List<Rule<int>> { rule };

            Assert.AreEqual(default(int), rules.EvaluateFor(ACTOR));
        }
        [Test]
        public void Test_Evaluation_ForUnaffectedActor()
        {
            var rule = new Rule<int>
            (
                precondition: Predicates.Always,
                affects: Indicators<Actor>.None,
                implication: 42,
                weight: 1.0f
            );
            var rules = new List<Rule<int>> { rule };

            Assert.AreEqual(default(int), rules.EvaluateFor(ACTOR));
        }
        [Test]
        public void Test_Evaluation_WithCompetingRules()
        {
            var first_rule = new Rule<int>
            (
                precondition: Predicates.Always,
                affects: Indicators<Actor>.Any,
                implication: 1,
                weight: 1.0f
            );
            var second_rule = new Rule<int>
            (
                precondition: Predicates.Always,
                affects: Indicators<Actor>.Any,
                implication: 2,
                weight: 2.0f
            );

            var rules_ascending = new List<Rule<int>>
            {
                first_rule,
                second_rule
            };
            var rules_descending = new List<Rule<int>>
            {
                second_rule,
                first_rule
            };

            Assert.AreEqual
            (
                second_rule.Implication, 
                rules_ascending.EvaluateFor(ACTOR)
            );
            Assert.AreEqual
            (
                second_rule.Implication,
                rules_descending.EvaluateFor(ACTOR)
            );
        }
    }
}
