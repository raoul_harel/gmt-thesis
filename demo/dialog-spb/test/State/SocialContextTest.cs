﻿using Common.Functional.Delegates;
using Dialog.Agency.Scene;
using Dialog.SPB.Program;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Dialog.SPB.State.Test
{
    [TestFixture]
    public sealed class SocialContextTest
    {
        private sealed class MockNode 
            : Node
        {
            public MockNode()
                : base("mock", new Node[0])
            { }

            public override void FindExpectedEvents(Indicator<MoveEvent> indicator, ICollection<MoveEvent> result)
            {
                throw new NotImplementedException();
            }

            protected override ResolutionStatus OnWitness(MoveEvent @event)
            {
                throw new NotImplementedException();
            }
        }

        private static readonly Actor 
            ACTOR = new Mock<Actor>().Object;
        private static readonly Node
            PROGRAM = new MockNode();

        private SocialContext _context;

        [SetUp]
        public void Setup()
        {
            _context = new SocialContext(ACTOR, PROGRAM);
        }
        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new SocialContext(null, PROGRAM)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new SocialContext(ACTOR, null)
            );
        }
        [Test]
        public void Test_Constructor()
        {
            Assert.AreEqual(ACTOR, _context.Self);
            Assert.AreEqual(PROGRAM, _context.Program);
        }

        [Test]
        public void Test_Equality()
        {
            var original = new SocialContext(ACTOR, PROGRAM);
            var good_copy = new SocialContext(ACTOR, PROGRAM);
            var flawed_actor_copy = new SocialContext
            (
                new Mock<Actor>().Object,
                PROGRAM
            );
            var flawed_program_copy = new SocialContext
            (
                ACTOR,
                new MockNode()
            );

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_actor_copy);
            Assert.AreNotEqual(original, flawed_program_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
