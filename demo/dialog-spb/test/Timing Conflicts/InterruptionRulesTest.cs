﻿using Common.Collections.Extensions;
using Common.Functional.Delegates;
using Dialog.Agency.Scene;
using Dialog.SPB.Rules;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Dialog.SPB.Timing_Conflicts.Test
{
    [TestFixture]
    public sealed class InterruptionRulesTest
    {
        private static readonly
            IEnumerable<Rule<InterruptionInitiation>>
            INITIATION_RULES =
            new Rule<InterruptionInitiation>[1]
            {
                new Rule<InterruptionInitiation>
                (
                    Predicates.Never,
                    Indicators<Actor>.None,
                    InterruptionInitiation.Do
                )
            };
        private static readonly
            IEnumerable<Rule<InterruptionSurrender>>
            SURRENDER_RULES =
            new Rule<InterruptionSurrender>[1]
            {
                new Rule<InterruptionSurrender>
                (
                    Predicates.Never,
                    Indicators<Actor>.None,
                    InterruptionSurrender.Do
                )
            };

        private InterruptionRules _rules;

        [SetUp]
        public void Setup()
        {
            _rules = new InterruptionRules(INITIATION_RULES, SURRENDER_RULES);
        }

        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new InterruptionRules(null, SURRENDER_RULES)
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new InterruptionRules(INITIATION_RULES, null)
            );
        }
        [Test]
        public void Test_Constructor_Default()
        {
            _rules = new InterruptionRules();

            Assert.IsTrue(_rules.Initiation.IsEmpty());
            Assert.IsTrue(_rules.Surrender.IsEmpty());
        }
        [Test]
        public void Test_Constructor()
        {
            Assert.AreEqual(INITIATION_RULES, _rules.Initiation);
            Assert.AreEqual(SURRENDER_RULES, _rules.Surrender);
        }
    }
}
