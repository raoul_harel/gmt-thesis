var hierarchy =
[
    [ "Dialog.Agency.Scene.Actor", "interface_dialog_1_1_agency_1_1_scene_1_1_actor.html", null ],
    [ "Dialog.Agency.System.AgencySystem", "class_dialog_1_1_agency_1_1_system_1_1_agency_system.html", null ],
    [ "Common.Time.Clock", "interface_common_1_1_time_1_1_clock.html", [
      [ "Common.Time.VariableIncrementClock", "class_common_1_1_time_1_1_variable_increment_clock.html", null ]
    ] ],
    [ "Comparer", null, [
      [ "Common.Functional.FunctionalComparer< T >", "class_common_1_1_functional_1_1_functional_comparer.html", null ]
    ] ],
    [ "Dialog.Agency.Modules.Perception.CurrentActivityReport", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_current_activity_report.html", null ],
    [ "Dialog.Agency.Dialog_Moves.DialogMove", "interface_dialog_1_1_agency_1_1_dialog___moves_1_1_dialog_move.html", [
      [ "Dialog.Agency.Dialog_Moves.DialogMove< TActor >", "class_dialog_1_1_agency_1_1_dialog___moves_1_1_dialog_move.html", null ]
    ] ],
    [ "Dialog.Agency.Dialog_Moves.DialogMove< Actor >", "interface_dialog_1_1_agency_1_1_dialog___moves_1_1_dialog_move.html", [
      [ "Dialog.Agency.Dialog_Moves.IdleMove", "class_dialog_1_1_agency_1_1_dialog___moves_1_1_idle_move.html", null ]
    ] ],
    [ "Dialog.Agency.Dialog_Moves.DialogMoveKind", "class_dialog_1_1_agency_1_1_dialog___moves_1_1_dialog_move_kind.html", null ],
    [ "Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_dialog_move_realization_event.html", null ],
    [ "Common.Randomization.Distributions.Distribution", "interface_common_1_1_randomization_1_1_distributions_1_1_distribution.html", [
      [ "Common.Randomization.Distributions.PseudoRandomDistribution", "class_common_1_1_randomization_1_1_distributions_1_1_pseudo_random_distribution.html", [
        [ "Common.Randomization.Distributions.UniformDistribution", "class_common_1_1_randomization_1_1_distributions_1_1_uniform_distribution.html", null ]
      ] ]
    ] ],
    [ "IEnumerable", null, [
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ],
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ],
      [ "Dialog.Agency.System.ModuleBundle", "class_dialog_1_1_agency_1_1_system_1_1_module_bundle.html", null ]
    ] ],
    [ "Common.Collections.Interfaces.ImmutableCollection", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", [
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ]
    ] ],
    [ "Common.Collections.Interfaces.ImmutableCollection< Actor >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ],
    [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", [
      [ "Common.Collections.Interfaces.ImmutableList< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_list.html", [
        [ "Common.Collections.Views.ListView< T >", "class_common_1_1_collections_1_1_views_1_1_list_view.html", null ]
      ] ],
      [ "Common.Collections.Views.CollectionView< T >", "class_common_1_1_collections_1_1_views_1_1_collection_view.html", [
        [ "Common.Collections.Views.ListView< T >", "class_common_1_1_collections_1_1_views_1_1_list_view.html", null ]
      ] ]
    ] ],
    [ "Dialog.Agency.State.ImmutableState", "interface_dialog_1_1_agency_1_1_state_1_1_immutable_state.html", [
      [ "Dialog.Agency.State.MutableState", "interface_dialog_1_1_agency_1_1_state_1_1_mutable_state.html", [
        [ "Dialog.Agency.State.InformationState", "class_dialog_1_1_agency_1_1_state_1_1_information_state.html", null ]
      ] ]
    ] ],
    [ "Dialog.SPB.Timing_Conflicts.InterruptionRules", "class_dialog_1_1_s_p_b_1_1_timing___conflicts_1_1_interruption_rules.html", null ],
    [ "Dialog.SPB.Program.Node", "class_dialog_1_1_s_p_b_1_1_program_1_1_node.html", [
      [ "Dialog.SPB.Program.Conditional", "class_dialog_1_1_s_p_b_1_1_program_1_1_conditional.html", null ],
      [ "Dialog.SPB.Program.Conjunction", "class_dialog_1_1_s_p_b_1_1_program_1_1_conjunction.html", null ],
      [ "Dialog.SPB.Program.Disjunction", "class_dialog_1_1_s_p_b_1_1_program_1_1_disjunction.html", null ],
      [ "Dialog.SPB.Program.Divergence", "class_dialog_1_1_s_p_b_1_1_program_1_1_divergence.html", null ],
      [ "Dialog.SPB.Program.EventExpectation", "class_dialog_1_1_s_p_b_1_1_program_1_1_event_expectation.html", null ],
      [ "Dialog.SPB.Program.Repeat", "class_dialog_1_1_s_p_b_1_1_program_1_1_repeat.html", null ],
      [ "Dialog.SPB.Program.Sequence", "class_dialog_1_1_s_p_b_1_1_program_1_1_sequence.html", null ]
    ] ],
    [ "Common.DesignPatterns.ObjectBuilder< T >", "class_common_1_1_design_patterns_1_1_object_builder.html", null ],
    [ "Common.DesignPatterns.ObjectBuilder< InformationState >", "class_common_1_1_design_patterns_1_1_object_builder.html", [
      [ "Dialog.Agency.State.InformationState.Builder", "class_dialog_1_1_agency_1_1_state_1_1_information_state_1_1_builder.html", null ]
    ] ],
    [ "Common.DesignPatterns.ObjectBuilder< ModuleBundle >", "class_common_1_1_design_patterns_1_1_object_builder.html", [
      [ "Dialog.Agency.System.ModuleBundle.Builder", "class_dialog_1_1_agency_1_1_system_1_1_module_bundle_1_1_builder.html", null ]
    ] ],
    [ "Dialog.Agency.Modules.OperationModule", "interface_dialog_1_1_agency_1_1_modules_1_1_operation_module.html", [
      [ "Dialog.Agency.Modules.OperationModule< TState >", "class_dialog_1_1_agency_1_1_modules_1_1_operation_module.html", null ]
    ] ],
    [ "Dialog.Agency.Modules.OperationModule< ImmutableState >", "interface_dialog_1_1_agency_1_1_modules_1_1_operation_module.html", [
      [ "Dialog.Agency.Modules.Action.ActionRealizationModule", "class_dialog_1_1_agency_1_1_modules_1_1_action_1_1_action_realization_module.html", [
        [ "Dialog.Agency.Modules.Action.ActionRealizationModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_action_1_1_action_realization_module_stub.html", null ]
      ] ],
      [ "Dialog.Agency.Modules.Action.ActionTimingModule", "class_dialog_1_1_agency_1_1_modules_1_1_action_1_1_action_timing_module.html", [
        [ "Dialog.Agency.Modules.Action.ActionTimingModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_action_1_1_action_timing_module_stub.html", null ],
        [ "Dialog.SPB.Modules.SPBATModule", "class_dialog_1_1_s_p_b_1_1_modules_1_1_s_p_b_a_t_module.html", null ]
      ] ],
      [ "Dialog.Agency.Modules.Deliberation.ActionSelectionModule", "class_dialog_1_1_agency_1_1_modules_1_1_deliberation_1_1_action_selection_module.html", [
        [ "Dialog.Agency.Modules.Deliberation.ActionSelectionModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_deliberation_1_1_action_selection_module_stub.html", null ],
        [ "Dialog.SPB.Modules.SPBASModule", "class_dialog_1_1_s_p_b_1_1_modules_1_1_s_p_b_a_s_module.html", null ]
      ] ],
      [ "Dialog.Agency.Modules.Perception.CurrentActivityPerceptionModule", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_current_activity_perception_module.html", [
        [ "Dialog.Agency.Modules.Perception.CurrentActivityPerceptionModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_current_activity_perception_module_stub.html", null ]
      ] ],
      [ "Dialog.Agency.Modules.Perception.RecentActivityPerceptionModule", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_recent_activity_perception_module.html", [
        [ "Dialog.Agency.Modules.Perception.RecentActivityPerceptionModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_recent_activity_perception_module_stub.html", null ]
      ] ]
    ] ],
    [ "Dialog.Agency.Modules.OperationModule< MutableState >", "interface_dialog_1_1_agency_1_1_modules_1_1_operation_module.html", [
      [ "Dialog.Agency.Modules.Deliberation.StateUpdateModule", "class_dialog_1_1_agency_1_1_modules_1_1_deliberation_1_1_state_update_module.html", [
        [ "Dialog.Agency.Modules.Deliberation.StateUpdateModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_deliberation_1_1_state_update_module_stub.html", null ]
      ] ]
    ] ],
    [ "Common.Functional.Options.Option< T >", "interface_common_1_1_functional_1_1_options_1_1_option.html", [
      [ "Common.Functional.Options.None< T >", "struct_common_1_1_functional_1_1_options_1_1_none.html", null ],
      [ "Common.Functional.Options.Some< T >", "struct_common_1_1_functional_1_1_options_1_1_some.html", null ]
    ] ],
    [ "Common.Functional.Options.Option< int >", "interface_common_1_1_functional_1_1_options_1_1_option.html", null ],
    [ "Common.Collections.Pair< TFirst, TSecond >", "struct_common_1_1_collections_1_1_pair.html", null ],
    [ "Dialog.Agency.Modules.Perception.RecentActivityReport", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_recent_activity_report.html", null ],
    [ "Dialog.SPB.Rules.Rule< T >", "class_dialog_1_1_s_p_b_1_1_rules_1_1_rule.html", null ],
    [ "Dialog.SPB.State.SocialContext", "class_dialog_1_1_s_p_b_1_1_state_1_1_social_context.html", null ],
    [ "Dialog.Agency.State.StateBundle                    < T_RAP_State, T_CAP_State, T_SU_State, T_AS_State, T_AT_State, T_AR_State >", "class_dialog_1_1_agency_1_1_state_1_1_state_bundle_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01.html", null ],
    [ "Dialog.Agency.State.StateComponent", "interface_dialog_1_1_agency_1_1_state_1_1_state_component.html", [
      [ "Dialog.Agency.State.StateComponent< T >", "class_dialog_1_1_agency_1_1_state_1_1_state_component.html", null ]
    ] ],
    [ "Dialog.Agency.System.SystemActivitySnapshot", "class_dialog_1_1_agency_1_1_system_1_1_system_activity_snapshot.html", null ],
    [ "Common.Time.Timer", "class_common_1_1_time_1_1_timer.html", null ]
];