﻿using Dialog.Agency.Scene;
using Dialog.Communication.Actors;
using Dialog.Communication.Management;
using Dialog.Communication.Packets;
using Example.CouplesTherapy.Agency.Scene;
using Example.CouplesTherapy.Communication;
using Example.CouplesTherapy.Simulation;
using Example.CouplesTherapy.Simulation.Actor_Controllers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

/// <summary>
/// This behavior manages the virtual session.
/// </summary>
public sealed class ScenarioDirector
    : SingletonMonoBehaviour<ScenarioDirector>
{
    public string therapistName = "Charles";
    public string firstPatientName = "Alice";
    public string secondPatientName = "Bob";

    public SpeechBubble therapistBubble;
    public SpeechBubble firstPatientBubble;
    public SpeechBubble secondPatientBubble;

    /// <summary>
    /// Gets the therapist actor controller.
    /// </summary>
    public TherapistController TherapistController
    {
        get { return _scenario.TherapistController; }
    }

    void Awake()
    {
        InitializeSingleton();

        var session = Session.Create
        (
            therapistName, 
            firstPatientName, secondPatientName
        );
        _scenario = new Scenario(session);

        var eavesdropper = new Eavesdropper();
        var subscription = _scenario.CommunicationManager.Engage(eavesdropper);
        subscription.Include<Speech>(
            (manager, packets) => OnPerceiveSpeech(packets));
        subscription.Include<MoveEvent>(
            (manager, packets) => OnPerceiveMoveEvent(packets));

        _speech_bubble_for_actor.Add(session.Patients.First, firstPatientBubble);
        _speech_bubble_for_actor.Add(session.Patients.Second, secondPatientBubble);
        _speech_bubble_for_actor.Add(session.Therapist, therapistBubble);

        foreach (var actor in session.Actors)
        {
            _expecting_new_phrase_for_actor.Add(actor, true);
        }
    }

    void Start()
    {
        foreach (var bubble in SpeechBubbles)
        {
            bubble.ClearDisplay();
        }
    }

    private void FixedUpdate()
    {
        if (TimeControl.IsPaused) { return; }

        _scenario.Step(Time.fixedDeltaTime);
        _new_perceptions_available = true;
    }
    void Update()
    {
        if (TimeControl.IsPaused) { return; }
        if (_scenario.HasEnded())
        {
            KeyboardInputHandler.Instance.IsEnabled = false;

            PauseMenu.Instance.Title = "THE END";
            PauseMenu.Instance.IsResumeButtonActive = false;

            TimeControl.Pause();

            StartCoroutine(OnScenarioEnd());
            
            return;
        }
        if (!_new_perceptions_available) { return; }

        foreach (var speech in _perceived_speech)
        {
            var speaker = speech.Speaker;
            SpeechBubble bubble = _speech_bubble_for_actor[speaker];
            
            if (_expecting_new_phrase_for_actor[speaker])
            {
                bubble.ClearDisplay();
                bubble.AddToDisplay(speech.Text);
                
                // We will expect a new phrase once we observe the dialog move
                // event associated with the current one, but for now we have 
                // no such expectation:
                _expecting_new_phrase_for_actor[speaker] = false;
            }
            else
            {
                bubble.AddToDisplay(" " + speech.Text);
            }
        }
        foreach (var @event in _perceived_move_events)
        {
            DevDebug.Log("Dialog move realization event: " + @event);

            _expecting_new_phrase_for_actor[@event.Source] = true;
        }
        _new_perceptions_available = false;
    }

    /// <summary>
    /// A dummy actor so we can listen in on the conversation.
    /// </summary>
    private sealed class Eavesdropper
        : ManagedActor
    {
        public void Act(CommunicationManager.DataSubmission submission) { }
    }

    private IEnumerable<SpeechBubble> SpeechBubbles
    {
        get { return _speech_bubble_for_actor.Values; }
    }
    
    private void OnPerceiveSpeech(IEnumerable<DataPacket<Speech>> packets)
    {
        _perceived_speech = 
            packets.Select(packet => packet.Payload).ToList();
    }
    private void 
        OnPerceiveMoveEvent(IEnumerable<DataPacket<MoveEvent>> packets)
    {
        _perceived_move_events = 
            packets.Select(packet => packet.Payload).ToList();
    }

    private IEnumerator OnScenarioEnd()
    {
        yield return new WaitForSeconds(1);

        PauseMenu.Instance.Show();
    }

    private Scenario _scenario;
    private bool _new_perceptions_available = false;
    private IEnumerable<Speech> _perceived_speech = new Speech[0];
    private IEnumerable<MoveEvent> _perceived_move_events = new MoveEvent[0];

    private readonly 
        Dictionary<Actor, SpeechBubble> 
        _speech_bubble_for_actor =
        new Dictionary<Actor, SpeechBubble>();
    private readonly
        Dictionary<Actor, bool>
        _expecting_new_phrase_for_actor =
        new Dictionary<Actor, bool>();
}
