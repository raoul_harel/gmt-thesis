﻿using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.System;
using Dialog.SPB.State;
using Example.CouplesTherapy.Agency.Modules.Action;
using Example.CouplesTherapy.Agency.Modules.Deliberation;
using Example.CouplesTherapy.Agency.Modules.Perception;
using Example.CouplesTherapy.Agency.Scene;
using Example.CouplesTherapy.Agency.State;
using Example.CouplesTherapy.Social_Practices;
using System;
using System.Collections.Generic;
using System.Linq;
using static Dialog.Communication.Management.CommunicationManager;
using DataSubmission = Dialog.Communication.Management.CommunicationManager.DataSubmission;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Example.CouplesTherapy.Simulation.Actor_Controllers
{
    /// <summary>
    /// This class represents user controlled therapist actor.
    /// </summary>
    public sealed class TherapistController
    {
        /// <summary>
        /// Creates a new controller for the specified actor.
        /// </summary>
        /// <param name="therapist">The actor to control.</param>
        /// <param name="scenario">
        /// The scenario the therapist is part of.
        /// </param>
        /// <remarks>
        /// <paramref name="therapist"/> must not be null.
        /// <paramref name="therapist"/> must belong to 
        /// <paramref name="scenario"/>
        /// <paramref name="scenario"/> must not be null.
        /// </remarks>
        public TherapistController(Therapist therapist, Scenario scenario)
        {
            Require.IsNotNull(therapist);
            Require.IsNotNull(scenario);
            Require.AreSame(therapist, scenario.Session.Therapist);

            Therapist = therapist;
            Scenario = scenario;

            _data_subscription = 
                Scenario.CommunicationManager.Engage(Therapist);
            _data_subscription
                .Include<MoveEvent>((manager, packets) =>
            {
                OnPerceiveMoveEventData
                (
                    packets.Select(packet => packet.Payload)
                );
            });

            Therapist.Action = 
                Option.CreateSome<Action<DataSubmission>>(Act);

            _rap_module = new VHRAPModule();
            _as_module = new ManualASModule();
            _ar_module = new VHARModule();
            var modules = new ModuleBundle.Builder()
                .WithRecentActivityPerceptionBy(_rap_module)
                .WithStateUpdateBy(new VHSUModule())
                .WithActionSelectionBy(_as_module)
                .WithActionRealizationBy(_ar_module)
                .Build();
            var state = ScenarioInformationState.Create
            (
                Scenario.Clock,
                new SocialContext
                (
                    Therapist,
                    ScenarioSocialPracticeProgram.Create(Scenario.Session)
                )
            );
            AgencySystem = new AgencySystem(state, modules);
        }

        /// <summary>
        /// Gets the scenario the controlled actor is part of.
        /// </summary>
        public Scenario Scenario { get; private set; }
        /// <summary>
        /// Gets the controlled actor.
        /// </summary>
        public Therapist Therapist { get; private set; }
        /// <summary>
        /// Gets the actor's agency system.
        /// </summary>
        public AgencySystem AgencySystem { get; private set; }

        /// <summary>
        /// Gets the suggested dialog moves to make at this time.
        /// </summary>
        /// <returns>An enumeration of dialog moves.</returns>
        public IEnumerable<DialogMove> GetSuggestedMoves()
        {
            return AgencySystem
                .State.Get<SocialContext>()
                .Program.FindExpectedEvents(
                    @event => @event.Source == Therapist)
                .Select(
                    @event => @event.Move);
        }
        /// <summary>
        /// Sets the target move to perform.
        /// </summary>
        /// <param name="move">The move to perform.</param>
        /// <remarks>
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public void SetTargetMove(DialogMove move)
        {
            Require.IsNotNull(move);

            _as_module.TargetMove = move;
        }

        /// <summary>
        /// This handles perceived <see cref="MoveEvent"/> data delivered by 
        /// the scenario's communication manager.
        /// </summary>
        /// <param name="data">The received speech data.</param>
        private void OnPerceiveMoveEventData(IEnumerable<MoveEvent> data)
        {
            _rap_module.PerceiveDialogMoves(data);
        }

        /// <summary>
        /// Performs action in the name of the controlled actor.
        /// </summary>
        /// <param name="submission">
        /// The object through which to submit output behavior.
        /// </param>
        /// <remarks>
        /// <paramref name="submission"/> must not be null.
        /// </remarks>
        private void Act(DataSubmission submission)
        {
            AgencySystem.Step();

            _ar_module.OutputSpeech.IfIsSome(speech =>
            {
                submission.Add(speech);
            });
            _ar_module.OutputMove.IfIsSome(move =>
            {
                submission.Add(new MoveEvent(Therapist, move));
                SetTargetMove(IdleMove.Instance);
            });
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(TherapistController)}(" +
                   $"{nameof(Therapist)} = {Therapist})";
        }

        private readonly DataSubscription _data_subscription;

        private readonly VHRAPModule _rap_module;
        private readonly ManualASModule _as_module;
        private readonly VHARModule _ar_module;
    }
}
