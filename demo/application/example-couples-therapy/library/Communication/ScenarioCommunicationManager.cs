﻿using Dialog.Agency.Modules.Perception;
using Dialog.Communication.Management;

namespace Example.CouplesTherapy.Communication
{
    /// <summary>
    /// This class is responsible for instantiating communication managers
    /// for the scenario.
    /// </summary>
    public static class ScenarioCommunicationManager
    {
        /// <summary>
        /// Creates a new communication manager for the couples therapy 
        /// scenario.
        /// </summary>
        /// <returns>
        /// A new communication manager supporting two channels: one for 
        /// realized dialog move events, and the other for string data.
        /// </returns>
        public static CommunicationManager Create()
        {
            return new CommunicationManager.Builder()
                .Support<DialogMoveRealizationEvent>()
                .Support<Speech>()
                .Build();
        }
    }
}
