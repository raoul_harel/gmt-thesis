﻿using Common.Hashing;
using Common.Validation;
using Example.CouplesTherapy.Agency.Scene;

namespace Example.CouplesTherapy.Communication
{
    /// <summary>
    /// Associates a string of text with a virtual human.
    /// </summary>
    public sealed class Speech
    {
        /// <summary>
        /// Creates a new speech by the specified speaker and made up of the
        /// specified text.
        /// </summary>
        /// <param name="speaker">The virtual human speaking.</param>
        /// <param name="text">The speech text.</param>
        /// <remarks>
        /// <paramref name="speaker"/> must not be null.
        /// <paramref name="text"/> must not be blank.
        /// </remarks>
        public Speech(VirtualHuman speaker, string text)
        {
            Require.IsNotNull(speaker);
            Require.IsNotBlank(text);

            Speaker = speaker;
            Text = text;
        }

        /// <summary>
        /// Gets the speaker.
        /// </summary>
        public VirtualHuman Speaker { get; private set; }
        /// <summary>
        /// Gets the speech text.
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as Speech;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.Speaker.Equals(Speaker) &&
                   other.Text.Equals(Text);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, Speaker);
            hash = HashCombiner.Hash(hash, Text);

            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Speech)}(" +
                   $"{nameof(Speaker)} = {Speaker}, " +
                   $"{nameof(Text)} = '{Text}')";
        }
    }
}
