﻿using Common.Collections.Extensions;
using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Randomization.Distributions;
using Common.Time;
using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Dialog_Moves.Extensions;
using Dialog.Agency.Modules.Action;
using Dialog.SPB.State;
using Example.CouplesTherapy.Agency.Scene;
using Example.CouplesTherapy.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using MoveKind = Example.CouplesTherapy.Agency.Dialog_Moves.ScenarioDialogMoveKind;

namespace Example.CouplesTherapy.Agency.Modules.Action
{
    /// <summary>
    /// This class is a stub implementation of the 
    /// <see cref="ActionRealizationModule"/>.
    /// </summary>
    public sealed class VHARModule
        : ActionRealizationModule
    {
        /// <summary>
        /// Creates a new module with the specified output rate.
        /// </summary>
        /// <param name="output_rate">
        /// The time it takes to output a single character of text.
        /// </param>
        /// <remarks>
        /// <paramref name="output_rate"/> must be >= 0.
        /// </remarks>
        public VHARModule(float output_rate = 0.05f)
        {
            Require.IsAtLeast(output_rate, 0);

            OutputSpeechRate = output_rate;
        }

        /// <summary>
        /// The time it takes to output a single character.
        /// </summary>
        public float OutputSpeechRate { get; private set; }

        /// <summary>
        /// Gets the actor in whose name this module outputs speech.
        /// </summary>
        public VirtualHuman Owner { get; private set; }
        /// <summary>
        /// Gets this module's time reference point.
        /// </summary>
        public Clock Clock { get; private set; }

        /// <summary>
        /// Gets the move pending realization (if there is any).
        /// </summary>
        public Option<DialogMove<VirtualHuman>> PendingMove
        {
            get;
            private set;
        } 
            = Option.CreateNone<DialogMove<VirtualHuman>>();
        
        /// <summary>
        /// Gets this module's output speech, if it has any.
        /// </summary>
        public Option<Speech> OutputSpeech
        {
            get;
            private set;
        } 
            = Option.CreateNone<Speech>();

        /// <summary>
        /// Gets this module's output move, if it has any.
        /// </summary>
        public Option<DialogMove<VirtualHuman>> OutputMove
        {
            get;
            private set;
        } 
            = Option.CreateNone<DialogMove<VirtualHuman>>();

        /// <summary>
        /// Gets the collection of data types required by this module to be 
        /// available through the information state.
        /// </summary>
        /// <param name="types">The collection of types.</param>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override
            void GetRequiredStateComponents(ICollection<Type> types)
        {
            types.Add(typeof(SocialContext));
            types.Add(typeof(Clock));
        }
        /// <summary>
        /// Sets up this module for operation.
        /// </summary>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override void Setup()
        {
            base.Setup();

            Owner = (VirtualHuman) State.Get<SocialContext>().Self;

            Clock = State.Get<Clock>();
            _action_delay_timer = new Timer(Clock);
            _speech_timer = new Timer(Clock);
        }

        /// <summary>
        /// Realizes the specified dialog move.
        /// </summary>
        /// <param name="move">The dialog move to realize.</param>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        /// <remarks>
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public override ActionRealizationStatus RealizeMove(DialogMove move)
        {
            Require.IsNotNull(move);

            OutputSpeech = Option.CreateNone<Speech>();
            OutputMove = Option.CreateNone<DialogMove<VirtualHuman>>();

            if (move.IsIdle())
            {
                PendingMove = Option.CreateNone<DialogMove<VirtualHuman>>();

                return ActionRealizationStatus.Complete;
            }

            var target_move = move.Cast<VirtualHuman>();
            if (!PendingMove.Contains(target_move))
            {
                return BeginRealization(target_move);
            }
            else
            {
                return ResumePendingRealization();
            }
        }

        /// <summary>
        /// Gets the word pending realization (only applicable if there is some
        /// move pending realization).
        /// </summary>
        public Option<string> PendingWord
        {
            get
            {
                if (_pending_words.Count > 0)
                {
                    return Option.CreateSome(_pending_words.Peek());
                }
                else
                {
                    return Option.CreateNone<string>();
                }
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(VHARModule)}(" +
                   $"{nameof(State)} = {State})";
        }

        /// <summary>
        /// Begin a new move realization.
        /// </summary>
        /// <param name="target_move">The move to realize.</param>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        private ActionRealizationStatus 
            BeginRealization(DialogMove<VirtualHuman> target_move)
        {
            _pending_words.Clear();
            _action_delay_timer.Reset();

            var text = ExpressThroughText(target_move);
            var words = text.Split(' ');
            foreach (var word in words)
            {
                _pending_words.Enqueue(word);
            }

            PendingMove = Option.CreateSome(target_move);
            SetRandomActionDelay();

            return ActionRealizationStatus.InProgress;
        }
        /// <summary>
        /// Resumes the currently pending move realization.
        /// </summary>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        private ActionRealizationStatus ResumePendingRealization()
        {
            if (_action_delay_timer.Update())
            {
                _action_delay_timer.Reset();

                OutputWord();
            }
            else if (_speech_timer.Update())
            {
                _speech_timer.Reset();

                if (PendingWord.IsSome())
                {
                    OutputWord();
                }
                else
                {
                    OutputMove = PendingMove;
                    PendingMove = Option.CreateNone<DialogMove<VirtualHuman>>();

                    return ActionRealizationStatus.Complete;
                }
            }
            return ActionRealizationStatus.InProgress;
        }
        /// <summary>
        /// Outputs the currently pending word.
        /// </summary>
        private void OutputWord()
        {
            string word = _pending_words.Dequeue();
            var speech = new Speech(speaker: Owner, text: word);
            OutputSpeech = Option.CreateSome(speech);

            float output_duration = OutputSpeechRate * word.Length;
            _speech_timer.Set(output_duration);
            _speech_timer.Start();
        }

        /// <summary>
        /// Produces a string of speech text representing realization of the 
        /// specified dialog move.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressThroughText(DialogMove<VirtualHuman> move)
        {
            if (move.Kind == MoveKind.Greeting)
            {
                return ExpressGreetingThroughText(move);
            }
            else if (move.Kind == MoveKind.Goodbye)
            {
                return ExpressGoodbyeThroughText(move);
            }
            else if (move.Kind == MoveKind.IssueSharingInvitation)
            {
                return ExpressIssueSharingInvitationThroughText(move);
            }
            else if (move.Kind == MoveKind.IssueSharing)
            {
                return ExpressIssueSharingThroughText(move);
            }
            else if (move.Kind == MoveKind.IssueSharingDeclination)
            {
                return ExpressIssueSharingDeclineThroughText(move);
            }
            else if (move.Kind == MoveKind.AdviceDispensation)
            {
                return ExpressAdviceDispensationThroughText(move);
            }
            else if (move.Kind == MoveKind.SessionClosing)
            {
                return ExpressSessionClosingThroughText(move);
            }
            else if (move.Kind == MoveKind.Acknowledgement)
            {
                return ExpressAcknowledgementThroughText(move);
            }
            else
            {
                throw new ArgumentException
                (
                    "Move is of an unknown kind and cannot be expressed " +
                    "through text."
                );
            }
        }
        /// <summary>
        /// Produces a string of speech text representing realization of a
        /// greeting.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string 
            ExpressGreetingThroughText(DialogMove<VirtualHuman> move)
        {
            Require.AreSame(move.Kind, MoveKind.Greeting);

            if (move.Targets.IsEmpty())
            {
                return "Hello.";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Hello {string.Join(", ", names)}.";
            }
        }
        /// <summary>
        /// Produces a string of speech text representing realization of a
        /// goodbye.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string 
            ExpressGoodbyeThroughText(DialogMove<VirtualHuman> move)
        {
            Require.AreSame(move.Kind, MoveKind.Goodbye);

            if (move.Targets.IsEmpty())
            {
                return "Goodbye.";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Goodbye {string.Join(", ", names)}.";
            }
        }

        /// <summary>
        /// Produces a string of speech text representing realization of an
        /// invitation to share an issue.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressIssueSharingInvitationThroughText
        (
            DialogMove<VirtualHuman> move
        )
        {
            Require.AreSame(move.Kind, MoveKind.IssueSharingInvitation);
            Require.IsTrue(move.Target.IsSome());

            return $"Would you like to share an issue with us, " +
                   $"{move.Target.GetValue().Name}?";
        }
        /// <summary>
        /// Produces a string of speech text representing realization of the
        /// sharing of an issue.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressIssueSharingThroughText
        (
            DialogMove<VirtualHuman> move
        )
        {
            Require.AreSame(move.Kind, MoveKind.IssueSharing);

            return
                "I love my partner, but sometimes even little things can get irritating. " +
                "For example, often times it feels as if my partner imitates what I " +
                "say just to get on my nerves!";
        }
        /// <summary>
        /// Produces a string of speech text representing realization of the
        /// decline to share an issue.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressIssueSharingDeclineThroughText
        (
            DialogMove<VirtualHuman> move
        )
        {
            Require.AreSame(move.Kind, MoveKind.IssueSharingDeclination);

            return "I don't have anything more I wish to discuss.";
        }
        /// <summary>
        /// Produces a string of speech text representing realization of the
        /// dispense of advice.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressAdviceDispensationThroughText
        (
            DialogMove<VirtualHuman> move
        )
        {
            Require.AreSame(move.Kind, MoveKind.AdviceDispensation);

            return 
                "The solution to your problem is simple: " + 
                "I think all you need to do is communicate more.";
        }
        /// <summary>
        /// Produces a string of speech text representing realization of a
        /// session's closing remarks.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressSessionClosingThroughText
        (
            DialogMove<VirtualHuman> move
        )
        {
            Require.AreSame(move.Kind, MoveKind.SessionClosing);

            return 
                "Well, we have made some good progress today, but for now I'm " +
                "afraid our time is up.";
        }
        /// <summary>
        /// Produces a string of speech text representing realization of an
        /// acknowledgement.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressAcknowledgementThroughText
        (
            DialogMove<VirtualHuman> move
        )
        {
            Require.AreSame(move.Kind, MoveKind.Acknowledgement);

            return "Alright.";
        }

        /// <summary>
        /// Sets the action delay timer to a random value in order to mimic
        /// human reaction time.
        /// </summary>
        private void SetRandomActionDelay()
        {
            float duration = (float) UniformDistribution.Sample(1.0, 1.5);
            _action_delay_timer.Set(duration);
            _action_delay_timer.Start();
        }

        private readonly Queue<string> _pending_words = new Queue<string>();

        private Timer _action_delay_timer;
        private Timer _speech_timer;
    }
}
