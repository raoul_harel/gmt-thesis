﻿using Common.Time;
using Common.Validation;
using Dialog.Agency.Modules.Perception;
using Example.CouplesTherapy.Agency.Scene;
using Example.CouplesTherapy.Communication;
using System;
using System.Collections.Generic;

namespace Example.CouplesTherapy.Agency.Modules.Perception
{
    /// <summary>
    /// This class is an implementation of the 
    /// <see cref="CurrentActivityPerceptionModule"/> for the scenario's 
    /// virtual humans.
    /// </summary>
    public sealed class VHCAPModule
        : CurrentActivityPerceptionModule
    {
        /// <summary>
        /// Creates a new module operating with the specified grace period.
        /// See <see cref="GracePeriod"/> for details.
        /// </summary>
        /// <param name="grace_period">The grace period to assume.</param>
        /// <remarks>
        /// <paramref name="grace_period"/> must be >= 0.
        /// </remarks>
        public VHCAPModule(float grace_period = 1.0f)
        {
            Require.IsAtLeast(grace_period, 0);

            GracePeriod = grace_period;
        }

        /// <summary>
        /// Gets this module's time reference point.
        /// </summary>
        public Clock Clock { get; private set; }
        /// <summary>
        /// Gets the grace period duration. Actors are still being considered
        /// active up until the end of the grace period after their latest
        /// speech.
        /// </summary>
        public float GracePeriod { get; private set; }

        /// <summary>
        /// Gets the collection of data types required by this module to be 
        /// available through the information state.
        /// </summary>
        /// <param name="types">The collection of types.</param>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override
            void GetRequiredStateComponents(ICollection<Type> types)
        {
            types.Add(typeof(Clock));
        }
        /// <summary>
        /// Sets up this module for operation.
        /// </summary>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override void Setup()
        {
            base.Setup();

            Clock = State.Get<Clock>();
        }
        /// <summary>
        /// Perceives ongoing speech.
        /// </summary>
        /// <param name="data">This iteration's speech data.</param>
        /// <remarks>
        /// <paramref name="data"/> must not be null.
        /// </remarks>
        public void PerceiveSpeech(IEnumerable<Speech> data)
        {
            Require.IsNotNull(data);

            foreach (var speech in data)
            {
                _latest_speech_time_by_actor[speech.Speaker] = Clock.Time;
            }
        }

        /// <summary>
        /// Actors are marked as active if they have spoken in the last 
        /// few seconds, otherwise they are marked as passive.
        /// </summary>
        /// <param name="report">The report to fill in.</param>
        /// <remarks>
        /// <paramref name="report"/> must not be null.
        /// </remarks>
        protected override void ComposeActivityReport
        (
            CurrentActivityReport report
        )
        {
            Require.IsNotNull(report);
            
            foreach (var actor in _latest_speech_time_by_actor.Keys)
            {
                var latest_speech_time = _latest_speech_time_by_actor[actor];

                if (Clock.Time - latest_speech_time <= GracePeriod)
                {
                    report.AddAsActive(actor);
                }
                else
                {
                    report.AddAsPassive(actor);
                }
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(VHCAPModule)}(" +
                   $"{nameof(GracePeriod)} = {GracePeriod}, " +
                   $"{nameof(State)} = {State})";
        }

        private readonly 
            Dictionary<VirtualHuman, float> 
            _latest_speech_time_by_actor = 
            new Dictionary<VirtualHuman, float>();
    }
}
