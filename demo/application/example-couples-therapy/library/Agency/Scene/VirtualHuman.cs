﻿using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Validation;
using Dialog.Agency.Scene;
using Dialog.Communication.Actors;
using Dialog.Communication.Management;
using System;

namespace Example.CouplesTherapy.Agency.Scene
{
    /// <summary>
    /// Abstract class representing a virtual human.
    /// </summary>
    public abstract class VirtualHuman
        : Actor,
          ManagedActor
    {
        /// <summary>
        /// Creates a new virtual human.
        /// </summary>
        /// <param name="name">The actor's name.</param>
        /// <remarks>
        /// <paramref name="name"/> must not be blank.
        /// </remarks>
        protected VirtualHuman(string name) 
            : base()
        {
            Require.IsNotBlank(name);

            Name = name;
        }

        /// <summary>
        /// Gets the actor's name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets/sets the action handler for this actor.
        /// </summary>
        public Option<Action<CommunicationManager.DataSubmission>> Action
        {
            get;
            set;
        }
        /// <summary>
        /// Forwards action handling to the delegate specified by 
        /// <see cref="Action"/>, if there is any.
        /// </summary>
        /// <param name="submission">The data submission to use.</param>
        public void Act(CommunicationManager.DataSubmission submission)
        {
            Action.IfIsSome(@delegate => @delegate.Invoke(submission));
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(VirtualHuman)}(" +
                   $"{nameof(Name)} = '{Name}')";
        }
    }
}
