﻿using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Validation;

namespace Example.CouplesTherapy.Agency.Scene
{
    /// <summary>
    /// The patient class represents an actor being treated during the therapy 
    /// session.
    /// </summary>
    public sealed class Patient
        : VirtualHuman
    {
        /// <summary>
        /// Creates a new patient.
        /// </summary>
        /// <param name="name">The actor's name.</param>
        /// <remarks>
        /// <paramref name="name"/> must not be blank.
        /// </remarks>
        public Patient(string name)
            : base(name)
        { }

        /// <summary>
        /// Gets this patient's partner.
        /// </summary>
        public Option<Patient> Partner { get; private set; } =
            Option.CreateNone<Patient>();

        /// <summary>
        /// Static methods for the query and manipulation of relationships
        /// between patients.
        /// </summary>
        public static class RelationshipManager
        {
            /// <summary>
            /// Creates a couple from the specified patients.
            /// </summary>
            /// <param name="first">The first patient.</param>
            /// <param name="second">The second patient.</param>
            /// <remarks>
            /// <paramref name="first"/> must not be null.
            /// <paramref name="second"/> must not be null.
            /// <paramref name="first"/> must not be in a relationship with 
            /// <paramref name="second"/>.
            /// Any previous relationships are broken up.
            /// </remarks>
            public static void CreateCouple(Patient first, Patient second)
            {
                Require.IsNotNull(first);
                Require.IsNotNull(second);
                Require.IsFalse(AreTogether(first, second));

                first.Partner.IfIsSome(actor => Breakup(first, actor));
                second.Partner.IfIsSome(actor => Breakup(second, actor));

                first.Partner = Option.CreateSome(second);
                second.Partner = Option.CreateSome(first);
            }
            /// <summary>
            /// Breakups the specified couple.
            /// </summary>
            /// <param name="first">The first patient.</param>
            /// <param name="second">The second patient.</param>
            /// <remarks>
            /// <paramref name="first"/> must not be null.
            /// <paramref name="second"/> must not be null.
            /// <paramref name="first"/> must be in a relationship with 
            /// <paramref name="second"/>.
            /// </remarks>
            public static void Breakup(Patient first, Patient second)
            {
                Require.IsNotNull(first);
                Require.IsNotNull(second);
                Require.IsTrue(AreTogether(first, second));

                first.Partner = Option.CreateNone<Patient>();
                second.Partner = Option.CreateNone<Patient>();
            }
            /// <summary>
            /// Determines whether the specified patient pair are a couple.
            /// </summary>
            /// <param name="first">The first patient.</param>
            /// <param name="second">The second patient.</param>
            /// <remarks>
            /// <paramref name="first"/> must not be null.
            /// <paramref name="second"/> must not be null.
            /// </remarks>
            /// <returns>
            /// True if the specified patients are a couple, otherwise false.
            /// </returns>
            public static bool AreTogether(Patient first, Patient second)
            {
                Require.IsNotNull(first);
                Require.IsNotNull(second);

                return first.Partner.Contains(second) &&
                       second.Partner.Contains(first);
            }
        }
    }
}
