﻿using Common.Collections;
using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Validation;

namespace Example.CouplesTherapy.Agency.Scene
{
    /// <summary>
    /// The session class represents a couples-therapy session involving one
    /// therapist and a pair of patients.
    /// </summary>
    public class Session
    {
        /// <summary>
        /// Creates a new session with generated actors.
        /// </summary>
        /// <param name="therapist_name">
        /// The therapist's given name.
        /// </param>
        /// <param name="first_patient_name">
        /// The first patient's given name.
        /// </param>
        /// <param name="second_patient_name">
        /// The second patient's given name.
        /// </param>
        /// <returns>The newly created session instance.</returns>
        /// <remarks>
        /// <paramref name="therapist_name"/> must not be blank.
        /// <paramref name="first_patient_name"/> must not be blank.
        /// <paramref name="second_patient_name"/> must not be blank.
        /// </remarks>
        public static Session Create
        (
            string therapist_name,
            string first_patient_name,
            string second_patient_name
        )
        {
            Require.IsNotBlank(therapist_name);
            Require.IsNotBlank(first_patient_name);
            Require.IsNotBlank(second_patient_name);

            var therapist = new Therapist(therapist_name);
            var first_patient = new Patient(first_patient_name);
            var second_patient = new Patient(second_patient_name);

            Patient
                .RelationshipManager
                .CreateCouple(first_patient, second_patient);

            return new Session
            (
                therapist, 
                Pair.Create(first_patient, second_patient)
            );
        }
        /// <summary>
        /// Creates a new session using the specified actors.
        /// </summary>
        /// <param name="therapist">The therapist.</param>
        /// <param name="couple">The patient couple.</param>
        /// <remarks>
        /// <paramref name="therapist"/> must not be null.
        /// The patients of <paramref name="couple"/> must be in a relationship
        /// with each other.
        /// The <paramref name="couple"/> must contain two distinct patients.
        /// </remarks>
        public Session(Therapist therapist, Pair<Patient, Patient> couple)
        {
            Require.IsNotNull(therapist);
            Require.AreNotEqual(couple.First, couple.Second);
            Require.IsTrue
            (
                Patient
                    .RelationshipManager
                    .AreTogether(couple.First, couple.Second)
            );

            Therapist = therapist;
            Patients = couple;
            Actors = new CollectionView<VirtualHuman>
            (
                new VirtualHuman[3] 
                {
                    Therapist,
                    Patients.First,
                    Patients.Second
                }
            );
        }

        /// <summary>
        /// Gets the therapist.
        /// </summary>
        public Therapist Therapist { get; private set; }
        /// <summary>
        /// Gets the couple.
        /// </summary>
        public Pair<Patient, Patient> Patients { get; private set; }
        /// <summary>
        /// Gets a collection of all the actors.
        /// </summary>
        public ImmutableCollection<VirtualHuman> Actors { get; private set; }
        
        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Session)}(" +
                   $"{nameof(Therapist)} = {Therapist}, " +
                   $"{nameof(Patients)} = {Patients})";
        }
    }
}
