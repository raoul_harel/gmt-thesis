﻿namespace Example.CouplesTherapy.Agency.Scene
{
    /// <summary>
    /// The therapist class represents the medical practitioner treating a
    /// patient couple.
    /// </summary>
    public sealed class Therapist
        : VirtualHuman
    {
        /// <summary>
        /// Creates a new therapist.
        /// </summary>
        /// <param name="name">The actor's name.</param>
        /// <remarks>
        /// <paramref name="name"/> must not be blank.
        /// </remarks>
        public Therapist(string name)
            : base(name)
        { }
    }
}
