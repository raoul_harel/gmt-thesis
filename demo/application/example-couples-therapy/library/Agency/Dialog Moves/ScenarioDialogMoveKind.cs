﻿using Dialog.Agency.Dialog_Moves;

namespace Example.CouplesTherapy.Agency.Dialog_Moves
{
    /// <summary>
    /// Enumerates the scenario's dialog move kinds.
    /// </summary>
    public sealed class ScenarioDialogMoveKind
        : DialogMoveKind
    {
        /// <summary>
        /// Indicates a greeting from one actor to one or more others.
        /// </summary>
        public static readonly DialogMoveKind Greeting =
            new ScenarioDialogMoveKind("greeting");
        /// <summary>
        /// Indicates a goodbye from one actor to one or more others.
        /// </summary>
        public static readonly DialogMoveKind Goodbye =
            new ScenarioDialogMoveKind("goodbye");

        /// <summary>
        /// Indicates an invitation to share an issue.
        /// </summary>
        public static readonly DialogMoveKind IssueSharingInvitation =
            new ScenarioDialogMoveKind("invitation to share an issue");
        /// <summary>
        /// Indicates the sharing of an issue.
        /// </summary>
        public static readonly DialogMoveKind IssueSharing =
            new ScenarioDialogMoveKind("sharing of an issue");
        /// <summary>
        /// Indicates an decline to share an issue.
        /// </summary>
        public static readonly DialogMoveKind IssueSharingDeclination =
            new ScenarioDialogMoveKind("declination to share an issue");
        
        /// <summary>
        /// Indicates a dispension of advice.
        /// </summary>
        public static readonly DialogMoveKind AdviceDispensation =
            new ScenarioDialogMoveKind("dispensation of advice");
        
        /// <summary>
        /// Indicates the closing of the session.
        /// </summary>
        public static readonly DialogMoveKind SessionClosing =
            new ScenarioDialogMoveKind("closing of a session");

        /// <summary>
        /// Indicates an acknowledgement.
        /// </summary>
        public static readonly DialogMoveKind Acknowledgement =
            new ScenarioDialogMoveKind("acknowledgement");

        /// <summary>
        /// Creates a new dialog move kind.
        /// </summary>
        /// <param name="label">The label associated with this kind.</param>
        /// <remarks>
        /// <paramref name="label"/> must not be blank.
        /// </remarks>
        private ScenarioDialogMoveKind(string label)
            : base(label)
        { }
    }
}
