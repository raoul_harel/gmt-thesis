﻿using Common.Time;
using Common.Validation;
using Dialog.Agency.Modules.Perception;
using Dialog.Agency.State;
using Dialog.SPB.State;

namespace Example.CouplesTherapy.Agency.State
{
    /// <summary>
    /// This class is responsible for instantiating information state objects
    /// for the scenario.
    /// </summary>
    public static class ScenarioInformationState
    {
        /// <summary>
        /// Creates a new information state for the couples therapy 
        /// scenario.
        /// </summary>
        /// <param name="clock">A time reference point.</param>
        /// <param name="social_context">A social context.</param>
        /// <returns>
        /// A new information state.
        /// </returns>
        /// <remarks>
        /// <paramref name="clock"/> must not be null.
        /// <paramref name="social_context"/> must not be null.
        /// </remarks>
        public static InformationState Create
        (
            Clock clock,
            SocialContext social_context
        )
        {
            Require.IsNotNull(clock);
            Require.IsNotNull(social_context);

            return new InformationState.Builder()
                .Support(new CurrentActivityReport())
                .Support(social_context)
                .Support(clock)
                .Build();
        }
    }
}
