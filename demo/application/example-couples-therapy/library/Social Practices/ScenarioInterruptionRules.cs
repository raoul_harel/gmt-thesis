﻿using Dialog.SPB.Timing_Conflicts;
using System.Collections.Generic;

namespace Example.CouplesTherapy.Social_Practices
{
    /// <summary>
    /// This class is responsible for instantiating a mapping between nods of 
    /// the scneario's practice program and sets of interruption rules.
    /// </summary>
    public static class ScenarioInterruptionRules
    {
        /// <summary>
        /// Creates a new mapping.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, InterruptionRules> Create()
        {
            var result = new Dictionary<string, InterruptionRules>();

            result.Add("greetings", InterruptionRules.CONFLICT_INDIFFERENCE);
            result.Add("counseling", InterruptionRules.CONFLICT_AVOIDANCE);
            result.Add("end of counseling", InterruptionRules.CONFLICT_INDIFFERENCE);
            result.Add("goodbyes", InterruptionRules.CONFLICT_INDIFFERENCE);

            return result;
        }
    }
}
