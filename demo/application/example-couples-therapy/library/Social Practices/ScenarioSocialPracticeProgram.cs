﻿using Common.Validation;
using Dialog.SPB.Program;
using Example.CouplesTherapy.Agency.Scene;
using static Dialog.SPB.Program.NLI.ProgramBuildUtilities;
using static Example.CouplesTherapy.Agency.Dialog_Moves.ScenarioDialogMoveKind;

namespace Example.CouplesTherapy.Social_Practices
{
    /// <summary>
    /// This class is responsible for instantiating social practice programs
    /// for the scenario.
    /// </summary>
    public static class ScenarioSocialPracticeProgram
    {
        /// <summary>
        /// Creates a new program for the couples therapy scenario.
        /// </summary>
        /// <param name="session">
        /// The session for which to make the program.
        /// </param>
        /// <returns>
        /// A new social practice program.
        /// </returns>
        /// <remarks>
        /// <paramref name="session"/> must not be null.
        /// </remarks>
        public static Node Create(Session session)
        {
            Require.IsNotNull(session);

            VirtualHuman therapist = session.Therapist;
            var patients = session.Patients;

            return

            // ----- BEGIN PRACTICE DESCRIPTION ------ //

            ExpectSequence
            (
                "session",

                ExpectAll
                (
                    "greetings",  // from therapist to patients and vice versa

                    ExpectEvent(Greeting, source: therapist, target: patients.First),
                    ExpectEvent(Greeting, source: therapist, target: patients.Second),
                    ExpectEvent(Greeting, source: patients.First, target: therapist),
                    ExpectEvent(Greeting, source: patients.Second, target: therapist)
                ),
                ExpectAny
                (
                    "counseling",  // issue discussion or session ending

                    ExpectRepeat(ExpectOne
                    (
                        "issue discussion with either patient",

                        ExpectIssueDiscussion(therapist, patients.First),
                        ExpectIssueDiscussion(therapist, patients.Second)
                    )),
                    ExpectEvent(SessionClosing, source: therapist)
                ),
                ExpectAll
                (
                    "end of counseling",  // patients acknowledge end of session

                    ExpectEvent(Acknowledgement, source: patients.First),
                    ExpectEvent(Acknowledgement, source: patients.Second)
                ),
                ExpectAll
                (
                    "goodbyes",  // from therapist to patients and vice versa

                    ExpectEvent(Goodbye, source: therapist, target: patients.First),
                    ExpectEvent(Goodbye, source: therapist, target: patients.Second),
                    ExpectEvent(Goodbye, source: patients.First, target: therapist),
                    ExpectEvent(Goodbye, source: patients.Second, target: therapist)
                )
            );
            
        }

        private static Node ExpectIssueDiscussion
        (
            VirtualHuman therapist,
            VirtualHuman patient
        )
        {
            bool there_are_undisclosed_issues = true; 
            bool therapist_thinks_there_are_undisclosed_issues = true;

            return 

            ExpectIf(() => therapist_thinks_there_are_undisclosed_issues,
            ExpectSequence
            (
                $"therapist invites {patient.Name} to share",

                ExpectEvent(IssueSharingInvitation, source: therapist, target: patient),
                ExpectOne
                (
                    "patient either accepts or declines",

                    ExpectIf(() => there_are_undisclosed_issues, 
                    ExpectSequence
                    (
                        "patient shares an issue and the therapist comments",

                        ExpectEvent(Acknowledgement, source: patient),
                        ExpectEvent(IssueSharing, source: patient),
                        ExpectEvent(Acknowledgement, source: therapist),
                        ExpectEvent(AdviceDispensation, source: therapist),
                        ExpectEvent
                        (
                            Acknowledgement, source: patient,
                            satisfied: (_) => there_are_undisclosed_issues = false
                        )
                    )),
                    ExpectSequence
                    (
                        "patient declines to share and the therapist acknowledges",

                        ExpectEvent(IssueSharingDeclination, source: patient),
                        ExpectEvent
                        (
                            Acknowledgement, source: therapist,
                            satisfied: (_) => therapist_thinks_there_are_undisclosed_issues = false
                        )
                    )
                )
            ));
        }
    }
}
