﻿using NUnit.Framework;
using System;

namespace Example.CouplesTherapy.Agency.Scene.Test
{
    [TestFixture]
    public class TherapistTest
    {
        private static readonly string NAME = "Alice";

        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentException>
            (
                () => new Therapist(null)
            );
            Assert.Throws<ArgumentException>
            (
                () => new Therapist("")
            );
            Assert.Throws<ArgumentException>
            (
                () => new Therapist(" ")
            );
        }
        [Test]
        public void Test_Contructor()
        {
            var therapist = new Therapist(NAME);

            Assert.AreEqual(NAME, therapist.Name);
        }
    }
}
