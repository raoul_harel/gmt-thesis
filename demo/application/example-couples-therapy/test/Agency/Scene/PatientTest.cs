﻿using Common.Functional.Options.Extensions;
using NUnit.Framework;
using System;

namespace Example.CouplesTherapy.Agency.Scene.Test
{
    [TestFixture]
    public class PatientTest
    {
        private Patient _alice;
        private Patient _bob;

        [SetUp]
        public void Setup()
        {
            _alice = new Patient("Alice");
            _bob = new Patient("Bob");
        }

        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentException>
            (
                () => new Patient(null)
            );
            Assert.Throws<ArgumentException>
            (
                () => new Patient("")
            );
            Assert.Throws<ArgumentException>
            (
                () => new Patient(" ")
            );
        }
        [Test]
        public void Test_Contructor()
        {
            var patient = new Patient("Alice");

            Assert.AreEqual("Alice", patient.Name);
        }

        [Test]
        public void Test_InitialState()
        {
            Assert.IsFalse(_alice.Partner.IsSome());
        }

        [Test]
        public void Test_CoupleExistence_WhenNotTogether()
        {
            Assert.IsFalse
            (
                Patient
                    .RelationshipManager
                    .AreTogether(_alice, _bob)
            );
        }
        [Test]
        public void Test_CoupleExistence_WhenTogether()
        {
            Patient
                .RelationshipManager
                .CreateCouple(_alice, _bob);

            Assert.IsTrue
            (
                Patient
                    .RelationshipManager
                    .AreTogether(_alice, _bob)
            );
        }

        [Test]
        public void Test_CoupleCreation_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => 
                    Patient
                        .RelationshipManager
                        .CreateCouple(null, _bob)
            );
            Assert.Throws<ArgumentNullException>
            (
                () =>
                    Patient
                        .RelationshipManager
                        .CreateCouple(_alice, null)
            );
        }
        [Test]
        public void Test_CoupleCreation_WhenAlreadyTogether()
        {
            Patient
                .RelationshipManager
                .CreateCouple(_alice, _bob);

            Assert.Throws<ArgumentException>
            (
                () =>
                    Patient
                        .RelationshipManager
                        .CreateCouple(_alice, _bob)
            );
        }
        [Test]
        public void Test_CoupleCreation()
        {
            Patient
                .RelationshipManager
                .CreateCouple(_alice, _bob);

            Assert.IsTrue(_alice.Partner.Contains(_bob));
            Assert.IsTrue(_bob.Partner.Contains(_alice));
        }

        [Test]
        public void Test_CoupleBreakup_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () =>
                    Patient
                        .RelationshipManager
                        .Breakup(null, _bob)
            );
            Assert.Throws<ArgumentNullException>
            (
                () =>
                    Patient
                        .RelationshipManager
                        .Breakup(_alice, null)
            );
        }
        [Test]
        public void Test_CoupleBreakup_WhenAlreadySingle()
        {
            Assert.Throws<ArgumentException>
            (
                () =>
                    Patient
                        .RelationshipManager
                        .Breakup(_alice, _bob)
            );
        }
        [Test]
        public void Test_CoupleBreakup()
        {
            Patient
                .RelationshipManager
                .CreateCouple(_alice, _bob);
            Patient
                .RelationshipManager
                .Breakup(_alice, _bob);

            Assert.IsFalse(_alice.Partner.IsSome());
            Assert.IsFalse(_bob.Partner.IsSome());
        }
    }
}
