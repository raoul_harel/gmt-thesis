﻿using NUnit.Framework;
using System;

namespace Example.CouplesTherapy.Agency.Scene.Test
{
    [TestFixture]
    public class VirtualHumanTest
    {
        private static readonly string NAME = "Alice";

        private class MockVirtualHuman
            : VirtualHuman
        {
            public MockVirtualHuman(string name)
                : base(name)
            { }
        }

        private MockVirtualHuman _virtual_human;

        [SetUp]
        public void Setup()
        {
            _virtual_human = new MockVirtualHuman(NAME);
        }

        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentException>
            (
                () => new MockVirtualHuman(null)
            );
            Assert.Throws<ArgumentException>
            (
                () => new MockVirtualHuman("")
            );
            Assert.Throws<ArgumentException>
            (
                () => new MockVirtualHuman(" ")
            );
        }
        [Test]
        public void Test_Constructor()
        {
            var human = new MockVirtualHuman(NAME);

            Assert.AreEqual(NAME, human.Name);
        }
    }
}
