﻿using Common.Collections;
using NUnit.Framework;
using System;

namespace Example.CouplesTherapy.Agency.Scene.Test
{
    [TestFixture]
    public class SessionTest
    {
        private Session _session;

        [SetUp]
        public void Setup()
        {
            _session = Session.Create("Charles", "Alice", "Bob");
        }

        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            var alice = new Patient("Alice");
            var bob = new Patient("Bob");
            var alice_and_bob = Pair.Create(alice, bob);

            var cindy = new Patient("Cindy");
            var david = new Patient("David");
            var cindy_and_david = Pair.Create(cindy, david);

            Patient
                .RelationshipManager
                .CreateCouple(cindy, david);

            var therapist = new Therapist("Charles");

            Assert.Throws<ArgumentNullException>
            (
                () => new Session(null, cindy_and_david)
            );
            Assert.Throws<ArgumentException>
            (
                () => new Session(therapist, Pair.Create(alice, alice))
            );
            Assert.Throws<ArgumentException>
            (
                () => new Session(therapist, alice_and_bob)
            );
        }
        [Test]
        public void Test_Constructor()
        {
            var alice = new Patient("Alice");
            var bob = new Patient("Bob");
            var alice_and_bob = Pair.Create(alice, bob);

            Patient
                .RelationshipManager
                .CreateCouple(alice, bob);

            var therapist = new Therapist("Charles");

            var session = new Session(therapist, alice_and_bob);

            Assert.AreEqual(therapist, session.Therapist);
            Assert.AreEqual(alice_and_bob, session.Patients);

            Assert.AreEqual(3, session.Actors.Count);

            Assert.IsTrue(session.Actors.Contains(therapist));
            Assert.IsTrue(session.Actors.Contains(alice));
            Assert.IsTrue(session.Actors.Contains(bob));
        }

        [Test]
        public void Test_StaticConstructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentException>
            (
                () => Session.Create(null, "Alice", "Bob")
            );
            Assert.Throws<ArgumentException>
            (
                () => Session.Create("", "Alice", "Bob")
            );
            Assert.Throws<ArgumentException>
            (
                () => Session.Create(" ", "Alice", "Bob")
            );

            Assert.Throws<ArgumentException>
            (
                () => Session.Create("Alice", null, "Bob")
            );
            Assert.Throws<ArgumentException>
            (
                () => Session.Create("Alice", "", "Bob")
            );
            Assert.Throws<ArgumentException>
            (
                () => Session.Create("Alice", " ", "Bob")
            );

            Assert.Throws<ArgumentException>
            (
                () => Session.Create("Alice", "Bob", null)
            );
            Assert.Throws<ArgumentException>
            (
                () => Session.Create("Alice", "Bob", "")
            );
            Assert.Throws<ArgumentException>
            (
                () => Session.Create("Alice", "Bob", " ")
            );
        }
        [Test]
        public void Test_StaticConstructor()
        {
            var session = Session.Create("Charles", "Alice", "Bob");

            Assert.AreEqual("Charles", session.Therapist.Name);
            Assert.AreEqual("Alice", session.Patients.First.Name);
            Assert.AreEqual("Bob", session.Patients.Second.Name);
        }
    }
}
