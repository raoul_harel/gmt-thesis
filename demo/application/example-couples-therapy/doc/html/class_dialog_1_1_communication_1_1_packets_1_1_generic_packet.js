var class_dialog_1_1_communication_1_1_packets_1_1_generic_packet =
[
    [ "GenericPacket", "class_dialog_1_1_communication_1_1_packets_1_1_generic_packet.html#aafc8709027e0f713802c068c631d81ed", null ],
    [ "Equals", "class_dialog_1_1_communication_1_1_packets_1_1_generic_packet.html#af986ef7c48a36c2e05097a8d15144cb0", null ],
    [ "GetHashCode", "class_dialog_1_1_communication_1_1_packets_1_1_generic_packet.html#ab72b1902e6d2678180b416d1000542b2", null ],
    [ "ToString", "class_dialog_1_1_communication_1_1_packets_1_1_generic_packet.html#ade7b3deeb5220b5ac520f109d4d85ec3", null ],
    [ "Author", "class_dialog_1_1_communication_1_1_packets_1_1_generic_packet.html#adb2f48012d0d06ff77a545878bed8f89", null ],
    [ "Payload", "class_dialog_1_1_communication_1_1_packets_1_1_generic_packet.html#ab908dc4855f55fe5283ea0257705da2a", null ]
];