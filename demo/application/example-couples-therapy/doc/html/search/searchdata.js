var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvw",
  1: "abcdefgilmnoprstuv",
  2: "cde",
  3: "abcdefghilnoprstuvw",
  4: "acgis",
  5: "air",
  6: "acdfips",
  7: "abcdefgiklmnoprstuvw",
  8: "frs"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Events"
};

