var namespace_dialog_1_1_communication_1_1_channels =
[
    [ "DataChannel", "interface_dialog_1_1_communication_1_1_channels_1_1_data_channel.html", null ],
    [ "DoubleBufferedDataChannel", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", null ],
    [ "GenericChannel", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel" ],
    [ "GenericDoubleBufferedChannel", "class_dialog_1_1_communication_1_1_channels_1_1_generic_double_buffered_channel.html", "class_dialog_1_1_communication_1_1_channels_1_1_generic_double_buffered_channel" ]
];