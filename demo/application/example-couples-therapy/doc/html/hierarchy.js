var hierarchy =
[
    [ "Dialog.Agency.Scene.Actor", "interface_dialog_1_1_agency_1_1_scene_1_1_actor.html", [
      [ "Example.CouplesTherapy.Agency.Scene.VirtualHuman", "class_example_1_1_couples_therapy_1_1_agency_1_1_scene_1_1_virtual_human.html", [
        [ "Example.CouplesTherapy.Agency.Scene.Patient", "class_example_1_1_couples_therapy_1_1_agency_1_1_scene_1_1_patient.html", null ],
        [ "Example.CouplesTherapy.Agency.Scene.Therapist", "class_example_1_1_couples_therapy_1_1_agency_1_1_scene_1_1_therapist.html", null ]
      ] ]
    ] ],
    [ "Dialog.Agency.System.AgencySystem", "class_dialog_1_1_agency_1_1_system_1_1_agency_system.html", null ],
    [ "Dialog.Communication.Management.ChannelBatch", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch.html", null ],
    [ "Common.Time.Clock", "interface_common_1_1_time_1_1_clock.html", [
      [ "Common.Time.VariableIncrementClock", "class_common_1_1_time_1_1_variable_increment_clock.html", null ]
    ] ],
    [ "Dialog.Communication.Management.CommunicationManager", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html", null ],
    [ "Comparer", null, [
      [ "Common.Functional.FunctionalComparer< T >", "class_common_1_1_functional_1_1_functional_comparer.html", null ]
    ] ],
    [ "Dialog.Agency.Modules.Perception.CurrentActivityReport", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_current_activity_report.html", null ],
    [ "Dialog.Communication.Actors.DataAuthor", "interface_dialog_1_1_communication_1_1_actors_1_1_data_author.html", [
      [ "Dialog.Communication.Actors.ManagedActor", "interface_dialog_1_1_communication_1_1_actors_1_1_managed_actor.html", [
        [ "Example.CouplesTherapy.Agency.Scene.VirtualHuman", "class_example_1_1_couples_therapy_1_1_agency_1_1_scene_1_1_virtual_human.html", null ]
      ] ]
    ] ],
    [ "Dialog.Communication.Channels.DataChannel", "interface_dialog_1_1_communication_1_1_channels_1_1_data_channel.html", [
      [ "Dialog.Communication.Channels.DataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_data_channel.html", null ],
      [ "Dialog.Communication.Channels.DoubleBufferedDataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", null ]
    ] ],
    [ "Dialog.Communication.Channels.DataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_data_channel.html", [
      [ "Dialog.Communication.Channels.DoubleBufferedDataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", null ],
      [ "Dialog.Communication.Channels.GenericChannel< T >", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html", null ]
    ] ],
    [ "Dialog.Communication.Packets.DataPacket", "interface_dialog_1_1_communication_1_1_packets_1_1_data_packet.html", [
      [ "Dialog.Communication.Packets.DataPacket< T >", "interface_dialog_1_1_communication_1_1_packets_1_1_data_packet.html", null ]
    ] ],
    [ "Dialog.Communication.Packets.DataPacket< T >", "interface_dialog_1_1_communication_1_1_packets_1_1_data_packet.html", [
      [ "Dialog.Communication.Packets.GenericPacket< T >", "class_dialog_1_1_communication_1_1_packets_1_1_generic_packet.html", null ]
    ] ],
    [ "Dialog.Communication.Management.CommunicationManager.DataSubmission", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_submission.html", null ],
    [ "Dialog.Communication.Management.CommunicationManager.DataSubscription", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_subscription.html", null ],
    [ "Dialog.Agency.Dialog_Moves.DialogMove", "interface_dialog_1_1_agency_1_1_dialog___moves_1_1_dialog_move.html", [
      [ "Dialog.Agency.Dialog_Moves.DialogMove< TActor >", "class_dialog_1_1_agency_1_1_dialog___moves_1_1_dialog_move.html", null ]
    ] ],
    [ "Dialog.Agency.Dialog_Moves.DialogMove< Actor >", "interface_dialog_1_1_agency_1_1_dialog___moves_1_1_dialog_move.html", [
      [ "Dialog.Agency.Dialog_Moves.IdleMove", "class_dialog_1_1_agency_1_1_dialog___moves_1_1_idle_move.html", null ]
    ] ],
    [ "Dialog.Agency.Dialog_Moves.DialogMoveKind", "class_dialog_1_1_agency_1_1_dialog___moves_1_1_dialog_move_kind.html", [
      [ "Example.CouplesTherapy.Agency.Dialog_Moves.ScenarioDialogMoveKind", "class_example_1_1_couples_therapy_1_1_agency_1_1_dialog___moves_1_1_scenario_dialog_move_kind.html", null ]
    ] ],
    [ "Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_dialog_move_realization_event.html", null ],
    [ "Common.Randomization.Distributions.Distribution", "interface_common_1_1_randomization_1_1_distributions_1_1_distribution.html", [
      [ "Common.Randomization.Distributions.PseudoRandomDistribution", "class_common_1_1_randomization_1_1_distributions_1_1_pseudo_random_distribution.html", [
        [ "Common.Randomization.Distributions.UniformDistribution", "class_common_1_1_randomization_1_1_distributions_1_1_uniform_distribution.html", null ]
      ] ]
    ] ],
    [ "Dialog.Communication.Channels.DoubleBufferedDataChannel", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", [
      [ "Dialog.Communication.Channels.DoubleBufferedDataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", null ]
    ] ],
    [ "Dialog.Communication.Channels.DoubleBufferedDataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", [
      [ "Dialog.Communication.Channels.GenericDoubleBufferedChannel< T >", "class_dialog_1_1_communication_1_1_channels_1_1_generic_double_buffered_channel.html", null ]
    ] ],
    [ "IEnumerable", null, [
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ],
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ],
      [ "Dialog.Agency.System.ModuleBundle", "class_dialog_1_1_agency_1_1_system_1_1_module_bundle.html", null ]
    ] ],
    [ "Common.Collections.Interfaces.ImmutableCollection", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", [
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ]
    ] ],
    [ "Common.Collections.Interfaces.ImmutableCollection< Actor >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ],
    [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", [
      [ "Common.Collections.Interfaces.ImmutableList< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_list.html", [
        [ "Common.Collections.Views.ListView< T >", "class_common_1_1_collections_1_1_views_1_1_list_view.html", null ]
      ] ],
      [ "Common.Collections.Views.CollectionView< T >", "class_common_1_1_collections_1_1_views_1_1_collection_view.html", [
        [ "Common.Collections.Views.ListView< T >", "class_common_1_1_collections_1_1_views_1_1_list_view.html", null ]
      ] ]
    ] ],
    [ "Dialog.Agency.State.ImmutableState", "interface_dialog_1_1_agency_1_1_state_1_1_immutable_state.html", [
      [ "Dialog.Agency.State.MutableState", "interface_dialog_1_1_agency_1_1_state_1_1_mutable_state.html", [
        [ "Dialog.Agency.State.InformationState", "class_dialog_1_1_agency_1_1_state_1_1_information_state.html", null ]
      ] ]
    ] ],
    [ "Dialog.SPB.Timing_Conflicts.InterruptionRules", "class_dialog_1_1_s_p_b_1_1_timing___conflicts_1_1_interruption_rules.html", null ],
    [ "Dialog.SPB.Program.Node", "class_dialog_1_1_s_p_b_1_1_program_1_1_node.html", [
      [ "Dialog.SPB.Program.Conditional", "class_dialog_1_1_s_p_b_1_1_program_1_1_conditional.html", null ],
      [ "Dialog.SPB.Program.Conjunction", "class_dialog_1_1_s_p_b_1_1_program_1_1_conjunction.html", null ],
      [ "Dialog.SPB.Program.Disjunction", "class_dialog_1_1_s_p_b_1_1_program_1_1_disjunction.html", null ],
      [ "Dialog.SPB.Program.Divergence", "class_dialog_1_1_s_p_b_1_1_program_1_1_divergence.html", null ],
      [ "Dialog.SPB.Program.EventExpectation", "class_dialog_1_1_s_p_b_1_1_program_1_1_event_expectation.html", null ],
      [ "Dialog.SPB.Program.Repeat", "class_dialog_1_1_s_p_b_1_1_program_1_1_repeat.html", null ],
      [ "Dialog.SPB.Program.Sequence", "class_dialog_1_1_s_p_b_1_1_program_1_1_sequence.html", null ]
    ] ],
    [ "Common.DesignPatterns.ObjectBuilder< T >", "class_common_1_1_design_patterns_1_1_object_builder.html", null ],
    [ "Common.DesignPatterns.ObjectBuilder< ChannelBatch >", "class_common_1_1_design_patterns_1_1_object_builder.html", [
      [ "Dialog.Communication.Management.ChannelBatch.Builder", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch_1_1_builder.html", null ]
    ] ],
    [ "Common.DesignPatterns.ObjectBuilder< CommunicationManager >", "class_common_1_1_design_patterns_1_1_object_builder.html", [
      [ "Dialog.Communication.Management.CommunicationManager.Builder", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_builder.html", null ]
    ] ],
    [ "Common.DesignPatterns.ObjectBuilder< InformationState >", "class_common_1_1_design_patterns_1_1_object_builder.html", [
      [ "Dialog.Agency.State.InformationState.Builder", "class_dialog_1_1_agency_1_1_state_1_1_information_state_1_1_builder.html", null ]
    ] ],
    [ "Common.DesignPatterns.ObjectBuilder< ModuleBundle >", "class_common_1_1_design_patterns_1_1_object_builder.html", [
      [ "Dialog.Agency.System.ModuleBundle.Builder", "class_dialog_1_1_agency_1_1_system_1_1_module_bundle_1_1_builder.html", null ]
    ] ],
    [ "Dialog.Agency.Modules.OperationModule", "interface_dialog_1_1_agency_1_1_modules_1_1_operation_module.html", [
      [ "Dialog.Agency.Modules.OperationModule< TState >", "class_dialog_1_1_agency_1_1_modules_1_1_operation_module.html", null ]
    ] ],
    [ "Dialog.Agency.Modules.OperationModule< ImmutableState >", "interface_dialog_1_1_agency_1_1_modules_1_1_operation_module.html", [
      [ "Dialog.Agency.Modules.Action.ActionRealizationModule", "class_dialog_1_1_agency_1_1_modules_1_1_action_1_1_action_realization_module.html", [
        [ "Dialog.Agency.Modules.Action.ActionRealizationModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_action_1_1_action_realization_module_stub.html", null ],
        [ "Example.CouplesTherapy.Agency.Modules.Action.VHARModule", "class_example_1_1_couples_therapy_1_1_agency_1_1_modules_1_1_action_1_1_v_h_a_r_module.html", null ]
      ] ],
      [ "Dialog.Agency.Modules.Action.ActionTimingModule", "class_dialog_1_1_agency_1_1_modules_1_1_action_1_1_action_timing_module.html", [
        [ "Dialog.Agency.Modules.Action.ActionTimingModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_action_1_1_action_timing_module_stub.html", null ],
        [ "Dialog.SPB.Modules.SPBATModule", "class_dialog_1_1_s_p_b_1_1_modules_1_1_s_p_b_a_t_module.html", null ]
      ] ],
      [ "Dialog.Agency.Modules.Deliberation.ActionSelectionModule", "class_dialog_1_1_agency_1_1_modules_1_1_deliberation_1_1_action_selection_module.html", [
        [ "Dialog.Agency.Modules.Deliberation.ActionSelectionModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_deliberation_1_1_action_selection_module_stub.html", null ],
        [ "Dialog.SPB.Modules.SPBASModule", "class_dialog_1_1_s_p_b_1_1_modules_1_1_s_p_b_a_s_module.html", null ],
        [ "Example.CouplesTherapy.Agency.Modules.Deliberation.ManualASModule", "class_example_1_1_couples_therapy_1_1_agency_1_1_modules_1_1_deliberation_1_1_manual_a_s_module.html", null ]
      ] ],
      [ "Dialog.Agency.Modules.Perception.CurrentActivityPerceptionModule", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_current_activity_perception_module.html", [
        [ "Dialog.Agency.Modules.Perception.CurrentActivityPerceptionModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_current_activity_perception_module_stub.html", null ],
        [ "Example.CouplesTherapy.Agency.Modules.Perception.VHCAPModule", "class_example_1_1_couples_therapy_1_1_agency_1_1_modules_1_1_perception_1_1_v_h_c_a_p_module.html", null ]
      ] ],
      [ "Dialog.Agency.Modules.Perception.RecentActivityPerceptionModule", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_recent_activity_perception_module.html", [
        [ "Dialog.Agency.Modules.Perception.RecentActivityPerceptionModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_recent_activity_perception_module_stub.html", null ],
        [ "Example.CouplesTherapy.Agency.Modules.Perception.VHRAPModule", "class_example_1_1_couples_therapy_1_1_agency_1_1_modules_1_1_perception_1_1_v_h_r_a_p_module.html", null ]
      ] ]
    ] ],
    [ "Dialog.Agency.Modules.OperationModule< MutableState >", "interface_dialog_1_1_agency_1_1_modules_1_1_operation_module.html", [
      [ "Dialog.Agency.Modules.Deliberation.StateUpdateModule", "class_dialog_1_1_agency_1_1_modules_1_1_deliberation_1_1_state_update_module.html", [
        [ "Dialog.Agency.Modules.Deliberation.StateUpdateModuleStub", "class_dialog_1_1_agency_1_1_modules_1_1_deliberation_1_1_state_update_module_stub.html", null ],
        [ "Example.CouplesTherapy.Agency.Modules.Deliberation.VHSUModule", "class_example_1_1_couples_therapy_1_1_agency_1_1_modules_1_1_deliberation_1_1_v_h_s_u_module.html", null ]
      ] ]
    ] ],
    [ "Common.Functional.Options.Option< T >", "interface_common_1_1_functional_1_1_options_1_1_option.html", [
      [ "Common.Functional.Options.None< T >", "struct_common_1_1_functional_1_1_options_1_1_none.html", null ],
      [ "Common.Functional.Options.Some< T >", "struct_common_1_1_functional_1_1_options_1_1_some.html", null ]
    ] ],
    [ "Common.Functional.Options.Option< int >", "interface_common_1_1_functional_1_1_options_1_1_option.html", null ],
    [ "Common.Collections.Pair< TFirst, TSecond >", "struct_common_1_1_collections_1_1_pair.html", null ],
    [ "Common.Collections.Pair< PatientController, PatientController >", "struct_common_1_1_collections_1_1_pair.html", null ],
    [ "Example.CouplesTherapy.Simulation.Actor_Controllers.PatientController", "class_example_1_1_couples_therapy_1_1_simulation_1_1_actor___controllers_1_1_patient_controller.html", null ],
    [ "Dialog.Agency.Modules.Perception.RecentActivityReport", "class_dialog_1_1_agency_1_1_modules_1_1_perception_1_1_recent_activity_report.html", null ],
    [ "Dialog.SPB.Rules.Rule< T >", "class_dialog_1_1_s_p_b_1_1_rules_1_1_rule.html", null ],
    [ "Example.CouplesTherapy.Simulation.Scenario", "class_example_1_1_couples_therapy_1_1_simulation_1_1_scenario.html", null ],
    [ "Example.CouplesTherapy.Agency.Scene.Session", "class_example_1_1_couples_therapy_1_1_agency_1_1_scene_1_1_session.html", null ],
    [ "Dialog.SPB.State.SocialContext", "class_dialog_1_1_s_p_b_1_1_state_1_1_social_context.html", null ],
    [ "Example.CouplesTherapy.Communication.Speech", "class_example_1_1_couples_therapy_1_1_communication_1_1_speech.html", null ],
    [ "Dialog.Agency.State.StateBundle                    < T_RAP_State, T_CAP_State, T_SU_State, T_AS_State, T_AT_State, T_AR_State >", "class_dialog_1_1_agency_1_1_state_1_1_state_bundle_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01_01.html", null ],
    [ "Dialog.Agency.State.StateComponent", "interface_dialog_1_1_agency_1_1_state_1_1_state_component.html", [
      [ "Dialog.Agency.State.StateComponent< T >", "class_dialog_1_1_agency_1_1_state_1_1_state_component.html", null ]
    ] ],
    [ "Dialog.Agency.System.SystemActivitySnapshot", "class_dialog_1_1_agency_1_1_system_1_1_system_activity_snapshot.html", null ],
    [ "Example.CouplesTherapy.Simulation.Actor_Controllers.TherapistController", "class_example_1_1_couples_therapy_1_1_simulation_1_1_actor___controllers_1_1_therapist_controller.html", null ],
    [ "Common.Time.Timer", "class_common_1_1_time_1_1_timer.html", null ]
];