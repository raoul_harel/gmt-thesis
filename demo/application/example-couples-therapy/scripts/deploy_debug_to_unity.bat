rem ################################################################
rem Copies all dlls from the solution's output directory to Unity's 
rem external assets directory, and builds their debug symbol data-
rem bases for Mono.
rem
rem Source path: "application\examples-couples-therapy\library\bin\Debug"
rem Target path: "application\unity\Assets\External"
rem ################################################################

setlocal
set unityExternalAssetsPath=..\..\..\..\unity\Assets\External\
set mdbMakerPath="C:\Program Files (x86)\Mono\bin\pdb2mdb.bat"

del /Q "%unityExternalAssetsPath%"

xcopy "*.dll" %unityExternalAssetsPath% /Y /R
xcopy "*.pdb" %unityExternalAssetsPath% /Y /R
xcopy "*.xml" %unityExternalAssetsPath% /Y /R

call %mdbMakerPath% "%unityExternalAssetsPath%\commons.dll"
call %mdbMakerPath% "%unityExternalAssetsPath%\dialog-communication.dll"
call %mdbMakerPath% "%unityExternalAssetsPath%\dialog-agency.dll"
call %mdbMakerPath% "%unityExternalAssetsPath%\dialog-spb.dll"
call %mdbMakerPath% "%unityExternalAssetsPath%\example-couples-therapy.dll"
