rem ################################################################
rem Copies all dlls from the solution's output directory to Unity's 
rem external assets directory.
rem
rem Source path: "application\examples-couples-therapy\library\bin\Debug"
rem Target path: "application\unity\Assets\External"
rem ################################################################

setlocal
set unityExternalAssetsPath=..\..\..\..\unity\Assets\External\

del /Q "%unityExternalAssetsPath%"

xcopy "*.dll" %unityExternalAssetsPath% /Y /R
